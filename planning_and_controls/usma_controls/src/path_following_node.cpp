#include <ros/ros.h>

#include <usma_controls/path_follower.h>

int main( int argc, char** argv )
{
    ros::init(argc, argv, "path_following");

    ros::NodeHandle n;

    ros::AsyncSpinner spinner(8);
    spinner.start();

    ROS_INFO("[WAY POINT FOLLOWER NODE]::Starting");
    usma::PathFollower* uop;
    uop = new usma::PathFollower( "/planned_path", "/gvr_bot/cmd_vel", n );
    ros::waitForShutdown();
    

    return 0;
}
