#include <usma_controls/path_follower.h>

namespace usma
{
  PathFollower::PathFollower(std::string path_topic, std::string cmd_topic, ros::NodeHandle& n)
  {
    num_obstacles_ = 0;
    last_joy_time_ = ros::Time::now();

    new_path_rdy_ = false;
    nh_ = n;

    joy_sub_ = nh_.subscribe("/joy", 5, &PathFollower::joyCb, this);
    path_sub_ = nh_.subscribe(path_topic, 5, &PathFollower::pathCb, this);
    obstacle_sub_ = nh_.subscribe("/obstacles_assembled/voxel", 5, &PathFollower::obstacleCb, this);
    cmd_pub_ = nh_.advertise<geometry_msgs::Twist>(cmd_topic, 5);
    cmd_pub_vis_ = nh_.advertise<geometry_msgs::WrenchStamped>(cmd_topic+std::string("_vis"), 5);
    mode_pub_ = nh_.advertise<std_msgs::String>("/igvc/mode", 5);

    initParams();

    control_thread_ = boost::thread( &PathFollower::controlLoop, this );
  };

  PathFollower::~PathFollower()
  {
    path_sub_.shutdown();

    control_thread_.join();
  };

  void PathFollower::initParams()
  {
    nh_.param<std::string>("/usma_controls/map_frame", map_frame_, "/odom");
    nh_.param<std::string>("/usma_controls/base_frame", base_frame_, "/base_link");
    nh_.param<double>("/usma_controls/control_freq", control_freq_, 100.0);
    nh_.param<double>("/usma_controls/tracking_distance", tracking_distance_, 0.1);
    nh_.param<double>("/usma_controls/stop_distance", stop_distance_, tracking_distance_/2.0);
    nh_.param<double>("/usma_controls/tf_fudge_factor", tf_fudge_factor_, 0.0);
    nh_.param<double>("/usma_controls/max_speed", max_speed_, 0.5);
    nh_.param<int>("/usma_controls/frames_look_ahead", num_look_ahead_frames_, 5);
  }

  void PathFollower::obstacleCb( pcl::PCLPointCloud2::ConstPtr cloud )
  {
    pcl::PointCloud<pcl::PointXYZ> pcl_cloud;
    pcl::fromPCLPointCloud2( *(cloud), pcl_cloud );

    obstacle_lock_.lock();
    obstacles_.resize(2,pcl_cloud.size());
    for( int i=0; i<pcl_cloud.size(); i++ )
    {
      obstacles_(0,i) = pcl_cloud[i].x;
      obstacles_(1,i) = pcl_cloud[i].y;
    }
    num_obstacles_ = pcl_cloud.size();
    obstacle_lock_.unlock();

    return;
  }

  void PathFollower::joyCb( const sensor_msgs::Joy &j )
  {
    std_msgs::String mode;

      if( !enable_joystick_ && j.buttons[0] )
      {
        ROS_ERROR("[Path Follower]::manual");
        mode.data = "manual";
        mode_pub_.publish(mode);
        enable_joystick_ = j.buttons[0];
        enable_controller_ = false;
      }
      else if( !enable_controller_ && j.buttons[1] )
      {
        ROS_ERROR("[Path Follower]::autonomous");
        mode.data = "auto";
        mode_pub_.publish(mode);
        enable_controller_ = j.buttons[1];
        enable_joystick_ = false;
      }

    last_joy_time_ = ros::Time::now();
    joy_lock_.lock();

    setXTheta( j.axes[1], j.axes[0], joy_cmd_ );
    joy_cmd_.linear.x  = std::max(std::min(joy_cmd_.linear.x,max_speed_),-max_speed_);
    joy_cmd_.angular.z  = std::max(std::min(joy_cmd_.angular.z,max_speed_),-max_speed_);
    joy_lock_.unlock();
  }

  void PathFollower::pathCb( const nav_msgs::Path &m )
  {
    // verify path against current position
    //

    // if path starts where you currently are then feed it into the velocity control
    
    if( m.poses.size() <= 0 )
      return;

    geometry_msgs::Point p1 = m.poses[0].pose.position;
    geometry_msgs::Point p2 = cur_location_.pose.position;
    p1.z = 0.0;
    p2.z = 0.0;

    if( withinDistanceThresh( p1, p2, tracking_distance_ ) )
    {
      ROS_INFO("[PATH FOLLOWER]::Valid path -- now tracking");
      cur_path_mtx_.lock();
      cur_path_ = m;
      cur_index_ = 0;
      new_path_rdy_ = true;
      cur_path_mtx_.unlock();
    }
    else
    {
      ROS_ERROR("[PATH FOLLOWER]::Invalid path -- throwing away");
      ROS_ERROR("-----%f,%f,%f",p1.x,p1.y,p1.z);
      ROS_ERROR("-----%f,%f,%f",p2.x,p2.y,p2.z);
    }
  }

  void PathFollower::controlLoop()
  {
    prev_cmd_.linear.x = 0.0;
    prev_cmd_.linear.y = 0.0;
    prev_cmd_.linear.z = 0.0;
    prev_cmd_.angular.x = 0.0;
    prev_cmd_.angular.y = 0.0;
    prev_cmd_.angular.z = 0.0;
    ros::Rate control_rate( (int) control_freq_ );
    tf::StampedTransform tfRobotToWorld, tfWorldToRobot, tfWorldToNavFrame, tfRobotToGoal, tfNavFrameToGoal;

    std::vector<tf::StampedTransform> tfMultiGoalCheck;

    ros::Duration( 1.0 ).sleep();

    cur_goal_.header.frame_id = "odom";
    cur_goal_.pose.position.x = 0.0;
    cur_goal_.pose.position.y = 0.0;
    cur_goal_.pose.position.z = 0.0;
    cur_goal_.pose.orientation.x = 0.0;
    cur_goal_.pose.orientation.y = 0.0;
    cur_goal_.pose.orientation.z = 0.0;
    cur_goal_.pose.orientation.w = 1.0;

    try
    {
      tf_listener_.lookupTransform(map_frame_, base_frame_, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfWorldToRobot);
      tf_listener_.lookupTransform(base_frame_, map_frame_, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfRobotToWorld);
      tf_listener_.lookupTransform(map_frame_, cur_goal_.header.frame_id, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfWorldToNavFrame);
    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR_THROTTLE(1,"[PATH FOLLOWER]::%s",ex.what());
    }

    tfToPose( tfWorldToRobot, cur_goal_.pose );

    ros::Time t1, t2;
    t1 = ros::Time::now();
    t2 = ros::Time::now();

    ROS_INFO("[PATH FOLLOWER]::Starting");
    while( ros::ok() )
    {
      cur_path_mtx_.lock();

      // Simple implementation involves using the delta transform as an attractive force.....
      // Not great but will work for now

      bool foundTF = false;

      try
      {
        tf_listener_.lookupTransform(map_frame_, base_frame_, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfWorldToRobot);
        tf_listener_.lookupTransform(base_frame_, map_frame_, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfRobotToWorld);
        tf_listener_.lookupTransform(map_frame_, cur_goal_.header.frame_id, ros::Time::now() - ros::Duration(tf_fudge_factor_), tfWorldToNavFrame);
        foundTF = true;
      }
      catch (tf::TransformException ex)
      {
        ROS_ERROR_THROTTLE(1,"[PATH FOLLOWER]::%s",ex.what());
      }
      
      // Update current location and tell the rest of the process that
      cur_location_mtx_.lock();
      tfToPose( tfWorldToRobot, cur_location_.pose );
      cur_location_.header.frame_id = map_frame_;
      cur_location_.header.stamp = (ros::Time::now() - ros::Duration(tf_fudge_factor_));
      cur_location_.header.seq++;
      cur_location_mtx_.unlock();

      if ( foundTF && cur_index_ < cur_path_.poses.size() )
      {
        cur_goal_.pose = cur_path_.poses[cur_index_].pose;
        poseToTf( cur_goal_.pose, tfNavFrameToGoal );

        tfMultiGoalCheck.clear();
        int frames_ahead = std::min<int>(num_look_ahead_frames_, ((int)cur_path_.poses.size() - cur_index_));
        tfMultiGoalCheck.resize(frames_ahead);
        for( int i = 0; i < frames_ahead; i++ )
        {
          poseToTf( cur_path_.poses[cur_index_+i].pose, tfNavFrameToGoal );
          tfMultiGoalCheck[i].setData(tfRobotToWorld * tfNavFrameToGoal);
        }

        tf::Vector3 rTg;
        rTg.setZero();
        for( int i = 0; i < frames_ahead; i++ )
          rTg += tfMultiGoalCheck[i].getOrigin();

        rTg /= frames_ahead;

        double dist = tfDist(rTg);
        double forward_v = rTg.getX();
        double theta_v = atan2( rTg.getY() , rTg.getX() );
        forward_v *= (fabs(theta_v)<0.75?1.0:(3.14-fabs(theta_v)));
        theta_v *= (fabs(theta_v)<0.75?1.0:(1.0+fabs(theta_v)));

        if( theta_v < -max_speed_ )
          theta_v = -max_speed_;
        else if( theta_v > max_speed_ )
          theta_v = max_speed_;

        if( forward_v < -max_speed_ )
          forward_v = -max_speed_;
        else if( forward_v > max_speed_ )
          forward_v = max_speed_;

        if( dist <= stop_distance_ )
        {
          zeroTwist( cur_cmd_ );
          cur_index_++;
          ROS_INFO_THROTTLE(1, "[PATH FOLLOWER]::At location -- need new goal" );
        }
        else if( dist <= tracking_distance_ )
        {
          setXTheta( forward_v, theta_v, cur_cmd_ );
          cur_index_++;
          ROS_INFO_THROTTLE(1, "[PATH FOLLOWER]::Tracking Location -- need new goal" );
        }
        else
        {
          setXTheta( forward_v, theta_v, cur_cmd_ );
          ROS_INFO_THROTTLE(1, "[PATH FOLLOWER]::Tracking Location--%f", dist );
        }


        ROS_INFO_THROTTLE( 1, "[PATH FOLLOWER]::Del Location |X: %f\t||Y: %f\t||Z: %f\t|", 
            rTg.getX(),
            rTg.getY(),
            rTg.getZ());

        ROS_INFO_THROTTLE( 1, "[PATH FOLLOWER]::Cur Location |X: %f\t||Y: %f\t||Z: %f\t|", 
            rTg.getX(), 
            rTg.getY(), 
            rTg.getZ());

        ROS_INFO_THROTTLE( 1, "[PATH FOLLOWER]::Cur Goal |X: %f\t||Y: %f\t||Z: %f\t|", 
            rTg.getX(), 
            rTg.getY(), 
            rTg.getZ());
      }
      else
      {
        zeroTwist( cur_cmd_ );
      }

      joy_lock_.lock();
      if(!enable_controller_ && (ros::Time::now()-last_joy_time_).toSec() > 0.1)
      {
        zeroTwist( cur_cmd_ );
      }
      else if (enable_joystick_)
      {
        cur_cmd_ = joy_cmd_;
      }

      joy_lock_.unlock();

      cur_cmd_.linear.x  = std::max(std::min(cur_cmd_.linear.x,max_speed_),-max_speed_);
      cur_cmd_.angular.z = std::max(std::min(cur_cmd_.angular.z,max_speed_),-max_speed_);

      cur_cmd_vis_.wrench.force.x=0.0;
      cur_cmd_vis_.wrench.force.y=0.0;
      cur_cmd_vis_.wrench.force.z=0.0;
      cur_cmd_vis_.wrench.torque.x=0.0;
      cur_cmd_vis_.wrench.torque.y=0.0;
      cur_cmd_vis_.wrench.torque.z=0.0;
      obstacle_lock_.lock();
      int counted_points = 0;
      double dist, theta;
      double fx = 0.0;
      double fy = 0.0;
      int cp;
      for(int i=0; i<num_obstacles_; i++)
      {
        cp = 0;
        dist = calcDist( obstacles_(0,i), obstacles_(1,i), 0.0 );
        theta = atan2( obstacles_(1,i), obstacles_(0,i) );
        if( dist < 2.0 && fabs(theta) < 1.57 ) //&& (obstacles_(0,i)*cur_cmd_.linear.x) > 0.05 )
        {
          fx -= obstacles_(0,i)/fabs(obstacles_(0,i)) / (dist);
          fy -= obstacles_(1,i)/fabs(obstacles_(1,i)) / (dist);
          cp = 1;
        }
        counted_points+=cp;
      }
      if( counted_points != 0 )
      {
        cur_cmd_vis_.wrench.force.x = fx;
        cur_cmd_vis_.wrench.torque.z = fy;
        cur_cmd_vis_.wrench.force.x /= ((double)counted_points);
        cur_cmd_vis_.wrench.torque.z /= ((double)counted_points);
      }
      else
      {
        cur_cmd_vis_.wrench.force.x = 0.0;
        cur_cmd_vis_.wrench.torque.z = 0.0;
      }
      cur_cmd_vis_.wrench.force.x  = std::max(std::min(cur_cmd_vis_.wrench.force.x,0.5),-0.5);
      cur_cmd_vis_.wrench.torque.z = std::max(std::min(cur_cmd_vis_.wrench.torque.z,0.5),-0.5);

      ROS_INFO_THROTTLE(1,"[PATH FOLLOWER]::Num Obstacles %zu Force X %f -- Torque Z %f", num_obstacles_,cur_cmd_vis_.wrench.force.x, cur_cmd_vis_.wrench.torque.z);

      obstacle_lock_.unlock();

      cur_cmd_vis_.header.seq++;
      cur_cmd_vis_.header.stamp = ros::Time::now();
      cur_cmd_vis_.header.frame_id = base_frame_;

      cmd_pub_vis_.publish( cur_cmd_vis_ );
      

      joy_lock_.lock();
      if(enable_controller_)
      {
        setXTheta( max_speed_, 0.0, cur_cmd_ );
        cur_cmd_.linear.x += cur_cmd_vis_.wrench.force.x;
        cur_cmd_.angular.z += cur_cmd_vis_.wrench.torque.z;
        cur_cmd_.linear.x = 0.5 * cur_cmd_.linear.x + 0.5 * prev_cmd_.linear.x;
        cur_cmd_.angular.z = 0.5 * cur_cmd_.angular.z + 0.5 * prev_cmd_.angular.z;
      }
      joy_lock_.unlock();

      cmd_pub_.publish( cur_cmd_ );

      t2 = t1;
      t1 = ros::Time::now();
      ROS_INFO_THROTTLE(1, "[PATH FOLLOWER]::Update Rate %f Hz", 1.0/((t1 - t2).toSec()) );

      cur_path_mtx_.unlock();

      control_rate.sleep();
    }
  }
}
