#ifndef OCTOMAP_PLANNER_H_
#define OCTOMAP_PLANNER_H_

#include <ros/ros.h>

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>

#include <std_msgs/String.h>
#include <sensor_msgs/Joy.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/WrenchStamped.h>
#include <geometry_msgs/Twist.h>
#include <tf/transform_listener.h>

// octomap specifics
#include <octomap_ros/conversions.h>
#include <octomap_msgs/Octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>

// pcl specifics
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

namespace usma
{
  class PathFollower
  {
    public:
      PathFollower(std::string path_topic, std::string cmd_topic, ros::NodeHandle& n);

      ~PathFollower();

      static double tfDist( tf::Vector3 &t )
      {
        return t.length();
      }

      static double tfDist( tf::StampedTransform &t )
      {
        return t.getOrigin().length();
      }

      static void poseToTf( geometry_msgs::Pose &p, tf::StampedTransform &t )
      {
        t.setOrigin( tf::Vector3(p.position.x, p.position.y, p.position.z) );
        tf::Quaternion qtf( p.orientation.x, p.orientation.y, p.orientation.z, p.orientation.w );
        t.setRotation(qtf);
      }

      static void tfToMat4d( tf::StampedTransform &t, Eigen::Matrix4d &m )
      {
        Eigen::Quaternion<double> q( t.getRotation().getW(), t.getRotation().getAxis().getX(), t.getRotation().getAxis().getY(), t.getRotation().getAxis().getZ() );
        Eigen::Matrix<double, 3, 1> trans;
        trans(0, 0) = t.getOrigin().getX();
        trans(1, 0) = t.getOrigin().getY();
        trans(2, 0) = t.getOrigin().getZ();
        Eigen::Matrix<double, 1, 4> a;
        a(0, 0) = 0;
        a(0, 1) = 0;
        a(0, 2) = 0;
        a(0, 3) = 1;
        m.block<3,3>(0,0) = q.toRotationMatrix();
        m.block<3,1>(0,3) = trans;
        m.block<1,4>(3,0) = a;
      }

      static void mat4dToPose( Eigen::Matrix4d &m, geometry_msgs::Pose &p )
      {
        Eigen::Quaternion<double> q( m.block<3,3>(0,0) );
        p.orientation.x = q.x();
        p.orientation.y = q.y();
        p.orientation.z = q.z();
        p.orientation.w = q.w();

        p.position.x = m(0,3);
        p.position.y = m(1,3);
        p.position.z = m(2,3);
      };

      static void tfToPose( tf::StampedTransform &t, geometry_msgs::Pose &p )
      {
        p.orientation.x = t.getRotation().getAxis().getX();
        p.orientation.y = t.getRotation().getAxis().getY();
        p.orientation.z = t.getRotation().getAxis().getZ();
        p.orientation.w = t.getRotation().getW();

        p.position.x = t.getOrigin().getX();
        p.position.y = t.getOrigin().getY();
        p.position.z = t.getOrigin().getZ();
      };

      static void poseToMat4d( geometry_msgs::Pose &p, Eigen::Matrix4d &m )
      {
        // create the quaternion and normalize it
        Eigen::Quaternion<double> q( p.orientation.w, p.orientation.x, p.orientation.y, p.orientation.z );
        q.normalize();
        Eigen::Matrix<double, 3, 1> t;
        t(0, 0) = p.position.x;
        t(1, 0) = p.position.y;
        t(2, 0) = p.position.z;
        Eigen::Matrix<double, 1, 4> a;
        a(0, 0) = 0;
        a(0, 1) = 0;
        a(0, 2) = 0;
        a(0, 3) = 1;
        m.block<3,3>(0,0) = q.toRotationMatrix();
        m.block<3,1>(0,3) = t;
        m.block<1,4>(3,0) = a;
      };

      inline void setTwist( double x, double y, double z, double r, double p, double yaw, geometry_msgs::Twist &t )
      {
        t.linear.x = x;
        t.linear.y = y;
        t.linear.z = z;

        t.angular.x = r;
        t.angular.y = p;
        t.angular.z = yaw;
      };

      inline void zeroTwist( geometry_msgs::Twist &t )
      {
        setTwist( 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, t );
      };

      inline void setXTheta( double x, double theta, geometry_msgs::Twist &t )
      {
        setTwist( x, 0.0, 0.0, 0.0, 0.0, theta, t );
      };

      inline bool withinDistanceThresh( geometry_msgs::Point &p1, geometry_msgs::Point &p2, double d )
      {
        if( abs( p1.x - p2.x ) > d )
          return false;
        if( abs( p1.y - p2.y ) > d )
          return false;
        if( abs( p1.z - p2.z ) > d )
          return false;

        return true;
      };

      inline double calcDist( double x, double y, double z )
      {
        return sqrt( x*x + y*y + z*z );
      };

    private:
      void initParams();

      ros::NodeHandle nh_;
      tf::TransformListener tf_listener_;

      void pathCb( const nav_msgs::Path &m );
      ros::Subscriber path_sub_;

      std::string map_frame_;
      std::string base_frame_;
 
      ///////////////////////////////////////////////////////////////////////////
      // Communication pieces between control loop and path subscriber
      ///////////////////////////////////////////////////////////////////////////
      boost::mutex cur_path_mtx_;
      bool new_path_rdy_;
      nav_msgs::Path cur_path_;
      int cur_index_;
      ///////////////////////////////////////////////////////////////////////////

      ///////////////////////////////////////////////////////////////////////////
      // Dynamic Obstacle
      ///////////////////////////////////////////////////////////////////////////
      void obstacleCb( pcl::PCLPointCloud2::ConstPtr cloud );
      ros::Subscriber obstacle_sub_;
      Eigen::MatrixXd obstacles_;
      int num_obstacles_;
      boost::mutex obstacle_lock_;
      ///////////////////////////////////////////////////////////////////////////

      ///////////////////////////////////////////////////////////////////////////
      // Joystick Control 
      ///////////////////////////////////////////////////////////////////////////
      void joyCb( const sensor_msgs::Joy &j );
      ros::Subscriber joy_sub_;
      ros::Time last_joy_time_;
      bool enable_joystick_;
      bool enable_controller_;
      geometry_msgs::Twist joy_cmd_;
      boost::mutex joy_lock_;
      ///////////////////////////////////////////////////////////////////////////


      ///////////////////////////////////////////////////////////////////////////
      // Robot state info -- only control loop writes to it
      ///////////////////////////////////////////////////////////////////////////
      boost::mutex cur_location_mtx_;
      geometry_msgs::PoseStamped cur_location_;
      geometry_msgs::PoseStamped cur_goal_;
      ///////////////////////////////////////////////////////////////////////////

      
      void controlLoop();
      geometry_msgs::Twist prev_cmd_;
      geometry_msgs::Twist cur_cmd_;
      ros::Publisher mode_pub_;
      ros::Publisher cmd_pub_;
      ros::Publisher cmd_pub_vis_;
      geometry_msgs::WrenchStamped cur_cmd_vis_;
      boost::thread control_thread_;
      int num_look_ahead_frames_;
      double control_freq_; /// < Hz to run the controller at
      double max_speed_; /// < Max magnitude of the twist ( norm([x,y,z,r,p,yaw]) )
      double tracking_distance_; /// < Distance away from each waypoint before it is considered completed
      double stop_distance_; /// < Distance away from a waypoint before the robot stops -- should be less than the tracking distance

      double tf_fudge_factor_; /// < Acceptable timing offset when looking up current location
  };
}

#endif
