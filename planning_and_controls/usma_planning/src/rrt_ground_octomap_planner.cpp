#include <usma_planning/rrt_ground_octomap_planner.h>

namespace usma
{
  RRTGroundOctomapPlanner::RRTGroundOctomapPlanner()
  {
    init();
  };

  RRTGroundOctomapPlanner::RRTGroundOctomapPlanner( ros::NodeHandle& n )
  {
    init();
  };

  RRTGroundOctomapPlanner::RRTGroundOctomapPlanner(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n)
  {
    init();
  };

  RRTGroundOctomapPlanner::~RRTGroundOctomapPlanner()
  {
  };


  bool RRTGroundOctomapPlanner::genPlan( boost::shared_ptr<octomap::OcTree> octree, Eigen::Matrix4d start, Eigen::Matrix4d end, nav_msgs::Path &path, std_msgs::Header &h )
  {
    if( path.poses.size() > 0 )
    {
      ROS_INFO("[RRTGroundOctomapPlanner]::Checking current path for errors");
      bool current_path_fails = false;
      for( int i = 0; i < path.poses.size(); i++ )
      {
        Eigen::Matrix4d transform = Eigen::Matrix4d::Identity();
        transform(0,3) = path.poses[i].pose.position.x;
        transform(1,3) = path.poses[i].pose.position.y;
        transform(2,3) = path.poses[i].pose.position.z;

        double collision_prob = CollisionUtils::checkCollision( transform, collision_points_, octree, CollisionUtils::LINE_SCAN );
        if ( 1.0 < collision_prob )
        {
          ROS_ERROR("Collision Probability of %f aborting", collision_prob);
          current_path_fails = true;
          break;
        }
      }

      if( !current_path_fails )
      {
        ROS_INFO("[RRTGroundOctomapPlanner]::No Errors on current path");
        return false; // no need to update the plan
      }
      else
      {
        ROS_ERROR("[RRTGroundOctomapPlanner]::Current path failed -- sending null path");
        path.poses.clear();
        path.header = h;
        geometry_msgs::PoseStamped a1;
        a1.header = h;
        mat4dToPose( start, a1.pose );
        path.poses.push_back( a1 );
        return true;
      }
    }
    int count = 0;
    bool path_found = false;

    std::vector<NODE> node_list;
    NODE s;
    s.x = start(0,3);
    s.y = start(1,3);
    s.theta = start.block<3,3>(0,0).eulerAngles(0,1,2)(2);

    node_list.push_back( s );

    NODE e;
    e.x = end(0,3);
    e.y = end(1,3);
    e.theta = end.block<3,3>(0,0).eulerAngles(0,1,2)(2);

    double current_distance_from_end = dist( s.x, s.y, e.x, e.y );
    NODE closest_n;
    closest_n = s;

    ROS_INFO("[RRTGroundOctomapPlanner]::Using %d threads", RRT_GROUND_NUM_THREADS);
    ROS_INFO("[RRTGroundOctomapPlanner]::Max iterations %d", num_iters_);
    while( count < num_iters_ && !path_found )
    {
      std::vector<NODE> loop_nodes;
      loop_nodes.resize(RRT_GROUND_NUM_THREADS);
      std::vector<bool> valid_nodes;
      valid_nodes.resize(RRT_GROUND_NUM_THREADS);
      // parallelize the generation of nodes to add and validate their positioning
#pragma omp parallel num_threads(RRT_GROUND_NUM_THREADS)
      {
        // generate a random node
        NODE t;
        t.x = fRand( std::min(e.x,closest_n.x)-search_size_, std::max(e.x,closest_n.x)+search_size_ );
        t.y = fRand( std::min(e.y,closest_n.y)-search_size_, std::max(e.y,closest_n.y)+search_size_ );
        t.theta = 0.0;
        // determine which node is closest
        double min_dist = std::numeric_limits<double>::max();
        int closest = 0;
        for( int i = 0; i < node_list.size(); i++ )
        {
          double cur_dist = dist( t.x, t.y, node_list[i].x, node_list[i].y );
          if( min_dist > ( cur_dist ) )
          {
            min_dist = cur_dist;
            closest = i;
          }
        }

        // now that we know what node the random node is closest to--use it to drive the direction of the node that will be added
        double rot_node_to_rand = atan2( ( t.y - node_list[closest].y ), ( t.x - node_list[closest].x ) );
        t.x = node_list[closest].x + cos(rot_node_to_rand) * step_size_;
        t.y = node_list[closest].y + sin(rot_node_to_rand) * step_size_;
        t.parent = closest;
        loop_nodes[ omp_get_thread_num() ] = t;

        Eigen::Matrix4d transform = Eigen::Matrix4d::Identity();
        transform(0,3) = t.x;
        transform(1,3) = t.y;

        if ( 0.5 > CollisionUtils::checkCollision( transform, collision_points_, octree, CollisionUtils::LINE_SCAN ) )
          valid_nodes[ omp_get_thread_num() ] = true;
        else
          valid_nodes[ omp_get_thread_num() ] = false;
      }

      // serial adding them to the full vector of nodes
      for( int i = 0; i < loop_nodes.size(); i++ )
      {
        if( !(valid_nodes[i]) )
          continue;

        double cur_dist = dist( loop_nodes[i].x, loop_nodes[i].y, e.x, e.y );

        if( cur_dist < current_distance_from_end )
        {
          closest_n = loop_nodes[i];
          current_distance_from_end = cur_dist;
        }

        node_list[ loop_nodes[i].parent ].children.push_back( node_list.size() );
        node_list.push_back( loop_nodes[i] );

        if( cur_dist < step_size_ )
        {
          path_found = true;
          break; // break out so that the last node in the list is the end of the computed path
        }
      }

      count += RRT_GROUND_NUM_THREADS;
    }

    if( path_found )
    {
      ROS_INFO("[RRTGroundOctomapPlanner]::Found solution in %d rounds", count);

      // trace from solution up to the start
      int cid = node_list.size()-1;
      int pid = node_list[0].parent;

      path.poses.clear();

      path.header = h;
      geometry_msgs::PoseStamped a1;
      a1.header = h;
      mat4dToPose( end, a1.pose );
      path.poses.push_back( a1 );

      while( cid != pid )
      {
        geometry_msgs::PoseStamped p;
        p.header = h;
        p.pose.position.x = node_list[cid].x;
        p.pose.position.y = node_list[cid].y;
        path.poses.push_back( p );
        cid = node_list[cid].parent;
      }
     
      // put them in the correct order
      std::reverse(path.poses.begin(),path.poses.end());

      return true;
    }
    else
    {
      path.poses.clear();
      path.header = h;
      geometry_msgs::PoseStamped a1;
      a1.header = h;
      mat4dToPose( start, a1.pose );
      path.poses.push_back( a1 );
      return true;
    }

  }

  void RRTGroundOctomapPlanner::init()
  {
    nh_.param<double>("/usma_planning/rrt/step_size", step_size_, 0.1);
    nh_.param<double>("/usma_planning/rrt/search_size", search_size_, 10.0);
    nh_.param<int>("/usma_planning/rrt/num_iters", num_iters_, 1000);
    srand (time(NULL));
    //CollisionUtils::genRectPrism( 0.1, 0.1, 0.5, 0.5, collision_points_ );
    collision_points_.resize(240);
    int rad_step = 240;
    for( int rad = 0; rad < 1; rad++ )
    {
    for(int i = 0; i < rad_step; i++)
    {
      collision_points_[rad_step*rad + i].setZero();
      collision_points_[rad_step*rad + i](0) = (0.4 + (double)rad * 0.025) * sin( (double)(i) / 40.0 * M_PI );
      collision_points_[rad_step*rad + i](1) = (0.4 + (double)rad * 0.025) * cos( (double)(i) / 40.0 * M_PI );
      collision_points_[rad_step*rad + i](2) = 0.15;
      collision_points_[rad_step*rad + i](3) = 1.0;
    }
    }

  }
}
