#include <usma_planning/octomap_planner.h>

namespace usma
{
  OctomapPlanner::OctomapPlanner()
  {
    ros::NodeHandle nh;
    initialize( std::string("/octomap_full"), std::string("/move_base_simple/goal"), std::string("/planned_path"), std::string(""), nh );
  };

  OctomapPlanner::OctomapPlanner( ros::NodeHandle& n )
  {
    initialize( std::string("/octomap_full"), std::string("/move_base_simple/goal"), std::string("/planned_path"), std::string(""), n );
  };

  OctomapPlanner::OctomapPlanner(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n)
  {
    initialize( octomap_topic, nav_goal_topic, path_topic, tf_namespace, n );
  };

  OctomapPlanner::~OctomapPlanner()
  {
    octomap_sub_.shutdown();
    goal_sub_.shutdown();

    path_pub_.shutdown();
  };

  void OctomapPlanner::initialize(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n)
  {
    nh_ = n;
    new_data_rdy_ = false;
    alive_ = true;
    octomap_sub_ = nh_.subscribe(octomap_topic, 5, &OctomapPlanner::octomapCb, this);
    goal_sub_ = nh_.subscribe(nav_goal_topic, 5, &OctomapPlanner::goalCb, this);

    path_pub_ = nh_.advertise< nav_msgs::Path >( path_topic, 10 );

    init();

    planning_thread_ = boost::thread( &OctomapPlanner::planningThread, this );
  }

  void OctomapPlanner::octomapCb( const octomap_msgs::Octomap::ConstPtr& msg )
  {
    ROS_INFO("[OCTOMAP PLANNING]::New map recieved");
    new_data_mtx_.lock();
    octomap::AbstractOcTree* tree = octomap_msgs::msgToMap( *(msg) );
    octree_.reset( dynamic_cast<octomap::OcTree*>(tree) );
    octomap_frame_ = msg->header.frame_id;
    new_data_rdy_ = true;
    new_data_mtx_.unlock();
    planning_cv_.notify_one();
  }

  void OctomapPlanner::goalCb( const geometry_msgs::PoseStamped& msg )
  {
    ROS_INFO("[OCTOMAP PLANNING]::New goal recieved");
    new_data_mtx_.lock();
    goal_ = msg;
    new_goal_data_rdy_ = true;
    new_data_mtx_.unlock();
    planning_cv_.notify_one();
  }

  // base class doesn't provide a very useful implementation of this
  bool OctomapPlanner::genPlan( boost::shared_ptr<octomap::OcTree> octree, Eigen::Matrix4d start, Eigen::Matrix4d end, nav_msgs::Path &path, std_msgs::Header &h )
  {
    path.header = h;
    geometry_msgs::PoseStamped a1;
    a1.header.seq = path.header.seq + 1;
    a1.header.frame_id = path.header.frame_id;
    mat4dToPose( start, a1.pose ); 
    a1.pose.position.z = 0.0;

    geometry_msgs::PoseStamped a2;
    a2.header.seq = path.header.seq + 2;
    a2.header.frame_id = path.header.frame_id;
    mat4dToPose( end, a2.pose ); 

    path.poses.push_back( a1 );
    path.poses.push_back( a2 );

    return true;
  }

  void OctomapPlanner::init()
  {
  }

  void OctomapPlanner::planningThread()
  {
    ROS_INFO( "[OCTOMAP PLANNING]::Planning thread started" );
    while( alive_ )
    {
      boost::unique_lock<boost::mutex> l(pcv_mtx_);
      while( alive_ && !new_data_rdy_ && !new_goal_data_rdy_ )
      {
        planning_cv_.wait(l);
      }
      ROS_INFO("[OCTOMAP PLANNING]::Got new information for static plan");
      if( alive_ )
      {
        if( goal_.header.frame_id == "" )
        {
          new_data_mtx_.lock();
          new_data_rdy_ = false;
          new_goal_data_rdy_ = false;
          new_data_mtx_.unlock();
          continue;
        }

        // FOR TESTING IN SIM ONLY!!!!!
        if ( octomap_frame_ == "" )
        {
          octomap_frame_ = "/odom";
        }

        ROS_INFO( "[OCTOMAP PLANNING]::Looking up transforms" );
        tf::StampedTransform TmapToBase, TmapToGoalFrame;
        try
        {
          tf_listener_.lookupTransform(octomap_frame_, "/base_link", ros::Time(0), TmapToBase);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("[OCTOMAP PLANNING]::OCTREE TO BASE LINK %s",ex.what());
          new_data_mtx_.lock();
          new_data_rdy_ = false;
          new_goal_data_rdy_ = false;
          new_data_mtx_.unlock();
          continue;
        }

        try
        {
          tf_listener_.lookupTransform(octomap_frame_, goal_.header.frame_id, ros::Time(0), TmapToGoalFrame);
        }
        catch (tf::TransformException ex)
        {
          ROS_ERROR("[OCTOMAP PLANNING]::OCTREE TO GOAL FRAME %s",ex.what());
          new_data_mtx_.lock();
          new_data_rdy_ = false;
          new_goal_data_rdy_ = false;
          new_data_mtx_.unlock();
          continue;
        }

        if( new_goal_data_rdy_ )
          current_path_.poses.clear();

        // get all frames to give to genPlan into the octomap base frame
        Eigen::Matrix4d mapToBase, mapToGoalFrame, goalFrameToGoal;
        tfToMat4d( TmapToBase, mapToBase );
        tfToMat4d( TmapToGoalFrame, mapToGoalFrame );
        poseToMat4d( goal_.pose, goalFrameToGoal );

        ros::Time t1 = ros::Time::now();
        ROS_INFO( "[OCTOMAP PLANNING]::Generating plan" );
        new_data_mtx_.lock();
        if( genPlan( octree_, mapToBase, goalFrameToGoal * mapToGoalFrame, current_path_, goal_.header ) )
        {
          ros::Time t2 = ros::Time::now();
          if( current_path_.poses.size() == 1 )
            ROS_ERROR( "[OCTOMAP PLANNING]::Current path failed -- give new goal" );
          else
            ROS_INFO( "[OCTOMAP PLANNING]::Plan generation time %f sec", (t2-t1).toSec() );
          path_pub_.publish( current_path_ );
        }
        else
        {
          ros::Time t2 = ros::Time::now();
          if( current_path_.poses.size() == 0)
            ROS_ERROR( "[OCTOMAP PLANNING]::Plan generation failed in %f sec", (t2-t1).toSec() );
          else
            ROS_INFO("[OCTOMAP PLANNING]::Continuing old path");
        }
        new_data_mtx_.unlock();

        new_data_rdy_ = false;
        new_goal_data_rdy_ = false;

      }
    }
    ROS_INFO( "[OCTOMAP PLANNING]::Planning thread ended" );
  }
}
