#include <ros/ros.h>

#include <usma_planning/octomap_planner.h>
#include <usma_planning/rrt_ground_octomap_planner.h>
#include <usma_planning/bi_rrt_ground_octomap_planner.h>

int main( int argc, char** argv )
{
    ros::init(argc, argv, "octomap_planning");

    ros::NodeHandle n;


    std::string planner_type;
    n.param<std::string>("/usma_planning/planner_type", planner_type, "straight_line");

    ros::AsyncSpinner spinner(8);
    spinner.start();

    ROS_INFO("[OCTOMAP PLANNING NODE]::Loading %s", planner_type.c_str());
    if( planner_type == "bi_rrt_ground" )
    {
        usma::BIRRTGroundOctomapPlanner* uop;
        uop = new usma::BIRRTGroundOctomapPlanner( n );
        ros::waitForShutdown();
    }
    else if( planner_type == "rrt_ground" )
    {
        usma::RRTGroundOctomapPlanner* uop;
        uop = new usma::RRTGroundOctomapPlanner( n );
        ros::waitForShutdown();
    }
    else if( planner_type == "basic" )
    {
        usma::OctomapPlanner* uop;
        uop = new usma::OctomapPlanner( n );
        ros::waitForShutdown();
    }
    return 0;
}
