#ifndef OCTOMAP_PLANNER_H_
#define OCTOMAP_PLANNER_H_

#include <ros/ros.h>

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>

// octomap specifics
#include <octomap_ros/conversions.h>
#include <octomap_msgs/Octomap.h>
#include <octomap_msgs/conversions.h>
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <usma_planning/collision_utils.h>

namespace usma
{
  struct NODE
  {
    int parent;
    std::vector<int> children;

    double x;
    double y;
    double theta;
  };

  class OctomapPlanner
  {
    public:
      OctomapPlanner();

      OctomapPlanner( ros::NodeHandle& n );

      OctomapPlanner(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n);

      ~OctomapPlanner();

      static void tfToMat4d( tf::StampedTransform &t, Eigen::Matrix4d &m )
      {
        Eigen::Quaternion<double> q( t.getRotation().getW(), t.getRotation().getAxis().getX(), t.getRotation().getAxis().getY(), t.getRotation().getAxis().getZ() );
        Eigen::Matrix<double, 3, 1> trans;
        trans(0, 0) = t.getOrigin().getX();
        trans(1, 0) = t.getOrigin().getY();
        trans(2, 0) = t.getOrigin().getZ();
        Eigen::Matrix<double, 1, 4> a;
        a(0, 0) = 0;
        a(0, 1) = 0;
        a(0, 2) = 0;
        a(0, 3) = 1;
        m.block<3,3>(0,0) = q.toRotationMatrix();
        m.block<3,1>(0,3) = trans;
        m.block<1,4>(3,0) = a;
      }

      static void poseToMat4d( geometry_msgs::Pose &p, Eigen::Matrix4d &m )
      {
        // create the quaternion and normalize it
        Eigen::Quaternion<double> q( p.orientation.w, p.orientation.x, p.orientation.y, p.orientation.z );
        q.normalize();
        Eigen::Matrix<double, 3, 1> t;
        t(0, 0) = p.position.x;
        t(1, 0) = p.position.y;
        t(2, 0) = p.position.z;
        Eigen::Matrix<double, 1, 4> a;
        a(0, 0) = 0;
        a(0, 1) = 0;
        a(0, 2) = 0;
        a(0, 3) = 1;
        m.block<3,3>(0,0) = q.toRotationMatrix();
        m.block<3,1>(0,3) = t;
        m.block<1,4>(3,0) = a;
      };

      static void mat4dToPose( Eigen::Matrix4d &m, geometry_msgs::Pose &p )
      {
        Eigen::Quaternion<double> q( m.block<3,3>(0,0) );
        p.orientation.x = q.x();
        p.orientation.y = q.y();
        p.orientation.z = q.z();
        p.orientation.w = q.w();

        p.position.x = m(0,3);
        p.position.y = m(1,3);
        p.position.z = m(2,3);
      };

      static void xyzrpyToMat4d( double x, double y, double z, double r, double p, double yaw, Eigen::Matrix4d &m )
      {
        Eigen::AngleAxisd rollAngle(r, Eigen::Vector3d::UnitX());
        Eigen::AngleAxisd yawAngle(yaw, Eigen::Vector3d::UnitY());
        Eigen::AngleAxisd pitchAngle(p, Eigen::Vector3d::UnitZ());

        Eigen::Quaternion<double> q = rollAngle * yawAngle * pitchAngle;

        // create the quaternion and normalize it
        Eigen::Matrix<double, 3, 1> t;
        t(0, 0) = x;
        t(1, 0) = y;
        t(2, 0) = z;
        Eigen::Matrix<double, 1, 4> a;
        a(0, 0) = 0;
        a(0, 1) = 0;
        a(0, 2) = 0;
        a(0, 3) = 1;
        m.block<3,3>(0,0) = q.toRotationMatrix();
        m.block<3,1>(0,3) = t;
        m.block<1,4>(3,0) = a;
      };

    protected:
      /** \brief Generate the plan from the octree, start and end. By the time they reach here, they are all in the same frame.
        * \param octree passes a pointer to the most recent octree kept
        * \param start passes the current location of the robot
        * \param end passes the goal location that we want to get to
        * \param path this is the return variable for the full generated path
        * \return whether or not there was success generating the path
        */
      virtual bool genPlan( boost::shared_ptr<octomap::OcTree> octree, Eigen::Matrix4d start, Eigen::Matrix4d end, nav_msgs::Path &path, std_msgs::Header &h );

      /** \brief Use this to initialize anything specific to the subclass.
        */
      virtual void init();
      ros::NodeHandle nh_;

    private:
      void initialize(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n);

      void octomapCb( const octomap_msgs::Octomap::ConstPtr &msg );
      boost::shared_ptr<octomap::OcTree> octree_;
      std::string octomap_frame_;
      
      void goalCb( const geometry_msgs::PoseStamped &msg );
      geometry_msgs::PoseStamped goal_;

      tf::TransformListener tf_listener_;
      ros::Subscriber octomap_sub_;
      ros::Subscriber goal_sub_;

      ros::Publisher path_pub_;

      nav_msgs::Path current_path_;

      void planningThread();
      boost::thread planning_thread_;
      boost::condition_variable planning_cv_;
      boost::mutex pcv_mtx_;
      bool new_data_rdy_;
      bool new_goal_data_rdy_;
      boost::mutex new_data_mtx_;
      bool alive_;

  };
}

#endif
