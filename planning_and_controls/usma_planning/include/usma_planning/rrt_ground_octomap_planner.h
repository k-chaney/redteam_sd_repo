#ifndef RRT_GROUND_OCTOMAP_PLANNER_H_
#define RRT_GROUND_OCTOMAP_PLANNER_H_

#include <omp.h>
#include <usma_planning/octomap_planner.h>
#include <time.h>
#include <limits>

#define RRT_GROUND_NUM_THREADS 2

namespace usma
{
  class RRTGroundOctomapPlanner : public OctomapPlanner
  {
    public:
      RRTGroundOctomapPlanner();

      RRTGroundOctomapPlanner( ros::NodeHandle& n );

      RRTGroundOctomapPlanner(std::string octomap_topic, std::string nav_goal_topic, std::string path_topic, std::string tf_namespace, ros::NodeHandle& n);

      ~RRTGroundOctomapPlanner();


      /** \brief Generate the plan from the octree, start and end. By the time they reach here, they are all in the same frame.
        * \param octree passes a pointer to the most recent octree kept
        * \param start passes the current location of the robot
        * \param end passes the goal location that we want to get to
        * \param path this is the return variable for the full generated path
        * \return whether or not there was success generating the path
        */
      bool genPlan( boost::shared_ptr<octomap::OcTree> octree, Eigen::Matrix4d start, Eigen::Matrix4d end, nav_msgs::Path &path, std_msgs::Header &h );

      /** \brief Use this to initialize anything specific to the subclass.
        */
      void init();

      static double fRand(double fMin, double fMax)
      {
        double f = (double)rand() / RAND_MAX;
        return fMin + f * (fMax - fMin);
      };

      static double dist( double x1, double y1, double x2, double y2 )
      {
        return sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) );
      };
    private:
      double step_size_;
      double search_size_;
      int num_iters_;
      std::vector<Eigen::Matrix<double,4,1> > collision_points_;
  };
}

#endif
