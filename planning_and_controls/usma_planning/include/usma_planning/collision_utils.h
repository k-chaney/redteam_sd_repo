#ifndef USMA_COLLISION_UTILS_H_
#define USMA_COLLISION_UTILS_H_

#include <ros/ros.h>

#include <boost/thread.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>

#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <tf/transform_listener.h>

// octomap specifics
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace usma
{
  class CollisionUtils
  {
    public:
      enum COLLISION_CHECKERS { NONE, HEIGHT, LINE_SCAN };

      static double checkCollision( Eigen::Matrix4d &t, std::vector<Eigen::Matrix<double, 4, 1> > &cp, boost::shared_ptr<octomap::OcTree> &ot, COLLISION_CHECKERS type )
      {
        double collision_prob = 0.0;
        switch( type )
        {
          case NONE : collision_prob = 0.0; break; 
          case HEIGHT : collision_prob = relativeHeightChecker( t, cp, ot ); break;
          case LINE_SCAN : collision_prob = lineScan( t, cp, ot ); break;
        }

        return collision_prob;
      };

      static double lineScan( Eigen::Matrix4d &t, std::vector<Eigen::Matrix<double, 4, 1> > &cp, boost::shared_ptr<octomap::OcTree> &ot )
      {
        Eigen::Matrix<double,4,1> nt;

        octomap::OcTreeNode* res;

        double collision_prob = 0.0;
        double x, y, z, zprob;
        for( int i = 0; i < cp.size(); i++ )
        {
          nt = t * cp[i];
          x = nt(0);
          y = nt(1);
          z = 0.5;
          zprob = 0.0;
          for( int j = -1; j <= 3; j++ )
          {
            res = ot->search( x, y, (double)( j ) * ot->getResolution() * 2 + z, ot->getTreeDepth() - 2 );
            if( res != NULL )
              zprob += res->getOccupancy();
          }
          zprob /= 3.0;
          if( zprob > collision_prob )
            collision_prob = zprob;
        }

        return collision_prob;
      };

      static double relativeHeightChecker( Eigen::Matrix4d &t, std::vector<Eigen::Matrix<double, 4, 1> > &cp, boost::shared_ptr<octomap::OcTree> &ot )
      {
        double r = ot->getResolution();

        // generate map of grid points to check
        std::map<std::pair<int,int>, int > height_map;
        std::map<std::pair<int,int>, int >::iterator height_map_it;
        Eigen::Matrix<double, 4, 1> tmp_point;
        for( int i = 0; i < cp.size(); i++ )
        {
          tmp_point = t*cp[i];
          height_map.insert( std::make_pair(std::make_pair((int)tmp_point(0)/r, (int)tmp_point(1)/r), (int)tmp_point(2)/r) );
        }

        double minx;
        double miny;
        double minz;
        double maxx;
        double maxy;
        double maxz;
        
        ot->getMetricMax(maxx, maxy, maxz);
        ot->getMetricMin(minx, miny, minz);

        for( height_map_it = height_map.begin(); height_map_it != height_map.end(); ++height_map_it )
        {
          for( double z = maxz; z > minz; z-=r)
          {
          }
        }

      };

      
      static void genSphere( double theta_resolution, double phi_resolution, double radius, std::vector<Eigen::Matrix<double, 4, 1> > &rp)
      {
        for( double theta = -M_PI; theta <= M_PI; theta+=theta_resolution )
        {
          for( double phi = -M_PI; phi <= M_PI; phi+=phi_resolution )
          {
            rp.resize( rp.size() + 1 );
            rp[rp.size()-1](0) = radius*sin(theta)*cos(phi); // x
            rp[rp.size()-1](1) = radius*sin(theta)*sin(phi); // y
            rp[rp.size()-1](2) = radius*cos(theta); // z
            rp[rp.size()-1](3) = 1.0; // w
          }
        }
        return;
      };

      static void genRectPrism( double resolution, double height, double width, double depth, std::vector<Eigen::Matrix<double, 4, 1> > &rp)
      {
        for( double h=-(height/2); h<=(height/2); h+=resolution )
        {
          for( double w=-(width/2); w<=(width/2); w+=resolution )
          {
            for( double d=-(depth/2); d<=(depth/2); d+=resolution )
            {
              rp.resize( rp.size() + 1 );
              rp[rp.size()-1](0) = w; // x
              rp[rp.size()-1](1) = d; // y
              rp[rp.size()-1](2) = h; // z
              rp[rp.size()-1](3) = 1.0; // w
            }
          }
        }
      };

  };
}

#endif
