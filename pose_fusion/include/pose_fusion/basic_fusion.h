#ifndef USMA_BASIC_POSE_FUSION_H_
#define USMA_BASIC_POSE_FUSION_H_

#include <ros/ros.h>
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <geometry_msgs/Quaternion.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/math/special_functions/sign.hpp>
#include <limits>

#include <tf/transform_broadcaster.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <deque>

namespace usma
{
  class BasicFusion
  {
    public:
      typedef Eigen::Matrix<double, 3, 1> SINGLE_STATE;
      typedef Eigen::Matrix<double, 18, 1> FULL_STATE;
      typedef Eigen::Matrix<double, 18, 18> STATE_UPDATER;

      BasicFusion();
      BasicFusion( ros::NodeHandle &n );
      ~BasicFusion();

    private:
      tf::TransformBroadcaster br_;
      ros::Publisher pose_pub_;
      ros::Publisher imu_pub_;

      // IMU PIECES
      void imuSub( const sensor_msgs::Imu::ConstPtr &m, int update_mask, double yaw );
      ros::Subscriber imu_sub_;
      SINGLE_STATE imu_linear_gravity_est_;
      SINGLE_STATE imu_linear_filtered_;
      double lin_alpha_;
      SINGLE_STATE imu_orientation_filtered_;
      double or_alpha_;
      bool imu_init_set_;

      // this filter assumes and corrects for single source constant freq vibration
      // also corrects for a constant bias
      std::deque<SINGLE_STATE> past_imu_readings_;
      std::deque<double> reading_times_;
      SINGLE_STATE amp_; // amplitude of vibration
      SINGLE_STATE amp_offset_; // constant bias offset
      double period_; // period of vibration -- should be the same for all axis
      SINGLE_STATE phase_offset_; // phase offset wrt time 0
      int num_est_samples_;
      bool has_model_;
      // END IMU PIECES


      void odomSub( const nav_msgs::Odometry::ConstPtr &m, int update_mask );
      nav_msgs::Odometry prev_odom_msg_;
      bool prev_odom_set_;
      ros::Subscriber odom_sub_1_;
      ros::Subscriber odom_sub_2_;

      void curVelSub( const geometry_msgs::Twist &m );
      ros::Time last_cmd_recieved_;
      bool is_moving_;
      ros::Subscriber cur_vel_sub_;

      boost::mutex model_mtx_;
      boost::thread model_thread_;
      FULL_STATE internal_state_;
      geometry_msgs::Quaternion cur_orientation_;
      ros::Time last_pose_update_;
      ros::Time last_twist_update_;
      ros::Time last_accel_update_;

      void init( ros::NodeHandle &n );


      void applyDeltaPose( SINGLE_STATE &p, SINGLE_STATE &r, int options = 255 ); // bitmask what to update
      void updatePose ( SINGLE_STATE &p, SINGLE_STATE &r, int options = 255 ); // bitmask what to update
      void updateTwist( SINGLE_STATE &p, SINGLE_STATE &r, int options = 255 );
      void updateAccel( SINGLE_STATE &p, SINGLE_STATE &r, int options = 255 );
      void updateLoop();

      static Eigen::Matrix<double, 3, 3> getRotMat( SINGLE_STATE &r )
      {
        return (Eigen::AngleAxis<double>(r[0], Eigen::Vector3d::UnitX())
              *Eigen::AngleAxis<double>(r[1], Eigen::Vector3d::UnitY())
              *Eigen::AngleAxis<double>(r[2], Eigen::Vector3d::UnitZ())).toRotationMatrix();
      }

      static Eigen::Matrix<double, 3, 3> getRotMat( geometry_msgs::Quaternion &q )
      {
        return Eigen::Quaternion<double>( q.w, q.x, q.y, q.z ).toRotationMatrix();
      }

      static bool updateMin( SINGLE_STATE& min, SINGLE_STATE& n )
      {
        if( n(0) < min(0) )
        {
          min(0) = n(0);
          return true;
        }
        if( n(1) < min(1) )
        {
          min(1) = n(1);
          return true;
        }
        if( n(2) < min(2) )
        {
          min(2) = n(2);
          return true;
        }
        return false;
      }
      static bool updateMax( SINGLE_STATE& max, SINGLE_STATE& n )
      {
        if( n(0) > max(0) )
        {
          max(0) = n(0);
          return true;
        }
        if( n(1) > max(1) )
        {
          max(1) = n(1);
          return true;
        }
        if( n(2) > max(2) )
        {
          max(2) = n(2);
          return true;
        }
        return false;
      }
  };
}

#endif
