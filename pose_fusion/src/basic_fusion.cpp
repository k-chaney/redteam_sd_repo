#include <pose_fusion/basic_fusion.h>

#define SIX_STATE_MASK 0x3F
#define THREE_STATE_MASK 0x07
#define USE_DELTA 0x40000
#define X_MASK 1
#define Y_MASK 2
#define Z_MASK 4
#define XYZ_MASK THREE_STATE_MASK
#define RPY_MASK (THREE_STATE_MASK << 3)
#define D_XYZ_MASK (THREE_STATE_MASK << 6)
#define D_RPY_MASK (THREE_STATE_MASK << 9)
#define DD_XYZ_MASK (THREE_STATE_MASK << 12)


namespace usma
{
  BasicFusion::BasicFusion()
  {
    ros::NodeHandle n;
    init( n );
  }

  BasicFusion::BasicFusion( ros::NodeHandle &n )
  {
    init( n );
  }

  void BasicFusion::init( ros::NodeHandle &n )
  {
    has_model_ = false;
    num_est_samples_ = 100;
    lin_alpha_ = 0.5;
    or_alpha_ = 0.9;
    imu_init_set_ = false;
    prev_odom_set_ = false;

    int i = D_RPY_MASK;// | (Y_MASK << 12);
    double yaw = -M_PI/2.0;
    imu_sub_ = n.subscribe<sensor_msgs::Imu>( "/imu/data", 10, ( boost::bind(&BasicFusion::imuSub, this, _1, i, yaw) ) );

    i = USE_DELTA | XYZ_MASK; // | RPY_MASK;
    //odom_sub_1_ = n.subscribe<nav_msgs::Odometry>( "/ethz_odom_error", 10, ( boost::bind(&BasicFusion::odomSub, this, _1, i) ) );

    i = USE_DELTA | 64 | 1 ;
    odom_sub_2_ = n.subscribe<nav_msgs::Odometry>( "/viso_odom_error", 10, ( boost::bind(&BasicFusion::odomSub, this, _1, i) ) );

    is_moving_ = false;
    cur_vel_sub_ = n.subscribe( "/gvr_bot/cmd_vel", 10, &BasicFusion::curVelSub, this );

    pose_pub_ = n.advertise<geometry_msgs::Pose>("/odometry/filtered", 10);
    imu_pub_ = n.advertise<sensor_msgs::Imu>("/imu/data/filt", 10);

    model_thread_ = boost::thread( &BasicFusion::updateLoop, this );
  }

  BasicFusion::~BasicFusion()
  {
  }

  void BasicFusion::applyDeltaPose( SINGLE_STATE &p, SINGLE_STATE &r, int options  )
  {
    model_mtx_.lock();
    last_pose_update_ = ros::Time::now();

    for(int i = 0; i < 3; i++)
      if( (options >> i) & 1 )
        internal_state_( i, 0 ) += p( i, 0 );

    for(int i = 0; i < 3; i++)
      if( (options >> (3+i)) & 1 )
        internal_state_( 3+i, 0 ) += r( i, 0 );

    model_mtx_.unlock();
  }

  void BasicFusion::updatePose( SINGLE_STATE &p, SINGLE_STATE &r, int options  )
  {
    model_mtx_.lock();
    last_pose_update_ = ros::Time::now();

    for(int i = 0; i < 3; i++)
      if( (options >> i) & 1 )
        internal_state_( i, 0 ) = p( i, 0 );

    for(int i = 0; i < 3; i++)
      if( (options >> (3+i)) & 1 )
        internal_state_( 3+i, 0 ) = r( i, 0 );

    model_mtx_.unlock();
  }

  void BasicFusion::updateTwist( SINGLE_STATE &p, SINGLE_STATE &r, int options  )
  {
    model_mtx_.lock();
    last_twist_update_ = ros::Time::now();

    for(int i = 0; i < 3; i++)
      if( (options >> i) & 1 )
        internal_state_( 6+i, 0 ) = p( i, 0 );

    for(int i = 0; i < 3; i++)
      if( (options >> (3+i)) & 1 )
        internal_state_( 9+i, 0 ) = r( i, 0 );

    model_mtx_.unlock();
  }

  void BasicFusion::updateAccel( SINGLE_STATE &p, SINGLE_STATE &r, int options  )
  {
    model_mtx_.lock();
    last_accel_update_ = ros::Time::now();

    for(int i = 0; i < 3; i++)
      if( (options >> i) & 1 )
        internal_state_( 12+i, 0 ) = p( i, 0 );

    for(int i = 0; i < 3; i++)
      if( (options >> (3+i)) & 1 )
        internal_state_( 15+i, 0 ) = r( i, 0 );

    model_mtx_.unlock();
  }


  void BasicFusion::updateLoop()
  {
    ros::Rate r( 1000.0 ); // request 1000 Hz

    internal_state_ = FULL_STATE::Zero();

    ros::Time pt = ros::Time::now(); 

    int c = 0;

    while( ros::ok() )
    {
      ros::Time ct = ros::Time::now(); 
      is_moving_ = (ct - last_cmd_recieved_).toSec() < 0.1;

      if ( is_moving_ )
      {
        // generate the update matrix
        STATE_UPDATER integral_updater = STATE_UPDATER::Identity();
        double et = ( ct - pt ).toSec(); // compute elapsed time -- not actually 1000 Hz
        for( int i = 0; i < 12; i++ )
        {
          integral_updater( i, i + 6 ) = et;
        } 
        

        model_mtx_.lock();
        internal_state_ = integral_updater * internal_state_;

        for ( int i = 0; i < 3; i++ )
        {
          if( internal_state_( 6+i, 0 ) > 0.1 )
          {
            internal_state_( 6+i, 0 ) = 0.1;
          }
          else if( internal_state_( 6+i, 0 ) < -0.1 )
          {
            internal_state_( 6+i, 0 ) = -0.1;
          }
        }
      }
      else
      {
        for( int i = 7; i < 18; i++)
        {
          internal_state_(i,0) = 0.0;
        }
      }
      
      pt = ct;

      model_mtx_.unlock();

      // create and broadcast the transform
      SINGLE_STATE tmp_r = internal_state_.block<3,1>(3,0);
      Eigen::Matrix<double, 3, 3> tmp_R = getRotMat( tmp_r );
      Eigen::Quaternion<double> qeig( tmp_R );

      tf::Transform transform;

      ROS_INFO_THROTTLE(1, "[XYZ]:::%f:%f:%f", internal_state_(0,0), internal_state_(1,0), internal_state_(2,0) );
      ROS_INFO_THROTTLE(1, "[XYZ]':::%f:%f:%f", internal_state_(6,0), internal_state_(7,0), internal_state_(8,0) );
      ROS_INFO_THROTTLE(1, "[XYZ]'':::%f:%f:%f", internal_state_(12,0), internal_state_(13,0), internal_state_(14,0) );

      ROS_INFO_THROTTLE(1, "[RPY]:::%f:%f:%f", internal_state_(3,0), internal_state_(4,0), internal_state_(5,0) );
      ROS_INFO_THROTTLE(1, "[RPY]':::%f:%f:%f", internal_state_(9,0), internal_state_(10,0), internal_state_(11,0) );
      ROS_INFO_THROTTLE(1, "[RPY]'':::%f:%f:%f", internal_state_(15,0), internal_state_(16,0), internal_state_(17,0) );

      transform.setOrigin( tf::Vector3(internal_state_(0,0), internal_state_(1,0), internal_state_(2,0)) );
      tf::Quaternion qtf( qeig.x(), qeig.y(), qeig.z(), qeig.w() );
      transform.setRotation(qtf);
      br_.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "/odom", "/base_link"));

      geometry_msgs::Pose cur_pose;
      tf::poseTFToMsg( transform, cur_pose );
      pose_pub_.publish(cur_pose);

      r.sleep();
    }
  }

  void BasicFusion::imuSub( const sensor_msgs::Imu::ConstPtr &m, int update_mask, double yaw )
  {
    Eigen::Matrix3d sensor_yaw_mat = Eigen::Matrix3d::Identity();
    sensor_yaw_mat(0,0) = cos(yaw);
    sensor_yaw_mat(0,1) = -sin(yaw);
    sensor_yaw_mat(1,0) = sin(yaw);
    sensor_yaw_mat(1,1) = cos(yaw);

    SINGLE_STATE orientation, angular_velocity, linear_acceleration;
    SINGLE_STATE zero = SINGLE_STATE::Zero();

    geometry_msgs::Quaternion q(m->orientation);
    orientation = (getRotMat( q )).eulerAngles(0,1,2);
    updatePose(zero, orientation, update_mask & RPY_MASK);

    // convert the linear acceleration into global coordinates
    linear_acceleration(0) = ( update_mask & (X_MASK << 12) ) ? m->linear_acceleration.x : 0.0; // local -- this changes
    linear_acceleration(1) = ( update_mask & (Y_MASK << 12) ) ? m->linear_acceleration.y : 0.0; // local -- should be zero
    linear_acceleration(2) = ( update_mask & (Z_MASK << 12) ) ? m->linear_acceleration.z : 0.0; // local -- should be zero

    if( !is_moving_ )
    {
      past_imu_readings_.push_back( linear_acceleration );
      reading_times_.push_back( (m->header.stamp).toSec() );

      // standard curve fittng assuming relatively constant source
      // can move to a least squares method to be more robust but
      // but will have to run less often is this is the case
      //
      // this should be fast enough to run every cycle that we aren't
      // moving. If we are moving then we throw out the collected data
      // to start collecting new data once we stop. This simplifies the
      // computation by maintaining the assumption that this is a continuous
      // data source. Phase offset drift is also accounted for with this.
      if( past_imu_readings_.size() >= num_est_samples_ )
      {
        amp_offset_ = past_imu_readings_.front();
        SINGLE_STATE min = past_imu_readings_.front();
        SINGLE_STATE max = past_imu_readings_.front();
        for( int i = 1; i < past_imu_readings_.size(); i++ )
        {
          amp_offset_ += past_imu_readings_[i];
          updateMin( min, past_imu_readings_[i] );
          updateMax( max, past_imu_readings_[i] );
        }
        amp_offset_ /= (double)past_imu_readings_.size();
        amp_ = (max - min)/2.0;

        std::vector<int> dir_change_inds;
        std::vector<double> dir_change_times;
        for( int i = 1; i < past_imu_readings_.size(); i++ )
        {
          // if the sign of [i-1] and [i] don't match
          if( ( ( Y_MASK << 12 ) && (boost::math::signbit( past_imu_readings_[i - 1](1) - amp_offset_(1) ) ^ boost::math::signbit( past_imu_readings_[i](1) - amp_offset_(1) )) ) )
          {
            dir_change_inds.push_back( i );
            dir_change_times.push_back( (reading_times_[i]) );
          }
        }

        // period is twice the  mean of the first derivative of the dir_change_times vector
        period_ = 0.0;
        for( int i = 1; i < dir_change_times.size(); i++ )
          period_ += ( dir_change_times[i] - dir_change_times[i - 1] );
        period_ = fabs( 2.0 * (period_ / (double) ( dir_change_times.size() - 1 ) ) );
        period_ = 0.095;

        // shouldn't be brute forcing this probably....
        SINGLE_STATE min_error;
        min_error(0) = std::numeric_limits<double>::max();
        min_error(1) = std::numeric_limits<double>::max();
        min_error(2) = std::numeric_limits<double>::max();
        SINGLE_STATE sum_errors_squared;
        SINGLE_STATE error;
        for( double po; po < 2.0 * M_PI; po += 1.0 / 100.0)
        {
          sum_errors_squared = SINGLE_STATE::Zero();
          for( int i = 0; i < past_imu_readings_.size(); i++ )
          {
            error(0) = ( past_imu_readings_[i](0) - (amp_(0) * sin( 2.0 * M_PI / period_ * reading_times_[i] - po ) + amp_offset_(0)) );
            error(1) = ( past_imu_readings_[i](1) - (amp_(1) * sin( 2.0 * M_PI / period_ * reading_times_[i] - po ) + amp_offset_(1)) );
            error(2) = ( past_imu_readings_[i](2) - (amp_(2) * sin( 2.0 * M_PI / period_ * reading_times_[i] - po ) + amp_offset_(2)) );

            error(0) = error(0) * error(0);
            error(1) = error(1) * error(1);
            error(2) = error(2) * error(2);

            sum_errors_squared += error;
          }
          if( sum_errors_squared(0) < min_error(0) )
          {
            min_error(0) = sum_errors_squared(0);
            phase_offset_(0) = po;
          }
          if( sum_errors_squared(1) < min_error(1) )
          {
            min_error(1) = sum_errors_squared(1);
            phase_offset_(1) = po;
          }
          if( sum_errors_squared(2) < min_error(2) )
          {
            min_error(2) = sum_errors_squared(2);
            phase_offset_(2) = po;
          }
        }

        //for( int i = 0; i < past_imu_readings_.size() / 2; i++ )
        {
          past_imu_readings_.pop_front();
          reading_times_.pop_front();
        }
        has_model_ = true;
      }
    }
    else
    {
      past_imu_readings_.clear();
      reading_times_.clear();
    }

    if(has_model_)
    {
      ROS_INFO_THROTTLE(1, "[A]::%f %f %f", amp_(0), amp_(1), amp_(2));
      ROS_INFO_THROTTLE(1, "[AO]::%f %f %f", amp_offset_(0), amp_offset_(1), amp_offset_(2));
      ROS_INFO_THROTTLE(1, "[PO]::%f %f %f", phase_offset_(0), phase_offset_(1), phase_offset_(2));
      ROS_INFO_THROTTLE(1, "[PERIOD]::%f", period_);

      linear_acceleration(0) -= amp_offset_(0);//-= (amp_(0) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(0) ) + amp_offset_(0));
      linear_acceleration(1) -= amp_offset_(1);//-= (amp_(1) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(1) ) + amp_offset_(1));
      linear_acceleration(2) -= amp_offset_(2);//-= (amp_(2) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(2) ) + amp_offset_(2));

      SINGLE_STATE tmp_la;

      tmp_la(0) = (amp_(0) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(0) ) + amp_offset_(0));
      tmp_la(1) = (amp_(1) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(1) ) + amp_offset_(1));
      tmp_la(2) = (amp_(2) * sin( 2.0 * M_PI / period_ * (m->header.stamp).toSec() - phase_offset_(2) ) + amp_offset_(2));


      SINGLE_STATE tmp_o = internal_state_.block<3,1>(3,0);
      linear_acceleration = (sensor_yaw_mat *  getRotMat( tmp_o )) * linear_acceleration;

      if( imu_init_set_ == false )
      {
        imu_init_set_ = true;
        imu_linear_filtered_ = linear_acceleration;
      }
      else
      {
        imu_linear_filtered_ = (lin_alpha_) * imu_linear_filtered_ + (1-lin_alpha_) * linear_acceleration;
      }
      //linear_acceleration = imu_linear_filtered_; 


      updateAccel( linear_acceleration, zero, XYZ_MASK ); // update only the xyz portion

      angular_velocity(0) = m->angular_velocity.x;
      angular_velocity(1) = m->angular_velocity.y;
      angular_velocity(2) = m->angular_velocity.z; 
      updateTwist( zero, angular_velocity, RPY_MASK );

      sensor_msgs::Imu c_imu = *( m );
      c_imu.linear_acceleration.x = linear_acceleration(0);
      c_imu.linear_acceleration.y = linear_acceleration(1);
      c_imu.linear_acceleration.z = linear_acceleration(2);

      c_imu.angular_velocity.x = tmp_la(0);
      c_imu.angular_velocity.y = tmp_la(1);
      c_imu.angular_velocity.z = tmp_la(2);
      imu_pub_.publish( c_imu );
    }

  }

  void BasicFusion::odomSub( const nav_msgs::Odometry::ConstPtr &m, int update_mask )
  {
    SINGLE_STATE pose_p, pose_o, twist_l, twist_a;
    SINGLE_STATE zero = SINGLE_STATE::Zero();
    pose_p(0) = ( update_mask & 1 ) ? m->pose.pose.position.x : 0.0;
    pose_p(1) = ( update_mask & 2 ) ? m->pose.pose.position.y : 0.0;
    pose_p(2) = ( update_mask & 4 ) ? m->pose.pose.position.z : 0.0;

    geometry_msgs::Quaternion q;
    q = m->pose.pose.orientation;
    pose_o = getRotMat( q ).eulerAngles(0,1,2);
    pose_o(0) = ( update_mask & 8 ) ? pose_o(0) : 0.0;
    pose_o(1) = ( update_mask & 16 ) ? pose_o(1) : 0.0;
    pose_o(2) = ( update_mask & 32 ) ? pose_o(2) : 0.0;

    twist_l(0) = ( update_mask & 64 ) ? m->twist.twist.linear.x : 0.0;
    twist_l(1) = ( update_mask & 128 ) ? m->twist.twist.linear.y : 0.0;
    twist_l(2) = ( update_mask & 256 ) ? m->twist.twist.linear.z : 0.0;

    twist_a(0) = ( update_mask & 512 ) ? m->twist.twist.angular.x : 0.0;
    twist_a(1) = ( update_mask & 1024 ) ? m->twist.twist.angular.y : 0.0;
    twist_a(2) = ( update_mask & 2048 ) ? m->twist.twist.angular.z : 0.0;

    SINGLE_STATE tmp_o = internal_state_.block<3,1>(3,0);
    twist_l = getRotMat( tmp_o ) * twist_l;

    if( update_mask & USE_DELTA )
    {
      pose_p = getRotMat( tmp_o ) * pose_p;
      applyDeltaPose( pose_p, pose_o, SIX_STATE_MASK );
    }
    else
    {
      updatePose( pose_p, pose_o, SIX_STATE_MASK );
    }
    updateTwist( twist_l, twist_a, (RPY_MASK || XYZ_MASK) );
  }

  void BasicFusion::curVelSub( const geometry_msgs::Twist &m )
  {
    last_cmd_recieved_ = ros::Time::now();

    //if( fabs(m.linear.x)>0.001 || 
    //    fabs(m.linear.y)>0.001 ||
    //    fabs(m.linear.z)>0.001 ||
    //    fabs(m.angular.x)>0.001 ||
    //    fabs(m.angular.y)>0.001 ||
    //    fabs(m.angular.z)>0.001 )
    //{
    //  is_moving_ = true;
    //}
    //else
    //{
    //  is_moving_ = false;
    //}
  }
} 
