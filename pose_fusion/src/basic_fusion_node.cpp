#include <ros/ros.h>
#include <pose_fusion/basic_fusion.h>

int main( int argc, char** argv )
{
  ros::init( argc, argv, "basic_fusion_node" );

  usma::BasicFusion bf;

  ros::spin();

  return 0;
}
