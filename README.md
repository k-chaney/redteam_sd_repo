# IGVC ARIBO Instructions

## Primary Operation
1. Turn on robot.
2. Add "192.168.0.200 redteam-onboard" to your /etc/hosts
3. Connect to GVR BOT wifi ( password modern0325 )
4. ssh user1@redteam-onboard ( password redteam16 )
5. roslaunch system_launch master.launch
6. Use the wireless xbox controller to control the robot
  - Tap A Button and then use joystick to drive
  - Tap B button to turn on autonomous mode
  
## Debugging
1. From the host computer ( where you ssh'ed from )
  - export ROS_MASTER_URI=http://redteam-onboard:11311
  - export ROS_IP=(your ip address here)
2. Run rviz and look at the outputs of the perception chain
3. Call Ken--number is on the board in the trailer

# Project Overview

This project contains three main (meta)packages.

- Perception (usma_perception)
 - Supports a directed graph based pluggable perception architecture
- Planning and controls
 - Supports planning and validation with a closed loop velocity control
- Pose fusion
 - Supports multi sensor pose fusion to create accurate pose estimate
 
 
# System dependencies
 
## ROS dependencies

- Must install ROS on remote terminal and robot
- ROS Indigo
 - Suggested to get desktop full for all of the convinience features like RVIZ
 - http://http://wiki.ros.org/indigo/Installation/Ubuntu
 - http://wiki.ros.org/ROS/Tutorials/InstallingandConfiguringROSEnvironment
 - ros-indigo-desktop-full
- Velodyne
 - ros-indigo-velodyne
- GVR Bot
 - Install from source and follow instructions
 - http://github.com/westpoint-robotics/gvr_bot

# Scripted Install

- Prereqs:
 - ROS installed
 - catkin_ws setup
 - redteam_sd_repo within the catkin_ws/src folder
- Navigate to system_launch/scripts within the redteam_sd_repo
- Run the redteam_install script
- Do the same for the on board computer for the robot

# To run the system on the robot

- Open four terminals
- Terminal A
  - ssh into robot
  - roscore &
  - roslaunch system_launch master.launch
- Terminal B
  - ssh into robot
  - roslaunch system_launch p_and_c.launch
- Terminal C
  - Setup remote ros core
  - rosrun rviz rviz
  - Add desired topics to display
- Terminal D
  - Setup remote ros core
  - roslaunch system_launch joy.launch
  
This provides all subsystems and two methodologies of using the robot. The "A" button on the xbox controller is the deadman's switch which must be pressed at all times for the robot to move. First, is teleop where you simply use the xbox controller to drive the robot around. Second, is waypoint navigation where you use RViz to select a waypoint to travel to.

# To checkout a new branch

- git fetch
- git checkout master
- git pull
- git checkout -b "branch_name"

# To commit and check-in

- git add [file list]
- git commit -m "[comment]"
- git push -u origin [branch name]