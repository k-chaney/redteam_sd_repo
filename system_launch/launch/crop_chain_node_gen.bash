#!/bin/bash

PID_ARRAY=()

for i in $(seq $1 $2);
do
  sleep 0.5
  rosrun usma_perception_plugins crop_chain_test $i $((i+1)) $i $i &
  PID_ARRAY+=($!)
done
rosrun usma_perception_plugins crop_chain_test $1 $1 $2 $1 &

sleep 100

for i in $(seq $1 $2);
do
  kill ${PID_ARRAY[i]}
done
