#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/octomap/octomap_background_subtraction_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::OctomapBackgroundSubtractionPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void OctomapBackgroundSubtractionPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("octomap_background_subtractor");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("pcl::PCLPointCloud2");
    it.push_back("octomap_msgs::Octomap");
    ot.push_back("pcl::PCLPointCloud2");
  }

  bool OctomapBackgroundSubtractionPlugin::checkInputReady()
  {
    return ( in_cloud_sub_->dataReady() || in_octomap_sub_->dataReady() );
  }

  void OctomapBackgroundSubtractionPlugin::init()
  {
    in_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >(subs_[0]);
    in_octomap_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<octomap_msgs::Octomap::ConstPtr> > >(subs_[1]);
    out_cloud_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
    octree = NULL;
    processOptions();
  }

  void OctomapBackgroundSubtractionPlugin::workLoad()
  {
    if ( new_options_ready_ )
    {
      options_mtx_.lock();
      processOptions();
      options_mtx_.unlock();
      new_options_ready_ = false;
    }
    // saves off the octomap into an internally used structure
    if( in_octomap_sub_->dataReady() )
    {
      if ( octomap_counter % octomap_freq_divider_ == 0 )
      {
        octomap::AbstractOcTree* tree = octomap_msgs::msgToMap( *(in_octomap_sub_->getData()) );

        if ( octree != NULL )
          delete octree;

        if (tree)
          octree = dynamic_cast<octomap::OcTree*>(tree);
      }
      octomap_counter++;
    }

    // check to make sure we have an octree
    if ( octree != NULL && in_cloud_sub_->dataReady() )
    {
      int sd = 0;
      // if nothing is set then search the full depth
      sd = octree->getTreeDepth() - (int)( floor( search_resolution_ / octree->getResolution() ) ); // calculate the serach depth that we want based on the search resolution

      pcl::PCLPointCloud2::ConstPtr ros_ci = in_cloud_sub_->getData();
      pcl::PCLPointCloud2::Ptr ros_co;
      ros_co.reset( new pcl::PCLPointCloud2 );
      pcl::PointCloud<pcl::PointXYZ> pcl_ci;
      pcl::fromPCLPointCloud2( *( ros_ci ), pcl_ci );

      int cloud_size = pcl_ci.size();
      int num_kept = 0;
      int point_step = ros_ci->point_step;
      // search through whole point cloud and get the points that are below the thresh_probability
      octomap::OcTreeNode* res;
      for( int i = 0; i < cloud_size; i++ )
      {
        pcl::PointXYZ p = pcl_ci[i];
        res = octree->search( p.x, p.y, p.z, sd );
        if ( res != NULL )
        {
          if ( res->getOccupancy() < thresh_probability_ )
          {
            ros_co->data.insert( ros_co->data.end(), ros_ci->data.begin()+(i*point_step), ros_ci->data.begin()+((i+1)*point_step) );
            num_kept++;
          }
        }
      }

      ros_co->height = 1;
      ros_co->width = num_kept;
      ros_co->header = ros_ci->header;
      ros_co->fields = ros_ci->fields;
      ros_co->point_step = ros_ci->point_step;
      ros_co->row_step = num_kept*ros_ci->point_step;
      out_cloud_pub_->publish( ros_co );
    }

    return;
  }

  void OctomapBackgroundSubtractionPlugin::processOptions()
  {
    search_resolution_ = getOption<double>("search_resolution", 0.0);
    octomap_freq_divider_ = getOption<int>("octomap_freq_divider", 4);
    thresh_probability_ = getOption<double>("thresh_probability", 0.5);
  }
}
