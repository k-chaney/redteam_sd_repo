#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/octomap/octomap_simple_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::OctomapSimplePlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void OctomapSimplePlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("octomap_simple");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("pcl::PCLPointCloud2");
    ot.push_back("octomap_msgs::Octomap");
  }

  bool OctomapSimplePlugin::checkInputReady()
  {
    return false;
  }

  void OctomapSimplePlugin::init()
  {
    // so we are getting the in and out names from the plugin system, however we only remap then destroy our subscribers
    subs_.pop_back();
    pubs_.pop_back();

    server_.reset( new octomap_server::OctomapServer() );
  }

  void OctomapSimplePlugin::workLoad()
  {
    return;
  }
}
