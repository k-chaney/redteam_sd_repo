#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/octomap/octomap_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::OctomapPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void OctomapPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("octomap");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("pcl::PCLPointCloud2");
    ot.push_back("octomap_msgs::Octomap");
    ot.push_back("visualization_msgs::MarkerArray");
  }

  bool OctomapPlugin::checkInputReady()
  {
    return in_cloud_sub_->dataReady();
  }

  void OctomapPlugin::init()
  {
    octree_ = NULL;
    processOptions(); // get the resolution populated so we can make the octree
    octree_ = new octomap::OcTree( resolution_ );
    processOptions(); // now we can process the options and they will be propegated into the octree

    in_cloud_sub_ = boost::any_cast< boost::shared_ptr< ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >( subs_[0] );
    octomap_pub_ = boost::any_cast< boost::shared_ptr< ros::Publisher > >( pubs_[0] );
    vis_marker_pub_ = boost::any_cast< boost::shared_ptr< ros::Publisher > >( pubs_[1] );
  }

  void OctomapPlugin::workLoad()
  {
    if (new_options_ready_)
    {
      options_mtx_.lock();
      processOptions();
      new_options_ready_ = false;
      options_mtx_.unlock();
    }

    if ( in_cloud_sub_->dataReady() )
    {
      pcl::PCLPointCloud2::ConstPtr ros_ci = in_cloud_sub_->getData();
      pcl::PointCloud<pcl::PointXYZ> pcl_ci;
      pcl::fromPCLPointCloud2( *(ros_ci), pcl_ci );
      ros::Time ctime = ros::Time::now() - ros::Duration(0.1);
      insertCloud( pcl_ci, ctime );

      if( octomap_pub_->getNumSubscribers() )
      {
        octomap_msgs::Octomap map;
        map.header.frame_id = publish_frame_;
        map.header.stamp = ros::Time::now();

        if (octomap_msgs::fullMapToMsg(*octree_, map))
          octomap_pub_->publish(map);
      }
      if( vis_marker_pub_->getNumSubscribers() )
      {
        publishVisualizationMarkers();
      }
    } 

    return;
  }

  void OctomapPlugin::insertCloud( pcl::PointCloud<pcl::PointXYZ> &in, ros::Time &stamp )
  {
    try {
      tf::StampedTransform sensor_to_world;
      tf_listener_.lookupTransform( publish_frame_, sensor_frame_, ros::Time::now() - ros::Duration(0.1), sensor_to_world );
      sensor_origin_ = octomap::pointTfToOctomap( sensor_to_world.getOrigin() );
      sensor_origin_.z() = sensor_origin_.z() + 0.1;
    } catch(tf::TransformException& ex) {
      ROS_ERROR("TF Exception--going with last valid transform--only used for ray projection--won't effect too much--%s -> %s",sensor_frame_.c_str(),publish_frame_.c_str());
    }

    octomap::KeySet free_cells, occupied_cells;
    // loop over all points we want to examin -- keep sets of occupied and free cells
    for ( pcl::PointCloud<pcl::PointXYZ>::const_iterator it = in.begin(); it != in.end(); ++it )
    {
      octomap::point3d p( it->x, it->y, it->z );

      // if less than max range
      if( (sensor_max_range_ < 0.0) || ((p - sensor_origin_).norm() <= sensor_max_range_) )
      {
        // free cells -- compute the ray keys then add them to the free cells set
        if( octree_->computeRayKeys( sensor_origin_, p, key_ray_  ) )
        {
          free_cells.insert( key_ray_.begin(), key_ray_.end() );
        }

        // set up the occupied end point
        octomap::OcTreeKey k;
        if( octree_->coordToKeyChecked( p, k ) )
        {
          occupied_cells.insert( k );
        }
      }
      else
      {
        octomap::point3d new_end = sensor_origin_ + ( p - sensor_origin_ ).normalized() * sensor_max_range_;
        if (octree_->computeRayKeys(sensor_origin_, new_end, key_ray_))
        {
          free_cells.insert( key_ray_.begin(), key_ray_.end() );
        }
      }
    }

    // free cells that aren't also in the occupied cells
    octomap::KeySet::iterator oend = occupied_cells.end();
    octomap::KeySet::iterator fend = free_cells.end();
    for ( octomap::KeySet::iterator it = free_cells.begin(); it != fend; it++ )
      if ( occupied_cells.find( *it ) == oend )
        octree_->updateNode( *it, false );

    // now update all occupied nodes
    for ( octomap::KeySet::iterator it = occupied_cells.begin(); it != oend; it++ )
      octree_->updateNode( *it, true );

    if( compress_map_ == true )
      octree_->prune();

    return;
  }

  void OctomapPlugin::processOptions()
  {      
    compress_map_ = getOption<bool>("compress_map",true);

    world_frame_ = getOption<std::string>("world_frame","map"); // ensure that the incoming point cloud is viable
    publish_frame_ = getOption<std::string>("publish_frame","map"); // ensure that the incoming point cloud is viable
    sensor_frame_ = getOption<std::string>("sensor_frame","velodyne"); // ensure that the incoming point cloud is viable
    resolution_ = getOption<double>("resolution",0.1);

    sensor_hit_ = getOption<double>("sensor_hit",0.7);
    sensor_miss_ = getOption<double>("sensor_miss",0.4);
    sensor_min_ = getOption<double>("sensor_min",0.12);
    sensor_max_ = getOption<double>("sensor_max",0.97);
    sensor_max_range_ = getOption<double>("sensor_max_range",25.0);

    if ( octree_ != NULL )
    {
      octree_->setResolution( resolution_ );
      octree_->setProbHit( sensor_hit_ );
      octree_->setProbMiss( sensor_miss_ );
      octree_->setClampingThresMin( sensor_min_ );
      octree_->setClampingThresMax( sensor_max_ );
      //octree_->setOccupancyThres( 0.5 );

      max_depth_ = octree_->getTreeDepth();
    }

    return;
  }

  void OctomapPlugin::publishVisualizationMarkers()
  {
    visualization_msgs::MarkerArray occupied_nodes_vis;
    occupied_nodes_vis.markers.resize( max_depth_ + 1 );
    int num_leaves = 0;
    for ( octomap::OcTree::iterator it = octree_->begin( max_depth_ ), end = octree_->end(); it != end; ++it )
    {
      if ( octree_->isNodeOccupied( *it ) )
      {
        num_leaves++;
        double x = it.getX();
        double y = it.getY();
        double z = it.getZ();
        double size = it.getSize();

        unsigned idx = it.getDepth();
        ROS_ASSERT( idx < occupied_nodes_vis.markers.size() );

        geometry_msgs::Point cube_center;
        cube_center.x = x;
        cube_center.y = y;
        cube_center.z = z;

        occupied_nodes_vis.markers[idx].points.push_back( cube_center );
        double minX, minY, minZ, maxX, maxY, maxZ;
        octree_->getMetricMin(minX, minY, minZ);
        octree_->getMetricMax(maxX, maxY, maxZ);

        double h = (1.0 - std::min(std::max((cube_center.z-minZ)/ (maxZ - minZ), 0.0), 1.0));
        occupied_nodes_vis.markers[idx].colors.push_back(mapColor(h));
      }
    }

    for ( unsigned i = 0; i < occupied_nodes_vis.markers.size(); ++i)
    {
      double size = octree_->getNodeSize( i );
      occupied_nodes_vis.markers[i].header.frame_id = publish_frame_;
      occupied_nodes_vis.markers[i].header.stamp = ros::Time::now();
      occupied_nodes_vis.markers[i].ns = "map";
      occupied_nodes_vis.markers[i].id = i;
      occupied_nodes_vis.markers[i].type = visualization_msgs::Marker::CUBE_LIST;
      occupied_nodes_vis.markers[i].scale.x = size;
      occupied_nodes_vis.markers[i].scale.y = size;
      occupied_nodes_vis.markers[i].scale.z = size;

      if ( occupied_nodes_vis.markers[i].points.size() > 0 )
        occupied_nodes_vis.markers[i].action = visualization_msgs::Marker::ADD;
      else
        occupied_nodes_vis.markers[i].action = visualization_msgs::Marker::DELETE;
    }

    vis_marker_pub_->publish( occupied_nodes_vis );

    return;
  }

  std_msgs::ColorRGBA OctomapPlugin::mapColor( double h )
  {
    std_msgs::ColorRGBA color;
    color.a = 1.0;
    // blend over HSV-values (more colors)

    double s = 1.0;
    double v = 1.0;

    h -= floor(h);
    h *= 6;
    int i;
    double m, n, f;

    i = floor(h);
    f = h - i;
    if (!(i & 1))
      f = 1 - f; // if i is even
    m = v * (1 - s);
    n = v * (1 - s * f);

    switch (i) {
      case 6:
      case 0:
        color.r = v; color.g = n; color.b = m;
        break;
      case 1:
        color.r = n; color.g = v; color.b = m;
        break;
      case 2:
        color.r = m; color.g = v; color.b = n;
        break;
      case 3:
        color.r = m; color.g = n; color.b = v;
        break;
      case 4:
        color.r = n; color.g = m; color.b = v;
        break;
      case 5:
        color.r = v; color.g = m; color.b = n;
        break;
      default:
        color.r = 1; color.g = 0.5; color.b = 0.5;
        break;
    }

    return color;

  }
}
