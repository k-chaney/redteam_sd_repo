#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/viso_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::LibVisoPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void LibVisoPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("viso");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("image_transport::Camera");
    ot.push_back("nav_msgs::Odometry");
    ot.push_back("nav_msgs::Odometry");
  }

  bool LibVisoPlugin::checkInputReady()
  {
    return im_sub_->dataReady();
  }

  void LibVisoPlugin::init()
  {
    tf_listener_.reset( new tf::TransformListener( ros::Duration(30.0) ) );
    processOptions();

    im_sub_ = boost::any_cast<boost::shared_ptr<ManagedCameraSubscriber> >( subs_[0] );
    odom_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >( pubs_[0] );
    delta_odom_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >( pubs_[1] );

    integrated_pose_.setIdentity();
  }

  void LibVisoPlugin::processMonoParams( VisualOdometryMono::parameters& p )
  {
    p.height = getOption("height", 1.0);
    p.pitch = getOption("pitch", 0.0);
    p.ransac_iters = getOption("ransac_iters", 1000);
    p.inlier_threshold = getOption("inlier_threshold", 1e-5);
    p.motion_threshold = getOption("motion_threshold", 100);
  }

  void LibVisoPlugin::processCommonParams( VisualOdometryMono::parameters& p )
  {
    p.match.nms_n =  getOption("nms_n", 3);
    p.match.nms_tau =  getOption("nms_tau", 50);
    p.match.match_binsize =  getOption("match_binsize", 50);
    p.match.match_radius =  getOption("match_radius", 200);
    p.match.match_disp_tolerance =  getOption("match_disp_tolerance", 2);
    p.match.outlier_disp_tolerance =  getOption("outlier_disp_tolerance", 5);
    p.match.outlier_flow_tolerance =  getOption("outlier_flow_tolerance", 5);
    p.match.multi_stage =  getOption("multi_stage", 1);
    p.match.half_resolution =  getOption("half_resolution", 1);
    p.match.refinement =  getOption("refinement", 1);

    p.bucket.max_features = getOption("max_features", 2);
    p.bucket.bucket_width = getOption("bucket_width", 50.0);
    p.bucket.bucket_height = getOption("bucket_height", 50.0);
  }

  void LibVisoPlugin::processOptions()
  {
    odom_frame_ = getOption("odom_frame", std::string("/odom"));
    base_link_frame_ =  getOption("base_link_frame", std::string("/base_link"));
    camera_frame_ =  getOption("camera_frame", std::string("/camera"));
    publish_tf_ =  getOption("publish_tf", false);

    processCommonParams( vo_params_ );
    processMonoParams( vo_params_ );
  }

  void LibVisoPlugin::workLoad()
  {
    if( new_options_ready_ )
    {
      options_mtx_.lock();
      new_options_ready_ = false;
      processOptions();
      options_mtx_.unlock();
    }

    std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > img_info_pair( im_sub_->getData() );

    bool first = false;


    if ( !visual_odometer_ )
    {

      first = true;

      image_geometry::PinholeCameraModel model;
      model.fromCameraInfo( img_info_pair.second );

      vo_params_.calib.f = model.fx();
      vo_params_.calib.cu = model.cx();
      vo_params_.calib.cv = model.cy();

      visual_odometer_.reset( new VisualOdometryMono( vo_params_ ) );

      if ( img_info_pair.first->header.frame_id != "" && camera_frame_ == "" )
        camera_frame_ = img_info_pair.first->header.frame_id;

      ROS_INFO("Mono odometry initialized%s", genMonoParamString(vo_params_).c_str() );
    }


    uint8_t *image_data;
    int step;
    cv_bridge::CvImageConstPtr cv_ptr;
    if( img_info_pair.first->encoding == sensor_msgs::image_encodings::MONO8)
    {
      image_data = const_cast<uint8_t*>(&(img_info_pair.first->data[0]));
      step = img_info_pair.first->step;
    }
    else
    {
      cv_ptr = cv_bridge::toCvShare(img_info_pair.first, sensor_msgs::image_encodings::MONO8);
      image_data = cv_ptr->image.data;
      step = cv_ptr->image.step[0];
    }
    
    tf::Transform delta_transform;
    int32_t dims[] = {img_info_pair.first->width, img_info_pair.first->height, step};
    if (first)
    {
      visual_odometer_->process(image_data, dims);
      delta_transform.setIdentity();
    }
    else
    {
      bool ret = visual_odometer_->process(image_data, dims);
      if(ret) // if a motion was successfully detected
      {
        Matrix camera_motion = Matrix::inv(visual_odometer_->getMotion());

        tf::Matrix3x3 r_m(
            camera_motion.val[0][0], camera_motion.val[0][1], camera_motion.val[0][2],
            camera_motion.val[1][0], camera_motion.val[1][1], camera_motion.val[1][2],
            camera_motion.val[2][0], camera_motion.val[2][1], camera_motion.val[2][2] );
        tf::Vector3 t_v( camera_motion.val[0][3], camera_motion.val[1][3], camera_motion.val[2][3] ); 
        ROS_INFO("%d Matches -- %d Inliers -- %s", visual_odometer_->getNumberOfMatches(), visual_odometer_->getNumberOfInliers(), (ret?"Motion":"No Motion") );
        ROS_INFO("viso2 motion [camera frame -- z out of the lens] x:%f y:%f z:%f", camera_motion.val[0][3], camera_motion.val[1][3], camera_motion.val[2][3]);
        delta_transform = tf::Transform(r_m, t_v);
      }
      else // else there was an error OR the motion was too small
      {
        delta_transform.setIdentity();
      }

      // useful for rosbag playbacks
      if( img_info_pair.first->header.stamp < prev_update_time_ )
      {
        integrated_pose_.setIdentity();
      }

      integrated_pose_ *= delta_transform;

      tf::StampedTransform base_to_camera;
      std::string er_msg;
      if ( tf_listener_->canTransform(base_link_frame_, camera_frame_, img_info_pair.first->header.stamp, &er_msg) )
      {
        tf_listener_->lookupTransform(
            base_link_frame_,
            camera_frame_,
            img_info_pair.first->header.stamp, base_to_camera);
      }
      else
      {
        base_to_camera.setIdentity();
      }

      tf::Transform base_tf = base_to_camera * integrated_pose_ * base_to_camera.inverse();
      nav_msgs::Odometry odom_m;
      nav_msgs::Odometry error_odom_m;
      odom_m.header = img_info_pair.first->header;
      odom_m.header.frame_id = odom_frame_;
      odom_m.child_frame_id = base_link_frame_;

      for( int i = 0; i < 6; i++ )
        for( int j = 0; j < 6; j++ )
          if(i==j)
            odom_m.pose.covariance[j+i*6] = ( i>3 ? 0.13 : 0.1 );
          else
            odom_m.pose.covariance[j+i*6] = 0.0;

      for( int i = 0; i < 6; i++ )
        for( int j = 0; j < 6; j++ )
          if(i==j)
            odom_m.twist.covariance[j+i*6] = ( i>3 ? 0.09 : 0.05 );
          else
            odom_m.twist.covariance[j+i*6] = 0.0;

      tf::poseTFToMsg( base_tf, odom_m.pose.pose );

      tf::Transform delta_base_tf = base_to_camera * delta_transform * base_to_camera.inverse();
      if( !first )
      {
        double dt = (img_info_pair.first->header.stamp - prev_update_time_).toSec();
        prev_update_time_ = img_info_pair.first->header.stamp;
        if(dt)
        {
          odom_m.twist.twist.linear.x = delta_base_tf.getOrigin().getX() / dt; 
          odom_m.twist.twist.linear.y = delta_base_tf.getOrigin().getY() / dt; 
          odom_m.twist.twist.linear.z = delta_base_tf.getOrigin().getZ() / dt; 
          tf::Quaternion drot = delta_base_tf.getRotation();
          tfScalar angle = drot.getAngle();
          tf::Vector3 axis = drot.getAxis();
          tf::Vector3 angular_twist = axis * angle / dt;
          odom_m.twist.twist.angular.x = angular_twist.x();
          odom_m.twist.twist.angular.y = angular_twist.y();
          odom_m.twist.twist.angular.z = angular_twist.z();
        }
      }


      odom_pub_->publish( odom_m );
      if(publish_tf_)
      {
        ROS_INFO("[viso]::Transform Published--%s --> %s", base_link_frame_.c_str(), odom_frame_.c_str());
        tf_broadcaster_.sendTransform( tf::StampedTransform(base_tf, img_info_pair.first->header.stamp, odom_frame_, base_link_frame_) );
      }

      error_odom_m = odom_m;
      tf::poseTFToMsg( delta_base_tf, error_odom_m.pose.pose );

      delta_odom_pub_->publish( error_odom_m );

    }
    return;
  }
}
