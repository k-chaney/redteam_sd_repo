/*
 *
 * Utilities found in github.com/utiasSTARS/msckf-swf-comparison
 *
 *      - /msckf/utils
 *      - /msckf
 *
 * The purpose is to implement all base functionality found there in C++ with Eigen
 *
 */
#include <usma_perception_plugins/images/msckf_utils.h>

#include <iostream>
#include <ctime>

#include <ros/ros.h>
#define FFL_ERROR {ROS_ERROR("%s::%s::%d",__FILE__,__func__,__LINE__);}

#define MSCKF_ERR(X) {FFL_ERROR; \
  ROS_ERROR("CS Size :: %d", X.cam_states_.size());\
  for(int i = 0; i < X.cam_states_.size(); i++) ROS_ERROR( "CS_TF%d :: size %d", i, X.cam_states_[i].tracked_features_.size() );\
  ROS_ERROR("CC Size :: %d rows by %d cols", X.cam_covar_.rows(), X.cam_covar_.cols());\
  ROS_ERROR("ICC Size:: %d rows by %d cols", X.imu_cam_covar_.rows(), X.imu_cam_covar_.cols());\
}

#ifdef TIME_FUNCS
#define TIMER_START std::time_t msckf_function_timer = std::clock()
#define TIMER_END ROS_INFO("%s -- %5.10f", __func__, std::difftime(std::clock(),msckf_function_timer)/ CLOCKS_PER_SEC )
#else
#define TIMER_START
#define TIMER_END
#endif


namespace usma_perception
{
  namespace msckf
  {
    Eigen::MatrixXd calcK( Eigen::MatrixXd &T_H, Eigen::MatrixXd &P, Eigen::MatrixXd &R_n )
    {
      TIMER_START;

      Eigen::MatrixXd THT = T_H.transpose();
      Eigen::MatrixXd PTHT = ( P * THT );
      //double thresh = std::max( THPTHT.rows(), THPTHT.cols() ) * THPTHT.colwise().maxCoeff().maxCoeff() * std::numeric_limits<double>::epsilon();
      //for(int i = 0; i < THPTHT.rows(); i++)
      //  for(int j = 0; j < THPTHT.cols(); j++)
      //    if( THPTHT(i,j) < thresh )
      //      THPTHT(i,j) = 0.0;

      Eigen::MatrixXd invTHPTHT = T_H;
      invTHPTHT *= P;
      invTHPTHT *= THT;
      invTHPTHT += R_n;
      invTHPTHT = (invTHPTHT).inverse();

      TIMER_END;

      return PTHT * invTHPTHT;
    }

    void propagateMsckfStateAndCovar( msckfState &msckf_state, measurement &m_k, noiseParams &noise_params )
    {
      TIMER_START;

      imuCovar F = calcF( msckf_state.imu_state_, m_k );
      imuCovar G = calcG( msckf_state.imu_state_ );

      propagateImuState( msckf_state.imu_state_, m_k );

      imuCovar Phi = imuCovar::Identity();
      Phi += ( F * m_k.dT_ );

      msckf_state.imu_covar_ = Phi * msckf_state.imu_covar_ * Phi.transpose() 
                                          + G * noise_params.Q_imu_ * G.transpose() * m_k.dT_;

      enforcePSD( msckf_state.imu_covar_ );
      msckf_state.cam_covar_ = msckf_state.cam_covar_;
      msckf_state.imu_cam_covar_ = Phi * msckf_state.imu_cam_covar_;
      msckf_state.cam_states_ = msckf_state.cam_states_;

      TIMER_END;

    }

    msckfState updateState( msckfState &msckf_state, Eigen::MatrixXd &deltaX )
    {
      TIMER_START;


      msckfState ret;

      ret = msckf_state;

      Eigen::Matrix<double,3,1> delta_theta_IG = deltaX.block<3,1>(0,0);
      Eigen::Matrix<double,3,1> delta_b_g = deltaX.block<3,1>(3,0);
      Eigen::Matrix<double,3,1> delta_b_v = deltaX.block<3,1>(6,0);
      Eigen::Matrix<double,3,1> delta_p_I_G = deltaX.block<3,1>(9,0);

      quaternion delta_q_IG = buildUpdateQuat( delta_theta_IG );

      ret.imu_state_.q_IG_ = quatLeftComp( delta_q_IG ) * ret.imu_state_.q_IG_;
      ret.imu_state_.b_g_ += delta_b_g;
      ret.imu_state_.b_v_ += delta_b_v;
      ret.imu_state_.p_IG_ += delta_p_I_G;

      Eigen::Matrix<double,3,1> delta_theta_CG;
      Eigen::Matrix<double,3,1> delta_p_CG;
      quaternion delta_q_CG;

      for( int i = 0; i < msckf_state.cam_states_.size(); i++ )
      {
        int qs = 12 + 6 * i;
        int ps = qs + 3;

        delta_theta_CG = deltaX.block<3,1>(qs,0);
        delta_p_CG = deltaX.block<3,1>(ps,0);

        delta_q_CG = buildUpdateQuat( delta_theta_CG );
        ret.cam_states_[i].q_CG_ = quatLeftComp( delta_q_CG ) * msckf_state.cam_states_[i].q_CG_;
        ret.cam_states_[i].p_CG_ += delta_p_CG;
      }

      TIMER_END;


      return ret;
    }

    remove_feature_ret removeTrackedFeature( msckfState &msckf_state, int feat_id )
    {
      TIMER_START;


      remove_feature_ret ret;

      for( int i = 0; i < msckf_state.cam_states_.size(); i++ )
      {
        // if the feature id is in this camera state
        int index = 0;
        bool found = false;

        for( index = 0; index < msckf_state.cam_states_[i].tracked_features_.size(); index++)
        {
           if( msckf_state.cam_states_[i].tracked_features_[index] == feat_id )
           {
             found = true;
             break;
           }
        }
        if( found )
        {
          msckf_state.cam_states_[i].tracked_features_.erase( msckf_state.cam_states_[i].tracked_features_.begin() + index );
          ret.cam_state_indices_.push_back( i );
          ret.feat_cam_states_.push_back( msckf_state.cam_states_[i] );
        }
      }

      TIMER_END;


      return ret;
    }

    void pruneStates( msckfState &msckf_state )
    {
      TIMER_START;


      std::vector<cameraState> kcs;
      std::vector<int> kept_states; // provides a map between old msckState covariance matrix and new one
      for( int i = 0; i < msckf_state.cam_states_.size(); i++ )
      {
        if( msckf_state.cam_states_[i].tracked_features_.size() != 0 )
        {
          kept_states.push_back( i );
          kcs.push_back( msckf_state.cam_states_[i] ); // save the camera states we want to keep
        }
      }
      msckf_state.cam_states_.clear(); // clear the whole vector

      Eigen::MatrixXd t1( 6 * kept_states.size(), 6 * kept_states.size() );
      Eigen::MatrixXd t2( 12, 6*kept_states.size() );

      for( int i = 0; i < kept_states.size(); i++ )
      {
        // push the kept camera states back onto the vector
        msckf_state.cam_states_.push_back( kcs[i] );

        for( int j = 0; j < kept_states.size(); j++ )
        {
          t1.block<6,6>(6*i,6*j) = msckf_state.cam_covar_.block<6,6>( 6*kept_states[i], 6*kept_states[j] );
        }
        t2.block<12,6>(0,6*i) = msckf_state.imu_cam_covar_.block<12,6>(0, 6*kept_states[i]);
      }

      msckf_state.cam_covar_.resize(t1.rows(),t1.cols());
      msckf_state.cam_covar_<< t1;
      msckf_state.imu_cam_covar_.resize(t2.rows(),t2.cols());
      msckf_state.imu_cam_covar_ << t2;

      TIMER_END;

    }

    Eigen::MatrixXd calcResidual( Eigen::Matrix<double,3,1> &p_f_G, std::vector<cameraState> &camera_states, std::vector<Eigen::Matrix<double,2,1> > &measurements )
    {
      TIMER_START;


      Eigen::MatrixXd ret( 2*camera_states.size(), 1 );
      ret.setZero();

      Eigen::Matrix<double,3,3> C_CG;
      Eigen::Matrix<double,3,1> p_f_C;
      Eigen::Matrix<double,2,1> zhat_i_j;

      for( int i = 0; i < camera_states.size(); i++ )
      {
        C_CG = quatToRotMat( camera_states[i].q_CG_ );
        p_f_C = C_CG * ( p_f_G - camera_states[i].p_CG_ );
        zhat_i_j = p_f_C.block<2,1>(0,0) / p_f_C(2);

        ret.block<2,1>( 2*i, 0 ) = measurements[i] - zhat_i_j;
      }

      TIMER_END;

      return ret;
    }

    std::pair<Eigen::MatrixXd,Eigen::MatrixXd> calcTH( Eigen::MatrixXd &H_o )
    {
      TIMER_START;

      Eigen::ColPivHouseholderQR<Eigen::MatrixXd> qr(H_o);
      Eigen::MatrixXd Q = qr.householderQ();
      Eigen::MatrixXd R = qr.matrixR();

      // find zero rows
      double eps = std::numeric_limits<double>::epsilon();
      std::vector<int> non_zero_rows;
      int non_zero_count = 0;
      for( int i = 0; i < R.rows(); i++ )
      {
        bool is_zero = true;
        for( int j = 0; j < R.cols(); j++ )
          if( fabs( R.row(i)(j) ) > eps )
            is_zero = false;

        if( !is_zero )
        {
          non_zero_rows.push_back( i );
          non_zero_count++;
        }
      }

      Eigen::MatrixXd T_H( non_zero_count, R.cols() );
      Eigen::MatrixXd Q_1( Q.rows(), non_zero_count );

      for( int i = 0; i < non_zero_count; i++ )
      {
        T_H.row( i ) = R.row( non_zero_rows[i] );
        Q_1.col( i ) = Q.col( non_zero_rows[i] );
      }

      TIMER_END;
      return std::make_pair( T_H, Q_1 );
    }

    void augmentState( msckfState &msckf_state, camera &cam, int state_k )
    {
      TIMER_START;


      Eigen::Matrix<double,3,3> C_IG = quatToRotMat( msckf_state.imu_state_.q_IG_ );
      quaternion q_CG = quatLeftComp( cam.q_CI_ ) * msckf_state.imu_state_.q_IG_;

      int nrows = msckf_state.imu_covar_.rows() + msckf_state.cam_covar_.rows();
      int ncols = msckf_state.imu_covar_.cols() + msckf_state.cam_covar_.cols();
      Eigen::MatrixXd P( nrows, ncols );
      P.block<12,12>(0,0) = msckf_state.imu_covar_;
      P.block(0,12,msckf_state.imu_cam_covar_.rows(),msckf_state.imu_cam_covar_.cols()) = msckf_state.imu_cam_covar_;
      P.block(12,0,msckf_state.imu_cam_covar_.cols(),msckf_state.imu_cam_covar_.rows()) = msckf_state.imu_cam_covar_.transpose();
      P.block(12,12,msckf_state.cam_covar_.rows(),msckf_state.cam_covar_.cols()) = msckf_state.cam_covar_;

      Eigen::MatrixXd J = calcJ( cam, msckf_state.imu_state_, msckf_state.cam_states_ );

      int n = msckf_state.cam_states_.size();

      Eigen::MatrixXd temp_mat( 12+6*n + J.rows(), 12+6*n );
      temp_mat.setZero();
      temp_mat.block(0,0,12+6*n,12+6*n) = Eigen::MatrixXd::Identity( 12+6*n, 12+6*n );
      temp_mat.block(12+6*n,0,J.rows(),J.cols()) = J;

      Eigen::MatrixXd P_aug = temp_mat * P * temp_mat.transpose();
      //double thresh = std::max( P_aug.rows(), P_aug.cols() ) * std::numeric_limits<double>::epsilon();
      //for( int i = 0; i < P_aug.rows(); i++ )
      //  for( int j = 0; j < P_aug.cols(); j++ )
      //    if( P_aug(i,j) < thresh )
      //      P_aug(i,j) = 0.0;

      msckf_state.cam_states_.resize( msckf_state.cam_states_.size() + 1 );
      cameraState* cs = &msckf_state.cam_states_.back();
      cs->p_CG_ = msckf_state.imu_state_.p_IG_ + C_IG.transpose() * cam.p_CI_;
      cs->q_CG_ = q_CG;
      cs->state_k_ = state_k;

      msckf_state.imu_covar_ = P_aug.block<12,12>(0,0);
      msckf_state.cam_covar_ = P_aug.block(12,12,P_aug.rows()-12,P_aug.cols()-12);
      msckf_state.imu_cam_covar_ = P_aug.block(0,12,12,P_aug.cols()-12);

      TIMER_END;
    }

    Eigen::MatrixXd calcJ( camera &cam, imuState &imu_state, std::vector<cameraState> &cam_states )
    {
      TIMER_START;

      Eigen::Matrix<double,3,3> C_CI = quatToRotMat( cam.q_CI_ );
      Eigen::Matrix<double,3,3> C_IG = quatToRotMat( imu_state.q_IG_ );

      Eigen::MatrixXd J(6, 12 + 6 * cam_states.size());
      J.setZero();
      J.block<3,3>(0,0) = C_CI;
      Eigen::Matrix<double,3,1> t = C_IG.transpose() * cam.p_CI_;
      J.block<3,3>(3,0) = crossMat( t );
      J.block<3,3>(3,9) = Eigen::Matrix<double,3,3>::Identity();

      TIMER_END;
      return J;
    }

    void calcHoj( Eigen::Matrix<double,3,1> &p_f_G_, msckfState &msckf_state, std::vector<int> &cam_state_indices, Eigen::MatrixXd &H_o_j, Eigen::MatrixXd &A_j, Eigen::MatrixXd &H_x_j )
    {
      TIMER_START;

      int N = msckf_state.cam_states_.size();
      int M = cam_state_indices.size();

      Eigen::MatrixXd H_f_j = Eigen::MatrixXd( 2*M, 3 );
      H_f_j.setZero();

      H_x_j.resize( 2*M, 12 + 6*N );
      H_x_j.setZero();

      int c_i = 1;

      Eigen::Matrix<double,3,3> C_CG;
      Eigen::Matrix<double,3,1> p_f_C;
      Eigen::Matrix<double,2,3> J_i;

      for( int i = 0; i < cam_state_indices.size(); i++ )
      {
        int cam_state_index = cam_state_indices[i];
        cameraState* cam_state = &(msckf_state.cam_states_[cam_state_index]);

        C_CG = quatToRotMat( cam_state->q_CG_ );

        p_f_C = C_CG * (p_f_G_ - cam_state->p_CG_);

        double x = p_f_C(0);
        double y = p_f_C(1);
        double z = p_f_C(2);

        J_i(0,0) = 1.0 / z;
        J_i(0,1) = 0.0;
        J_i(0,2) = -1.0 * (x/z) / z;
        J_i(1,0) = 0.0;
        J_i(1,1) = 1.0 / z;
        J_i(1,2) = -1.0 * (y/z) / z;

        H_f_j.block<2,3>(2*c_i - 2,0) = J_i * C_CG;

        H_x_j.block<2,3>(2*c_i -2, 12+6*cam_state_index) = J_i * crossMat(p_f_C);
        H_x_j.block<2,3>(2*c_i -2, 12+6*cam_state_index+3) = -1.0 * J_i * C_CG;

        c_i++;
      }

      // get the null space of the matrix
      A_j = H_f_j.transpose().fullPivLu().kernel();

      H_o_j = A_j.transpose() * H_x_j;

      TIMER_END;
    }

    gn_pos_est_ret calcGNPosEst( std::vector<cameraState> &camera_states, std::vector<pixelMeasurement> &observations, noiseParams &noise_params )
    {
      TIMER_START;


      // there must be one observation per camera state
      assert( camera_states.size() == observations.size() );

      gn_pos_est_ret ret;

      int sv_idx_ = camera_states.size()-1;

      Eigen::Matrix<double,3,3> C_12 = quatToRotMat( camera_states[0].q_CG_ ) * quatToRotMat( camera_states[sv_idx_].q_CG_ ).transpose();
      Eigen::Matrix<double,3,1> t_21_1 = quatToRotMat( camera_states[0].q_CG_ ) * ( camera_states[sv_idx_].p_CG_ - camera_states[0].p_CG_ );

      position p_f1_1_bar = triangulate( observations[0], observations[sv_idx_], C_12, t_21_1 );

      double x_bar = p_f1_1_bar(0);
      double y_bar = p_f1_1_bar(1);
      double z_bar = p_f1_1_bar(2);

      double alpha_bar = x_bar / z_bar;
      double beta_bar = y_bar / z_bar;
      double rho_bar = 1 / z_bar;

      Eigen::Matrix<double,3,1> x_est;
      x_est(0) = alpha_bar;
      x_est(1) = beta_bar;
      x_est(2) = rho_bar;

      int cam_state_size = camera_states.size();

      int max_iters = 10;
      double Jprev = std::numeric_limits<double>::max();

      // preallocate a lot of the stuff we need before the optimization that will init it a lot
      Eigen::MatrixXd E( 2 * cam_state_size, 3 );
      Eigen::MatrixXd W( 2 * cam_state_size, 2 * cam_state_size );
      Eigen::MatrixXd error_vec( 2*cam_state_size, 1 );

      Eigen::Matrix<double,3,3> C_i1;
      Eigen::Matrix<double,3,1> t_1i_i;
      Eigen::Matrix<double,2,1> z_hat;
      Eigen::Matrix<double,3,1> h;
      Eigen::Matrix<double,2,3> e_block;
      Eigen::Matrix<double,3,1> dx_star;


      W.setZero();
      for( int si = 0; si < cam_state_size; si++ )
      {
          W( 2 * (1+si) - 2, 2 * (1+si) - 2 ) = noise_params.u_var_prime_;
          W( 2 * (1+si) - 1, 2 * (1+si) - 1 ) = noise_params.v_var_prime_;
      }

      for( int oi = 0; oi < max_iters; oi++ )
      {
        Eigen::Matrix<double,3,1> ab_bar;
        ab_bar(0) = alpha_bar;
        ab_bar(1) = beta_bar;
        ab_bar(2) = 1.0;

        E.setZero();
        error_vec.setZero();

        for( int si = 0; si < cam_state_size; si++ )
        {
          C_i1 = quatToRotMat( camera_states[si].q_CG_ ) * quatToRotMat( camera_states[0].q_CG_ ).transpose();
          t_1i_i = quatToRotMat( camera_states[si].q_CG_ ) * ( camera_states[0].p_CG_  - camera_states[si].p_CG_ );
          z_hat = observations[si];
          h = C_i1 * ab_bar + rho_bar * t_1i_i;
          error_vec( 2*(1+si) - 2, 0 ) = z_hat(0) - h(0) / h(2);
          error_vec( 2*(1+si) - 1, 0 ) = z_hat(1) - h(1) / h(2);

          double h2_squared = h(2) * h(2);
          e_block(0,0) = (-C_i1(0,0)/h(2)) + ( h(0) / ( h2_squared )) * C_i1(2,0);
          e_block(1,0) = (-C_i1(1,0)/h(2)) + ( h(1) / ( h2_squared )) * C_i1(2,0);
          e_block(0,1) = (-C_i1(0,1)/h(2)) + ( h(0) / ( h2_squared )) * C_i1(2,1);
          e_block(1,1) = (-C_i1(1,1)/h(2)) + ( h(1) / ( h2_squared )) * C_i1(2,1);
          e_block(0,2) = (-t_1i_i(0)/h(2)) + ( h(0) / ( h2_squared )) * t_1i_i(2);
          e_block(1,2) = (-t_1i_i(1)/h(2)) + ( h(1) / ( h2_squared )) * t_1i_i(2);

          E.block<2,3>(2*(1+si)-2,0) = e_block;
        }


        // This is a row reduced solution because W is a diagonal consisting of u_var_prime_ and v_var_prime_
        Eigen::MatrixXd W_ev_sol(error_vec.rows(), error_vec.cols());
        for( int i = 0; i < W_ev_sol.rows(); i++ )
          for( int j = 0; j < W_ev_sol.cols(); j++ )
            W_ev_sol(i,j) = error_vec(i,j) / W(i,i);

        Eigen::MatrixXd WE(E.rows(),E.cols());
        for( int i = 0; i < WE.rows(); i++ )
          for( int j = 0; j < WE.cols(); j++ )
            WE(i,j) = E(i,j) / W(i,i);

        // this should always solve to be a single element matrix (that is 1x1)
        // since these are dynamic matricies Eigen doesn't realize that
        //
        // just grab element 0,0 which should always exist
        double Jnew = (0.5 * error_vec.transpose() * W_ev_sol)(0,0);


        Eigen::Matrix<double,3,3> EWE = E.transpose() * WE;

        ret.reciprocal_condition_num_ = calcRCOND( EWE );

        Eigen::MatrixXd tmp = (-1.0 * E.transpose() * W_ev_sol);

        dx_star = EWE.colPivHouseholderQr().solve( tmp );
        x_est = x_est + dx_star;

        double Jderiv = fabs( (Jnew - Jprev) / Jnew );

        Jprev = Jnew;

        if( Jderiv < 0.01 )
        {
          break;
        }
        else
        {
          alpha_bar= x_est(0);
          beta_bar = x_est(1);
          rho_bar  = x_est(2);
        }

      }

      Eigen::Matrix<double,3,1> tmp;
      tmp(0) = x_est(0);
      tmp(1) = x_est(1);
      tmp(2) = 1.0;
      ret.p_f_G_ = (1.0/x_est(2)) * quatToRotMat( camera_states[0].q_CG_ ).transpose() * tmp + camera_states[0].p_CG_;


      TIMER_END;
      return ret;
    }

    imuCovar calcF( imuState &is_k, measurement &m_k )
    {
      TIMER_START;


      imuCovar F = imuCovar::Zero();

      Eigen::Matrix<double,3,1> omegaHat = m_k.omega_ - is_k.b_g_;
      Eigen::Matrix<double,3,1> vHat = m_k.v_ - is_k.b_v_;
      Eigen::Matrix<double,3,3> C_IG = quatToRotMat( is_k.q_IG_ );

      F.block<3,3>(0,0) = -1.0 * crossMat( omegaHat );
      F.block<3,3>(0,3) = -1.0 * Eigen::Matrix<double,3,3>::Identity();
      F.block<3,3>(9,0) = ( -1.0 * C_IG.transpose() ) * crossMat( vHat );
      F.block<3,3>(9,6) = ( -1.0 * C_IG.transpose() );

      TIMER_END;
      return F;
    }

    imuCovar calcG( imuState &is_k )
    {
      TIMER_START;


      imuCovar G = imuCovar::Zero();
      Eigen::Matrix<double,3,3> C_IG = quatToRotMat( is_k.q_IG_ );

      G.block<3,3>(0,0) = -1.0 * Eigen::Matrix<double,3,3>::Identity();
      G.block<3,3>(3,3) = Eigen::Matrix<double,3,3>::Identity();
      G.block<3,3>(6,9) = Eigen::Matrix<double,3,3>::Identity();
      G.block<3,3>(9,6) = ( -1.0 * C_IG.transpose() );

      TIMER_END;
      return G;
    }

    void propagateImuState( imuState &is_k, measurement &m_k )
    {
      TIMER_START;

      Eigen::Matrix<double,3,3> C_IG_T = quatToRotMat( is_k.q_IG_ ).transpose();
      Eigen::Matrix<double,3,1> psi = ( m_k.omega_ - is_k.b_g_ ) * m_k.dT_;

      quaternion dQuat = 0.5 * omegaMat( psi ) * is_k.q_IG_;
      quaternion tempQuat = is_k.q_IG_ + dQuat;

      is_k.q_IG_ = tempQuat / tempQuat.norm();

      Eigen::Matrix<double,3,1> d;
      d = (m_k.v_ - is_k.b_v_) * m_k.dT_;

      is_k.p_IG_ = C_IG_T * d + is_k.p_IG_;

      TIMER_END;
    }

    Eigen::Matrix<double,3,3> crossMat( Eigen::Matrix<double,3,1> &vec )
    {
      Eigen::Matrix<double,3,3> r;

      r(0,0) = 0.0;     r(0,1) = -vec(2); r(0,2) = vec(1);
      r(1,0) = vec(2);  r(1,1) = 0.0;     r(1,2) = -vec(0);
      r(2,0) = -vec(1); r(2,1) = vec(0);  r(2,2) = 0.0;

      return r;
    }

    Eigen::Matrix<double,3,1> crossMatToVec( Eigen::Matrix<double,3,3> &mat )
    {
      Eigen::Matrix<double,3,1> r;

      r(0) = mat(2,1);
      r(1) = mat(0,2);
      r(2) = mat(1,0);

      return r;
    }

    Eigen::Matrix<double,3,3> axisAngleToRotMat( Eigen::Matrix<double,3,1> &psi )
    {
      Eigen::Matrix<double,3,3> r;
      double np = psi.norm();
      double cp = cos( np );
      double sp = sin( np );
      Eigen::Matrix<double,3,1> pnp = psi / np;

      r = ( cp * Eigen::Matrix<double,3,3>::Identity() ) + ( (1 - cp) * ( pnp * pnp.transpose() ) ) - ( sp * crossMat(pnp) );

      return r;
    }

    quaternion buildUpdateQuat( Eigen::Matrix<double,3,1> &deltaTheta )
    {
      quaternion r;
      Eigen::Matrix<double,3,1> deltaq = 0.5 * deltaTheta;
      double checkNorm = deltaq.transpose() * deltaq;

      r.block<3,1>(0,0) = deltaq;

      if( checkNorm > 1.0 )
      {
        r(3) = 1.0;
        r = r / sqrt( 1.0 + checkNorm );
      }
      else
      {
        r(3) = sqrt( 1.0 - checkNorm );
      }

      checkNorm = r.norm();
      r = r / checkNorm;

      return r;
    }

    void enforcePSD( imuCovar &m )
    {
      double ode;

      for( int i = 0; i < m.cols(); i++ )
      {
        for( int j = 0; j < m.rows(); j++ )
        {
          if( i == j )
          {
            m(i,j) = fabs( m(i,j) );
          }
          else
          {
            ode = (m(i,j) + m(j,i))/2.0;
            m(i,j) = ode;
            m(j,i) = ode;
          }
        }
      }
    }

    Eigen::Matrix<double,4,4> omegaMat( Eigen::Matrix<double, 3, 1> &omega )
    {
      Eigen::Matrix<double,4,4> r;

      r.block<3,3>(0,0) = -1.0 * crossMat( omega);
      r.block<1,3>(3,0) = -1.0 * omega;
      r.block<3,1>(0,3) = omega.transpose();
      r(3,3) = 0.0;

      return r;
    }

    quaternion quatInv( quaternion &q)
    {
      assert( abs( q.norm() - 1.0) <= std::numeric_limits<double>::epsilon() );

      quaternion qinv;
      qinv(0,0) = -qinv(0,0);
      qinv(1,0) = -qinv(1,0);
      qinv(2,0) = -qinv(2,0);
      qinv(3,0) = qinv(3,0);

      return qinv;
    }

    Eigen::Matrix<double,4,4> quatLeftComp( quaternion &q )
    {
      Eigen::Matrix<double,4,4> r;
      Eigen::Matrix<double, 3, 1>  vec = q.block<3,1>(0,0);
      double scalar = q(3);

      r.block<3,3>(0,0) = scalar * Eigen::Matrix<double,3,3>::Identity() - crossMat( vec );
      r.block<1,3>(3,0) = -1.0 * vec;
      r.block<3,1>(0,3) = vec.transpose();
      r(3,3) = scalar;

      return r;
    }

    quaternion quatMult( quaternion &q1, quaternion &q2 )
    {
      quaternion r;

      r = quatLeftComp( q1 ) * q2;

      return r;
    }

    Eigen::Matrix<double,4,4> quatRightComp( quaternion &q )
    {
      Eigen::Matrix<double,4,4> r;
      Eigen::Matrix<double, 3, 1>  vec = q.block<3,1>(0,0);
      double scalar = q(3);

      r.block<3,3>(0,0) = scalar * Eigen::Matrix<double,3,3>::Identity() + crossMat( vec );
      r.block<1,3>(3,0) = -1.0 * vec;
      r.block<3,1>(0,3) = vec.transpose();
      r(3,3) = scalar;

      return r;
    }

    Eigen::Matrix<double,3,3> renormalizeRotMat( Eigen::Matrix<double,3,3> &m )
    {
      Eigen::JacobiSVD<Eigen::Matrix<double,3,3> > jsvd(m, Eigen::ComputeFullU | Eigen::ComputeFullV );

      Eigen::Matrix<double,3,3> VT = jsvd.matrixV().transpose();

      return jsvd.matrixU() * Eigen::Matrix<double,3,3>::Identity() * VT;
    }

    Eigen::Matrix<double,3,3> quatToRotMat( quaternion &q )
    {
      q = q / q.norm();

      Eigen::Matrix<double,4,4> R = quatRightComp(q).transpose() * quatLeftComp(q);
      Eigen::Matrix<double,3,3> tmp = R.block<3,3>(0,0);
      tmp = renormalizeRotMat( tmp );

      return tmp;
    }

    template<typename T>
    std::vector< T > removeCells( std::vector<T> &in, std::vector<int> &delete_idx )
    {
      std::vector< T > r;

      for( int i = 0; i < in.size(); i++ )
        if( std::find( delete_idx.begin(), delete_idx.end(), i ) == delete_idx.end() )
          r.push_back( in[i] );

      return r;
    }

    quaternion rotMatToQuat( Eigen::Matrix<double,3,3> &R )
    {
      quaternion q;
      R = R.transpose();

      double Rxx = R(0,0); double Rxy = R(0,1); double Rxz = R(0,2);
      double Ryx = R(1,0); double Ryy = R(1,1); double Ryz = R(1,2);
      double Rzx = R(2,0); double Rzy = R(2,1); double Rzz = R(2,2);

      double w = sqrt( R.trace() + 1.0 ) / 2.0;
      double x = sqrt( 1.0 + Rxx - Ryy - Rzz ) / 2.0;
      double y = sqrt( 1.0 + Ryy - Rxx - Rzz ) / 2.0;
      double z = sqrt( 1.0 + Rzz - Ryy - Rxx ) / 2.0;

      double i = std::max( std::max(w,x), std::max(y,z) );
      if( i == w )
      {
        x = ( Rzy - Ryz ) / (4.0*w);
        y = ( Rxz - Rzx ) / (4.0*w);
        z = ( Ryx - Rxy ) / (4.0*w);
      }
      else if( i == x )
      {
        w = ( Rzy - Ryz ) / (4.0*x);
        y = ( Rxy + Ryx ) / (4.0*x);
        z = ( Rzx + Rxz ) / (4.0*x);
      }
      else if( i == y )
      {
        w = ( Rxz - Rzx ) / (4.0*y);
        x = ( Rxy + Ryx ) / (4.0*y);
        z = ( Ryz + Rzy ) / (4.0*y);
      }
      else if( i == z )
      {
        w = ( Ryx - Rxy ) / (4.0*z);
        x = ( Rzx + Rxz ) / (4.0*z);
        y = ( Ryz + Rzy ) / (4.0*z);
      }

      q(0) = x;
      q(1) = y;
      q(2) = z;
      q(3) = w;

      return q;
    }

    position triangulate( pixelMeasurement &o1, pixelMeasurement &o2, Eigen::Matrix<double,3,3> &R, Eigen::Matrix<double,3,1> &t )
    {
      TIMER_START;

      position ret;

      Eigen::Matrix<double,3,1> v1;
      Eigen::Matrix<double,3,1> v2;

      v1(0) = o1(0);
      v1(1) = o1(1);
      v1(2) = 1.0;

      v2(0) = o2(0);
      v2(1) = o2(1);
      v2(2) = 1.0;

      v1 = v1 / v1.norm();
      v2 = v2 / v2.norm();

      Eigen::Matrix<double,3,2> A;
      A.block<3,1>(0,0) = v1;
      A.block<3,1>(0,1) = -1.0 * R * v2;

      Eigen::Matrix<double,2,1> sc = A.jacobiSvd(Eigen::ComputeFullU | Eigen::ComputeFullV).solve(t);
      ret = sc(0) * v1;

      TIMER_END;
      return ret;
    }

    double calcRCOND( Eigen::Matrix<double,3,3> &m )
    {
      double A_1 = m.cwiseAbs().colwise().sum().maxCoeff(); // take the absolute value -- sum the columns -- take max
      double A_inv_1 = m.inverse().cwiseAbs().colwise().sum().maxCoeff();

      return 1.0/(A_1 * A_inv_1);
    }
  }
}
