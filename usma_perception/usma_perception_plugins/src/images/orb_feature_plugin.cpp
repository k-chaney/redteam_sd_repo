#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/orb_feature_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ORBPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ORBPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("ORB");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("image_transport::Camera"); // input image
    ot.push_back("usma_perception_msgs::FeatureArray"); // the feature array itself
    ot.push_back("image_transport::Camera"); // overlay of features onto the image
  }

  bool ORBPlugin::checkInputReady()
  {
    return im_sub_->dataReady();
  }

  void ORBPlugin::init()
  {
    processOptions();

    im_sub_ = boost::any_cast<boost::shared_ptr<ManagedCameraSubscriber> >( subs_[0] );
    feature_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >( pubs_[0] );
    image_overlay_pub_ = boost::any_cast<boost::shared_ptr<image_transport::CameraPublisher> >( pubs_[1] );

    orb_detector_.reset( new cv::ORB( n_features_, scale_factor_, n_levels_, edge_threshold_, first_level_, wta_k_, score_type_, patch_size_ ) );
    orb_detector_->create("Feature2D.ORB");

    matcher_.reset( new cv::BFMatcher(cv::NORM_HAMMING) );

    homography_ = cv::Mat::ones(3,3, CV_64F);
    homography_.at<double>(0,0) = 7.6285898e-01;
    homography_.at<double>(0,1) = -2.9922929e-01;
    homography_.at<double>(0,2) = 2.2567123e02;
    homography_.at<double>(1,0) = 3.3443473e-01;
    homography_.at<double>(1,1) = 1.0143901e00;
    homography_.at<double>(1,2) = -7.6999973e01;
    homography_.at<double>(2,0) = 3.4463091e-04;
    homography_.at<double>(2,1) = -1.4364524e-05;
    homography_.at<double>(2,2) = 1.000e00;

    cur_tag_num_ = 0;
  }

  void ORBPlugin::processOptions()
  {
    n_features_ = getOption("n_features",500);
    scale_factor_ = getOption("scale_factor",1.2);
    n_levels_ = getOption("n_levels",8);
    edge_threshold_ = getOption("edge_theshold",31);
    first_level_ = getOption("first_level",0);
    wta_k_ = getOption("wta_k",2);
    score_type_ = getOption("score_type", (int)cv::ORB::HARRIS_SCORE);
    patch_size_ = getOption("patch_size",31);
    nn_max_dist_ratio_ = getOption("nearest_neighbor_max_dist_ratio",0.9);
    inlier_threshold_ = getOption("inlier_threshold",5.0);
  }

  void ORBPlugin::workLoad()
  {
    if( new_options_ready_ )
    {
      options_mtx_.lock();
      processOptions();
      orb_detector_.reset( new cv::ORB( n_features_, scale_factor_, n_levels_, edge_threshold_, first_level_, wta_k_, score_type_, patch_size_ ) );
      orb_detector_->create("Feature2D.ORB");
      options_mtx_.unlock();
    }

    std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > img_info_pair( im_sub_->getData() );

    keypoints_c_.reset( new std::vector<cv::KeyPoint> );
    kp_tags_c_.reset( new std::vector<int> );
    kp_feature_length_c_.reset( new std::vector<int> );
    descriptors_c_.reset( new cv::Mat );

    cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvShare( img_info_pair.first, sensor_msgs::image_encodings::MONO8 );

    orb_detector_->detect( cv_ptr->image, *keypoints_c_ );
    orb_detector_->compute( cv_ptr->image, *keypoints_c_, *descriptors_c_ );


    std::vector< cv::KeyPoint > m1, m2, i1, i2;
    std::vector< cv::DMatch > gm;
    std::vector< std::pair<int,int> > mp, ip; // match pairs, inlier pairs

    // all keypoints get set to a new tag number
    kp_tags_c_->resize( keypoints_c_->size() );
    for( int i = 0; i < kp_tags_c_->size(); i++ )
      kp_tags_c_->at(i) = cur_tag_num_++;
    kp_feature_length_c_->resize( keypoints_c_->size() );
    for( int i = 0; i < kp_feature_length_c_->size(); i++ )
      kp_feature_length_c_->at(i) = 0;


    if( keypoints_p_ != NULL && keypoints_c_->size() > 0 && keypoints_p_->size() > 0 )
    {
      std::vector< std::vector<cv::DMatch> > nn_matches;
      // the previous is the "train" and the current is the "query"
      matcher_->knnMatch( *descriptors_c_, *descriptors_p_, nn_matches, 2 );

      for( int i = 0; i < nn_matches.size(); i++ )
      {
        cv::DMatch* f = &(nn_matches[i][0]);
        if( nn_matches[i][0].distance < nn_max_dist_ratio_ * nn_matches[i][1].distance )
        {
          m1.push_back(keypoints_c_->at( f->queryIdx ) );
          m2.push_back(keypoints_p_->at( f->trainIdx ) );
          mp.push_back( std::make_pair(f->trainIdx, f->queryIdx) );
        }
      }

      cv::Mat col = cv::Mat::ones(3,1,CV_64F);
      for( int i = 0; i < m1.size(); i++ )
      {
        col.at<double>(0) = m1[i].pt.x;
        col.at<double>(1) = m1[i].pt.y;
        col.at<double>(2) = 1.0;
        col = homography_ * col;
        col /= col.at<double>(2);
        double dist = sqrt( pow((col.at<double>(0)-m2[i].pt.x),2) + pow((col.at<double>(1)-m2[i].pt.y),2) );

        if( dist < inlier_threshold_ )
        {
          int ni = static_cast<int>(i1.size());
          i1.push_back(m1[i]);
          i2.push_back(m2[i]);
          ip.push_back(mp[i]);
          gm.push_back( cv::DMatch(ni,ni,0) );
        }
      }

      // tags that match previous get updated to match
      int max = 0;
      for( int i = 0; i < ip.size(); i++ )
      {
        int length = kp_feature_length_p_->at( ip[i].first ) + 1;
        if(length > max)
          max = length;
        kp_feature_length_c_->at( ip[i].second ) = length;
        kp_tags_c_->at( ip[i].second ) = kp_tags_p_->at( ip[i].first );
      }

      usma_perception_msgs::FeatureArray fa;
      fa.header = cv_ptr->header;
      fa.features.resize( ip.size() );
      usma_perception_msgs::FeatureTag* ft;
      for( int i = 0; i < ip.size(); i++ )
      {
        ft = &( fa.features[i] );
        ft->x = (keypoints_c_->at( ip[i].second ).pt.x - img_info_pair.second->K[2]) / img_info_pair.second->K[0];
        ft->y = (keypoints_c_->at( ip[i].second ).pt.y - img_info_pair.second->K[4]) / img_info_pair.second->K[5];
        ft->id = kp_tags_c_->at( ip[i].second );
      }
      feature_pub_->publish( fa );

      if( image_overlay_pub_->getNumSubscribers() )
      {
        cv::drawKeypoints( cv_ptr->image, *keypoints_c_, keydrawn1 );
        cv::drawKeypoints( p_img_->image, *keypoints_p_, keydrawn2 );
        cv::drawMatches( keydrawn1, i1, keydrawn2, i2, gm, matchdrawn );
        cv_bridge::CvImage out_msg;
        out_msg.header = cv_ptr->header;
        out_msg.encoding = sensor_msgs::image_encodings::RGB8;
        out_msg.image = matchdrawn;
        sensor_msgs::CameraInfo nci;
        nci.header = img_info_pair.second->header;
        nci.height = matchdrawn.rows;
        nci.width =  matchdrawn.cols;
        sensor_msgs::Image ni = *out_msg.toImageMsg();
        ni.height = nci.height;
        ni.width = nci.width;
        ni.encoding = sensor_msgs::image_encodings::RGB8;
        image_overlay_pub_->publish( ni, nci );
      }
    }

    keypoints_p_.swap( keypoints_c_ );
    descriptors_p_.swap( descriptors_c_ );
    kp_tags_p_.swap( kp_tags_c_ );
    kp_feature_length_p_.swap( kp_feature_length_c_ );
    p_img_ = cv_ptr;
    return;
  }
}
