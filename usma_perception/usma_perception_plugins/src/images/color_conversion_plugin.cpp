#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/color_conversion_plugin.h>
#include <usma_perception_plugins/images/image_plugin_base.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ColorConversionPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ColorConversionPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string ColorConversionPlugin::setShortName()
  {
    return std::string( "color_converter" );
  }

  bool ColorConversionPlugin::isInputReady( std::vector<bool> has_new_image_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_image_data.size(); i++ )
      if( has_new_image_data[i] == true )
        return true;
  }

  void ColorConversionPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    goal_color_ = getOption<std::string>("goal_color","mono8");
  }

  void ColorConversionPlugin::processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair< sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector<bool> &should_publish )
  {
    // if we are dividing by one don't do anything unnecessary
    if( goal_color_ == in[0].first->encoding )
    {
      *(out[0].first) = *(in[0].first);
      *(out[0].second) = *(in[0].second);
      should_publish[0] = true;
      return;
    }

    cv::Mat cv_image;
    try
    {
      cv_image = cv_bridge::toCvShare(in[0].first, in[0].first->encoding)->image;
    }
    catch (cv::Exception &e)
    {
      ROS_ERROR("Could not create cv::Mat");
      return;
    }

    cv::Mat buffer;
    cv::cvtColor(cv_image, buffer, CV_RGB2GRAY);

    *(out[0].first) = *(cv_bridge::CvImage(in[0].first->header, goal_color_, buffer).toImageMsg());

    for( int i = 0; i < should_publish.size(); i++ )
       should_publish[i] = true;

    *(out[0].second) = *(in[0].second);

    return;
  }
 }
