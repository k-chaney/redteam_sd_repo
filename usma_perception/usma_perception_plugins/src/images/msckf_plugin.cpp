#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/msckf_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::MSCKFPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void MSCKFPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("MSCKF");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("usma_perception_msgs::FeatureArray"); // an input feature array
    it.push_back("sensor_msgs::Imu"); // an input feature array
  }

  bool MSCKFPlugin::checkInputReady()
  {
    return imu_sub_->dataReady();
  }

  void MSCKFPlugin::init()
  {
    feature_sub_ = boost::any_cast< boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::FeatureArray::ConstPtr> > >( subs_[0] );
    imu_sub_ = boost::any_cast< boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > >( subs_[1] );

    processOptions();

    // configure camera parameters -- only need to populate q_CI_ and p_CI_
    // because we are recieving idealized feature measurments
    tf_listener_.reset( new tf::TransformListener( ros::Duration(60.0) ) );
    try
    {
      tf::StampedTransform transform;
      ros::Time t = ros::Time::now();
      tf_listener_->waitForTransform( imu_frame_, cam_frame_, t, ros::Duration(3.0) );
      tf_listener_->lookupTransform( imu_frame_, cam_frame_, t, transform );

      tf::Quaternion q = transform.getRotation().normalized();
      cam_params_.q_CI_(0) = q.getAxis().getX();
      cam_params_.q_CI_(1) = q.getAxis().getY();
      cam_params_.q_CI_(2) = q.getAxis().getZ();
      cam_params_.q_CI_(3) = q.getW();
      cam_params_.p_CI_(0) = transform.getOrigin().getX();
      cam_params_.p_CI_(1) = transform.getOrigin().getY();
      cam_params_.p_CI_(2) = transform.getOrigin().getZ();

    }
    catch (tf::TransformException ex)
    {
      ROS_ERROR("Transform from IMU to Camera frame not found, assuming the Identity");

      cam_params_.q_CI_(0) = 0.0;
      cam_params_.q_CI_(1) = 0.0;
      cam_params_.q_CI_(2) = 0.0;
      cam_params_.q_CI_(3) = 1.0;
      cam_params_.p_CI_.setZero();
    }

    // configure the noise parameters of the IMU
    // wait for data to be ready from the IMU and store just one frame of it
    boost::unique_lock<boost::mutex> l(ss_mtx_);
    while(!imu_sub_->dataReady())
      calc_spike_->wait(l);

    previous_time_ = ros::Time::now();

    sensor_msgs::Imu::ConstPtr cimu = imu_sub_->getData();
    populateIMUMeasurment( cimu, cur_measurement_ );

    // populate our initial measurment -- already have an IMU frame
    // now wait for a camera frame to go with it
    while(!feature_sub_->dataReady())
      calc_spike_->wait(l);

    usma_perception_msgs::FeatureArray::ConstPtr cfa = feature_sub_->getData();
    populateFeatureMeasurment( cfa, cur_measurement_ );

    msckf::imuState initial_imu;
    initial_imu.q_IG_.setZero();
    initial_imu.p_IG_.setZero();
    initial_imu.b_g_.setZero();
    initial_imu.b_v_.setZero();

    // ready to initialize the MSCKF, now ready for continuous work
    msckf_.reset( new msckf::MSCKF( initial_imu, cur_measurement_, cam_params_, 0, noise_params_, msckf_params_ ) );
  }

  void MSCKFPlugin::processOptions()
  {
    imu_frame_ = getOption<std::string>("imu_frame","imu");
    cam_frame_ = getOption<std::string>("camera_optical_frame","camera");
    noise_params_.u_var_prime_ = getOption( "cam_u_var_prime", 1.0 );
    noise_params_.v_var_prime_ = getOption( "cam_v_var_prime", 1.0 );

    // get the msckf parameters that are being used
    msckf_params_.min_track_length_ = getOption( "min_track_length", 5 );
    msckf_params_.max_track_length_ = getOption( "max_track_length", 15 );
    msckf_params_.max_gn_cost_norm_ = getOption("max_gn_cost_norm",0.01);
    msckf_params_.min_rcond_ = getOption("min_gn_cost_norm",0.00);
    msckf_params_.do_null_space_trick_ = getOption("do_null_space_trick", true);
    msckf_params_.do_qr_decomp_ = getOption("do_qr_decomp", true);
    msckf_params_.is_velocity_ = getOption("is_velocity_update", true);
  }

  void MSCKFPlugin::workLoad()
  {
    ros::Time cur_time = ros::Time::now();

    sensor_msgs::Imu::ConstPtr cimu = imu_sub_->getData();
    populateIMUMeasurment( cimu, cur_measurement_ );

    if(feature_sub_->dataReady())
    {
      usma_perception_msgs::FeatureArray::ConstPtr cfa = feature_sub_->getData();
      populateFeatureMeasurment( cfa, cur_measurement_ );
    }

    previous_time_ = cur_time; 
  }

  void MSCKFPlugin::populateIMUMeasurment( sensor_msgs::Imu::ConstPtr &imu, msckf::measurement &m_k )
  {
  }

  void MSCKFPlugin::populateFeatureMeasurment( usma_perception_msgs::FeatureArray::ConstPtr &features, msckf::measurement &m_k )
  {
  }
}
