#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/resize_plugin.h>
#include <usma_perception_plugins/images/image_plugin_base.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ImageResizePlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ImageResizePlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string ImageResizePlugin::setShortName()
  {
    return std::string( "resizer" );
  }

  bool ImageResizePlugin::isInputReady( std::vector<bool> has_new_image_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_image_data.size(); i++ )
      if( has_new_image_data[i] == true )
        return true;
  }

  void ImageResizePlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    divider_ = getOption("divider",1.0);
  }

  void ImageResizePlugin::processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair< sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector<bool> &should_publish )
  {
    // if we are dividing by one don't do anything unnecessary
    if( fabs(divider_ - 1.0) <= std::numeric_limits<double>::epsilon() )
    {
      *(out[0].first) = *(in[0].first);
      *(out[0].second) = *(in[0].second);
      should_publish[0] = true;
      return;
    }

    cv::Mat cv_image;
    try
    {
      cv_image = cv_bridge::toCvShare(in[0].first, in[0].first->encoding)->image;
    }
    catch (cv::Exception &e)
    {
      ROS_ERROR("Could not convert from '%s' to '%s'.", in[0].first->encoding.c_str(), in[0].first->encoding.c_str());
      return;
    }

    // Rescale image
    int new_width = (cv_image.cols / divider_) + 0.5;
    int new_height = (cv_image.rows / divider_) + 0.5;
    cv::Mat buffer;
    cv::resize(cv_image, buffer, cv::Size(new_width, new_height));

    *(out[0].first) = *(cv_bridge::CvImage(in[0].first->header, in[0].first->encoding.c_str(), buffer).toImageMsg());

    for( int i = 0; i < should_publish.size(); i++ )
       should_publish[i] = true;

    *(out[0].second) = *(in[0].second);

    // TODO :: Gotta figure this resizing piece out.....

    for( int i = 0; i < out[0].second->P.size(); i++ )
      out[0].second->P[i] /= divider_;
    out[0].second->P[ out[0].second->P.size() - 2 ] = 1.0;
    
    for( int i = 0; i < out[0].second->K.size(); i++ )
      out[0].second->K[i] /= divider_;
    out[0].second->K[ out[0].second->K.size() - 1 ] = 1.0;

    out[0].second->height = new_height;
    out[0].second->width = new_width;

    return;
  }
 }
