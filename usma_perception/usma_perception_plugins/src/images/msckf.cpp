#include <usma_perception_plugins/images/msckf.h>
#include <ros/ros.h>

// this causes a lot of large matricies to be printed -- only run a couple states
#ifdef SET_MSCKF_DEBUG
#define SET_MSCKF_CORE_DEBUG
#define MSCKF_DEBUG ROS_INFO
#define MSCKF_DEBUG_STREAM ROS_INFO_STREAM
#endif

#if (defined(SET_MSCKF_DEBUG) || defined(SET_MSCKF_CORE_DEBUG))
#define MSCKF_CORE_DEBUG ROS_INFO
#define MSCKF_CORE_DEBUG_STREAM ROS_INFO_STREAM
#define FFL_INFO {MSCKF_CORE_DEBUG("%s::%s::%d",__FILE__,__func__,__LINE__);}
#define MSCKF_INFO(X) {FFL_INFO; \
  MSCKF_CORE_DEBUG("CS Size :: %d", X.cam_states_.size());\
  for(int i = 0; i < X.cam_states_.size(); i++) MSCKF_CORE_DEBUG( "CS_TF%d :: size %d", i, X.cam_states_[i].tracked_features_.size() );\
  MSCKF_CORE_DEBUG("CC Size :: %d rows by %d cols", X.cam_covar_.rows(), X.cam_covar_.cols());\
  MSCKF_CORE_DEBUG("ICC Size:: %d rows by %d cols", X.imu_cam_covar_.rows(), X.imu_cam_covar_.cols());\
}
#endif
#define FFL_ERROR {ROS_ERROR("%s::%s::%d",__FILE__,__func__,__LINE__);}
namespace usma_perception
{
  namespace msckf
  {
    MSCKF::MSCKF( imuState &is, measurement &m_k, camera &cam, int kStart, noiseParams &np, msckfParams &mp)
    {
      msckf_params_ = mp;
      noise_params_ = np;
      camera_params_ = cam;
      msckf_state_.imu_state_ = is;
      msckf_state_.imu_covar_ = np.initial_IMU_covar_;
      msckf_state_.cam_covar_.resize(0,0);
      msckf_state_.imu_cam_covar_.resize(0,0);
      msckf_state_.cam_states_.clear();

      augmentState( msckf_state_, camera_params_, kStart );

      cur_state_k_ = kStart;

      for(std::map<int,pixelMeasurement>::iterator it=m_k.y_.begin(); it!=m_k.y_.end(); ++it)
      {
        feature_tracks_.insert( std::make_pair(it->first,std::vector<pixelMeasurement>()) );
        feature_tracks_[it->first].push_back( it->second );
        msckf_state_.cam_states_.back().tracked_features_.push_back( it->first );
      }
#ifdef SET_MSCKF_DEBUG
      MSCKF_INFO( msckf_state_ );
#endif
      p_m_k_ = m_k;
    }

    void MSCKF::update( measurement &m_k )
    {
      // shift measurments back one
#ifdef SET_MSCKF_DEBUG
      MSCKF_DEBUG("============================================");
      MSCKF_DEBUG("============================================");
      MSCKF_DEBUG("============================================");
      MSCKF_DEBUG("============================================");
#endif
#ifdef SET_MSCKF_DEBUG
      MSCKF_DEBUG("===============Prev IMU STATE===============");
      MSCKF_DEBUG_STREAM( "q_IG " << msckf_state_.imu_state_.q_IG_.transpose() );
      MSCKF_DEBUG_STREAM( "p_IG " << msckf_state_.imu_state_.p_IG_.transpose() );
      MSCKF_DEBUG_STREAM( "b_g  " << msckf_state_.imu_state_.b_g_.transpose() );
      MSCKF_DEBUG_STREAM( "b_v  " << msckf_state_.imu_state_.b_v_.transpose() );
#endif
      // always propagate the state
      propagateMsckfStateAndCovar( msckf_state_, p_m_k_, noise_params_ );

#if (defined(SET_MSCKF_DEBUG) || defined(SET_MSCKF_CORE_DEBUG))
      MSCKF_INFO( msckf_state_ );
      MSCKF_CORE_DEBUG("State K %d", cur_state_k_ );
      MSCKF_CORE_DEBUG("===============IMU STATE====================");
      MSCKF_CORE_DEBUG_STREAM( "q_IG " << msckf_state_.imu_state_.q_IG_.transpose() );
      MSCKF_CORE_DEBUG_STREAM( "p_IG " << msckf_state_.imu_state_.p_IG_.transpose() );
      MSCKF_CORE_DEBUG_STREAM( "b_g  " << msckf_state_.imu_state_.b_g_.transpose() );
      MSCKF_CORE_DEBUG_STREAM( "b_v  " << msckf_state_.imu_state_.b_v_.transpose() );
      MSCKF_CORE_DEBUG("==================IMU=======================");
      MSCKF_CORE_DEBUG("%d cols by %d rows", msckf_state_.imu_covar_.cols(), msckf_state_.imu_covar_.rows() );
      MSCKF_CORE_DEBUG_STREAM( std::endl << msckf_state_.imu_covar_ );
      MSCKF_CORE_DEBUG("================IMU CAM=====================");
      MSCKF_CORE_DEBUG("%d cols by %d rows", msckf_state_.imu_cam_covar_.cols(), msckf_state_.imu_cam_covar_.rows() );
      MSCKF_CORE_DEBUG_STREAM( std::endl << msckf_state_.imu_cam_covar_ );
      MSCKF_CORE_DEBUG("==================CAM=======================");
      MSCKF_CORE_DEBUG("%d cols by %d rows", msckf_state_.cam_covar_.cols(), msckf_state_.cam_covar_.rows() );
      MSCKF_CORE_DEBUG_STREAM( std::endl << msckf_state_.cam_covar_ );

      MSCKF_CORE_DEBUG("=============prev measurment================");
      MSCKF_CORE_DEBUG("dT :: %f", p_m_k_.dT_);
      MSCKF_CORE_DEBUG_STREAM( p_m_k_.omega_.transpose() );
      MSCKF_CORE_DEBUG_STREAM( p_m_k_.v_.transpose() );
      MSCKF_CORE_DEBUG("================measurment==================");
      MSCKF_CORE_DEBUG("dT :: %f", m_k.dT_);
      MSCKF_CORE_DEBUG_STREAM( m_k.omega_.transpose() );
      MSCKF_CORE_DEBUG_STREAM( m_k.v_.transpose() );
#endif
      // the state index updates here because the propagateMsckf runs off of the previous measurment
      cur_state_k_++;

      // if there are new observations augment the state and redo feature tracking
      if( m_k.y_.size() > 0 )
      {
        augmentState( msckf_state_, camera_params_, cur_state_k_ );



        // if a feature isn't in this map or the entry is false,
        // don't maintain the feature
        std::map<int,bool> maintain_track;

        for(std::map<int,pixelMeasurement>::iterator it=m_k.y_.begin(); it!=m_k.y_.end(); ++it)
        {
          // if the track doesn't exist insert it with a blank vector
          if( feature_tracks_.find(it->first) == feature_tracks_.end() )
          {
            feature_tracks_.insert( std::make_pair(it->first,std::vector<pixelMeasurement>() ) );
          }
          // push the measurment into the feature track
          feature_tracks_[ it->first ].push_back( it->second );

          // if the track is too long stop maintaining
          if(feature_tracks_[it->first].size() >= msckf_params_.max_track_length_)
            maintain_track.insert( std::make_pair(it->first,false) );
          else
            maintain_track.insert( std::make_pair(it->first,true) );

          // add this feature to the ones tracked by the current camera state
          msckf_state_.cam_states_.back().tracked_features_.push_back( it->first );
        }

        // TODO condense into one thing to update
        std::map<int,std::vector<pixelMeasurement> > features_to_residualize;
        std::map<int,std::vector<cameraState> > ftr_cam_states;
        std::map<int,std::vector<int> > ftr_cam_indices;

        std::vector<int> tracks_to_remove;
        for(std::map<int,std::vector<pixelMeasurement> >::iterator it=feature_tracks_.begin(); it!=feature_tracks_.end(); ++it)
        {
          if( maintain_track.find(it->first) == maintain_track.end() || !maintain_track[it->first] )
          {
            // remove the feature and update the state
            remove_feature_ret rfr = removeTrackedFeature( msckf_state_, it->first );

            tracks_to_remove.push_back( it->first );
            if( it->second.size() >= msckf_params_.min_track_length_ )
            {
#ifdef SET_MSCKF_DEBUG
              MSCKF_DEBUG("Feature index %d", it->first);
#endif
              // if the feature should be residualized then put it into the maps
              features_to_residualize.insert( *it );
              ftr_cam_states.insert( std::make_pair( it->first, rfr.feat_cam_states_ ) );
              ftr_cam_indices.insert( std::make_pair( it->first, rfr.cam_state_indices_ ) );
            }
          }
          else
          {
            maintain_track[it->first] = true;
          }
        }
#ifdef SET_MSCKF_DEBUG
        MSCKF_DEBUG("Removing %d feature tracks", tracks_to_remove.size());
        for(std::map<int,std::vector<pixelMeasurement> >::iterator it=feature_tracks_.begin(); it!=feature_tracks_.end(); ++it)
        {
          std::ostringstream s0;
          std::ostringstream s1;
          for( int i = 0; i < it->second.size(); i++ )
          {
            s0 << it->second[i](0) << "\t";
            s1 << it->second[i](1) << "\t";
          }
          MSCKF_DEBUG_STREAM( "F==" << it->first << "::R" << maintain_track[it->first] << std::endl << s0.str() << std::endl << s1.str() );
        }

        std::ostringstream ttr;
        for(int i = 0; i < tracks_to_remove.size(); i++)
          ttr << tracks_to_remove[i] << ",\t";
        MSCKF_DEBUG_STREAM( "Tracks to remove:  " << ttr.str() );

        std::ostringstream ftr;
        for(std::map<int,std::vector<pixelMeasurement> >::iterator it=features_to_residualize.begin(); it!=features_to_residualize.end(); ++it)
          ftr << it->first << ",\t";
        MSCKF_DEBUG_STREAM( "Features to residualize:  " << ftr.str() );
#endif

        std::sort(tracks_to_remove.begin(), tracks_to_remove.end()); // needs to be sorted to be removed in the proper order
        for( int i = tracks_to_remove.size() - 1; i >= 0; i-- )
        {
          // actually erase the feature track
          feature_tracks_.erase( tracks_to_remove[i] );
        }

        // feature residual corrections
        if( !features_to_residualize.empty() )
        {
          // resizing doesn't keep current values....so store components and construct after the loop
          Eigen::MatrixXd H_o;
          Eigen::MatrixXd r_o;
          Eigen::MatrixXd R_o;

          std::vector<Eigen::MatrixXd> H_o_comps;
          std::vector<Eigen::MatrixXd> r_o_comps;
          std::vector<Eigen::MatrixXd> R_o_comps;
          H_o_comps.resize(features_to_residualize.size());
          r_o_comps.resize(features_to_residualize.size());
          R_o_comps.resize(features_to_residualize.size());
          int comps_ind = 0;

          for(std::map<int,std::vector<pixelMeasurement> >::iterator it=features_to_residualize.begin(); it!=features_to_residualize.end(); ++it)
          {
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG("Cam states size %d observations size %d", ftr_cam_states[it->first].size(), it->second.size() );
            for( int i = 0; i < ftr_cam_states[it->first].size(); i++ )
            {
              MSCKF_DEBUG_STREAM( "Cam " << i << std::endl << "pCG::" << ftr_cam_states[it->first][i].p_CG_.transpose()  << std::endl << "qCG::" << ftr_cam_states[it->first][i].q_CG_.transpose() );
            }
#endif

            // estimate the 3D location of the feature -- Gauss Newton inv depth opt
            gn_pos_est_ret gn_return = calcGNPosEst( ftr_cam_states[it->first], it->second, noise_params_ );

#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "GN p_f_G " << gn_return.p_f_G_.transpose() << std::endl );
            std::ostringstream s0;
            std::ostringstream s1;
            for( int i = 0; i < it->second.size(); i++ )
            {
              s0 << it->second[i](0) << "\t";
              s1 << it->second[i](1) << "\t";
            }
            MSCKF_DEBUG_STREAM( "F==" << it->first << std::endl << s0.str() << std::endl << s1.str() );
#endif

            double n_obs = (double)( it->second.size() );
            double J_cost_norm = gn_return.J_ / n_obs;

            if( J_cost_norm > msckf_params_.max_gn_cost_norm_ || gn_return.reciprocal_condition_num_ < msckf_params_.min_rcond_ )
            {
              break;
            }
           
            Eigen::MatrixXd r_j = calcResidual( gn_return.p_f_G_, ftr_cam_states[it->first], it->second );
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "r_j" << std::endl << r_j << std::endl );
#endif
            Eigen::MatrixXd R_j(r_j.rows(), r_j.rows());
            Eigen::MatrixXd AjT;
            R_j.setZero();
            for(int i = 0; i < r_j.rows(); i++)
              R_j(i,i) = ( (i%2==0) ? noise_params_.u_var_prime_ : noise_params_.v_var_prime_ );
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "R_j" << std::endl << R_j << std::endl << std::endl );
#endif

            calc_hoj_ret hoj_ret;
            calcHoj( gn_return.p_f_G_, msckf_state_, ftr_cam_indices[it->first], hoj_ret.H_o_j_, hoj_ret.A_j_, hoj_ret.H_x_j_ );
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM("A_j" << std::endl << hoj_ret.A_j_ << std::endl );
#endif

            // TODO Check to see if this works
            if( msckf_params_.do_null_space_trick_ )
            {
              H_o_comps[comps_ind] = hoj_ret.H_o_j_;
              if( hoj_ret.A_j_.cols() > 0 && hoj_ret.A_j_.rows() > 0 )
              {
                AjT = hoj_ret.A_j_.transpose();
                r_o_comps[comps_ind] = AjT * r_j;
                R_o_comps[comps_ind] = AjT * R_j * hoj_ret.A_j_;
              }
              comps_ind++;
            }
            else
            {
              H_o_comps[comps_ind] = hoj_ret.H_x_j_;
              r_o_comps[comps_ind] = r_j;
              R_o_comps[comps_ind] = R_j;
              comps_ind++;
            }
          }

          // compute H_o based off of stored components -- only one resize needed
          int nrows = 0;
          int ncols = 0;
          for(int i = 0; i < H_o_comps.size(); i++)
          {
            nrows += H_o_comps[i].rows();
            ncols = H_o_comps[i].cols();
          }
          H_o.resize(nrows,ncols);
          H_o.setZero();
          nrows = 0;
          for(int i = 0; i < H_o_comps.size(); i++)
          {
            H_o.block(nrows,0,H_o_comps[i].rows(),H_o_comps[i].cols()) = H_o_comps[i];
            nrows += H_o_comps[i].rows();
          }
          // compute r_o based off of stored components -- only one resize needed
          nrows = 0;
          ncols = 0;
          for(int i = 0; i < r_o_comps.size(); i++)
          {
            nrows += r_o_comps[i].rows();
            ncols = r_o_comps[i].cols();
          }
          r_o.resize(nrows,ncols);
          r_o.setZero();
          nrows = 0;
          for(int i = 0; i < r_o_comps.size(); i++)
          {
            r_o.block(nrows,0,r_o_comps[i].rows(),r_o_comps[i].cols()) = r_o_comps[i];
            nrows += r_o_comps[i].rows();
          }
          // compute r_o based off of stored components -- only one resize needed
          nrows = 0;
          ncols = 0;
          for(int i = 0; i < R_o_comps.size(); i++)
          {
            nrows += R_o_comps[i].rows();
            ncols += R_o_comps[i].cols();
          }
          R_o.resize(nrows,ncols);
          R_o.setZero();
          nrows = 0;
          ncols = 0;
          for(int i = 0; i < R_o_comps.size(); i++)
          {
            R_o.block(nrows,ncols,R_o_comps[i].rows(),R_o_comps[i].cols()) = R_o_comps[i];
            nrows += R_o_comps[i].rows();
            ncols += R_o_comps[i].cols();
          }
#ifdef SET_MSCKF_DEBUG
          MSCKF_DEBUG_STREAM( "H_o" << std::endl << H_o << std::endl << std::endl );
          MSCKF_DEBUG_STREAM( "r_o" << std::endl << r_o << std::endl << std::endl );
          MSCKF_DEBUG_STREAM( "R_o" << std::endl << R_o << std::endl << std::endl );
#endif

          if( r_o.rows() > 0 && r_o.cols() > 0 )
          {
            std::pair<Eigen::MatrixXd,Eigen::MatrixXd> th_ret;
            Eigen::MatrixXd r_n;
            Eigen::MatrixXd R_n;
            Eigen::MatrixXd Q1T;

            if( false && msckf_params_.do_qr_decomp_ )
            {
              th_ret = calcTH( H_o );
              Q1T = th_ret.second.transpose();
              r_n = Q1T * r_o;
              R_n = Q1T * R_o * th_ret.second;
            }
            else
            {
              th_ret.first = H_o;
              r_n = r_o;
              R_n = R_o;
            }
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "T_H" << std::endl << th_ret.first << std::endl );
#endif

            // build the full covariance matrix
            Eigen::MatrixXd P( msckf_state_.imu_covar_.rows() + msckf_state_.cam_covar_.rows(),
                msckf_state_.imu_covar_.cols() + msckf_state_.cam_covar_.cols() );

            P.block<12,12>(0,0) = msckf_state_.imu_covar_;
            P.block(0,12,msckf_state_.imu_cam_covar_.rows(),msckf_state_.imu_cam_covar_.cols()) = msckf_state_.imu_cam_covar_;
            P.block(12,0,msckf_state_.imu_cam_covar_.cols(),msckf_state_.imu_cam_covar_.rows()) = msckf_state_.imu_cam_covar_.transpose();
            P.block(12,12,msckf_state_.cam_covar_.rows(),msckf_state_.cam_covar_.cols()) = msckf_state_.cam_covar_;
            //for( int i = 0; i < P.rows(); i++)
            //  for( int j = 0; j < P.cols(); j++)
            //    if( P(i,j) <= std::numeric_limits<double>::epsilon() )
            //      P(i,j) = 0.0;

#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "P" << std::endl << P << std::endl << std::endl );
#endif
            // compute the Kalman gain
            Eigen::MatrixXd K = calcK( th_ret.first, P, R_n );

#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG( "K size %d rows by %d cols", K.rows(), K.cols() );
            MSCKF_DEBUG_STREAM( "K" << std::endl << K << std::endl << std::endl );
#endif
            Eigen::MatrixXd deltaX = K * r_n;
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG_STREAM( "deltaX rows" << deltaX.rows() << " x cols" << deltaX.cols() << std::endl << deltaX << std::endl << std::endl );
#endif
            msckf_state_ = updateState( msckf_state_, deltaX );

            int s = 12 + 6 * msckf_state_.cam_states_.size();
#ifdef SET_MSCKF_DEBUG
            MSCKF_DEBUG( "Eye size %d", s );
            MSCKF_DEBUG( "TH size %d rows by %d cols", th_ret.first.rows(), th_ret.first.cols() );
#endif

            Eigen::MatrixXd tempM = ( Eigen::MatrixXd::Identity(s,s) - K * th_ret.first );
            Eigen::MatrixXd Pc = (tempM * P * tempM.transpose()) + ( K * R_n * K.transpose() );

            msckf_state_.imu_covar_ = Pc.block<12,12>(0,0);
            msckf_state_.cam_covar_ = Pc.block(12,12,Pc.rows()-12,Pc.cols()-12);
            msckf_state_.imu_cam_covar_ = Pc.block(0,12,12,Pc.cols()-12);
          }
        }

        // Finally prune states
        pruneStates( msckf_state_ );
      }
      p_m_k_ = m_k;
    }
  }
}
