#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/threshold_plugin.h>
#include <usma_perception_plugins/images/image_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ImageThresholdPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ImageThresholdPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string ImageThresholdPlugin::setShortName()
  {
    return std::string( "threshold" );
  }

  bool ImageThresholdPlugin::isInputReady( std::vector<bool> has_new_image_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_image_data.size(); i++ )
      if( has_new_image_data[i] == true )
        return true;
  }

  void ImageThresholdPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    num_channels_ = getOption("num_channels",1);
    mins_.resize( num_channels_ );
    maxs_.resize( num_channels_ );
    for( int i=0; i<num_channels_; i++ )
    {
      mins_[i] = getOption<double>(std::string("min_")+boost::lexical_cast<std::string>(i),0.0);
      maxs_[i] = getOption<double>(std::string("max_")+boost::lexical_cast<std::string>(i),1.0);
    }
  }

  void ImageThresholdPlugin::processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair< sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector<bool> &should_publish )
  {
    *(out[0].first) = *(in[0].first);
    *(out[0].second) = *(in[0].second);

    int mval;
    std::vector<int> min_cvals;
    std::vector<int> max_cvals;
    min_cvals.resize( num_channels_ );
    max_cvals.resize( num_channels_ );

    int bd = sensor_msgs::image_encodings::bitDepth( out[0].first->encoding );
    int num_channels = sensor_msgs::image_encodings::numChannels( out[0].first->encoding );
    if( num_channels != num_channels_ )
    {
      ROS_ERROR("[%s]::Wrong number of channels", getShortName().c_str());
      return;
    }

    if( bd==8 )
      mval = (int)std::numeric_limits<uint8_t>::max();
    else
    {
      ROS_ERROR("[%s]::Only works for 8 bit images right now", getShortName().c_str());
      return;
    }

    for( int i=0; i<num_channels_; i++ )
    {
      min_cvals[i] = mval * mins_[i];
      max_cvals[i] = mval * maxs_[i];
    }

    uint8_t* image = &(out[0].first->data.front());

    for(int i=0; i<out[0].first->height*out[0].first->width*num_channels; i++)
      if(image[i] > min_cvals[i%num_channels] && image[i] < max_cvals[i%num_channels])
        image[i] = mval;
      else
        image[i] = 0;

    should_publish[0] = true;
    return;
  }
  
 }
