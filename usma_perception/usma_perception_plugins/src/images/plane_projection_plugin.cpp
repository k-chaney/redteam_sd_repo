#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/plane_projection_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::PlaneProjectionPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void PlaneProjectionPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
  {
    // set the short name to be used
    sn = std::string("PlaneProjection");

    // set the types that are being used for this plugin
    ot.clear();
    it.clear();
    it.push_back("image_transport::Camera"); // input image
    ot.push_back("pcl::PCLPointCloud2"); // overlay of features onto the image
  }

  bool PlaneProjectionPlugin::checkInputReady()
  {
    return im_sub_->dataReady();
  }

  void PlaneProjectionPlugin::init()
  {
    new_options_ready_ = true;

    im_sub_ = boost::any_cast<boost::shared_ptr<ManagedCameraSubscriber> >( subs_[0] );
    point_cloud_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >( pubs_[0] );
  }

  void PlaneProjectionPlugin::processOptions()
  {
    offset_ = getOption<double>("ground_offset",0.0);
    thresh_ = getOption<int>("thresh",32);
    horizon_ = getOption<double>("horizon_location",1.0);
    ground_frame_ = getOption<std::string>("ground_frame", "base_link");
    sensor_frame_ = getOption<std::string>("sensor_frame", sensor_frame_);
  }

  void PlaneProjectionPlugin::workLoad()
  {
    std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > img_info_pair( im_sub_->getData() );

    if( new_options_ready_ )
    {
      sensor_frame_ = img_info_pair.second->header.frame_id;

      options_mtx_.lock();
      processOptions();
      options_mtx_.unlock();

      std::string er_msg;
      if ( tf_listener_.canTransform(sensor_frame_,ground_frame_,ros::Time(0),&er_msg) )
      {
          tf_listener_.lookupTransform(sensor_frame_,ground_frame_,ros::Time(0), T_ground_to_camera_);
          new_options_ready_ = false;
      }
      else
      {
          ROS_ERROR("[%s]::Can not complete transform::%s", getShortName().c_str(), er_msg.c_str());
          return;
      }
      R_ground_to_camera_.setIdentity();
      R_ground_to_camera_.setRotation( T_ground_to_camera_.getRotation() );

      T_camera_to_ground_ = T_ground_to_camera_;
      T_camera_to_ground_.setData(T_ground_to_camera_.inverse());
      R_camera_to_ground_.setIdentity();
      R_camera_to_ground_.setRotation( T_camera_to_ground_.getRotation() );


    }

    if( img_info_pair.first->encoding != sensor_msgs::image_encodings::MONO8 )
      return;

    cv_bridge::CvImageConstPtr cv_ptr = cv_bridge::toCvShare( img_info_pair.first, sensor_msgs::image_encodings::MONO8 );

    image_geometry::PinholeCameraModel pcm;
    pcm.fromCameraInfo( img_info_pair.second );

    pcl::PointCloud<pcl::PointXYZ> proj_cloud;
    proj_cloud.clear();
    cv::Point3d cv_tmp;
    pcl::PointXYZ pcl_tmp;

    const uint8_t* image = &(img_info_pair.first->data.front());

    for( int v=(img_info_pair.first->height) - (img_info_pair.first->height) * horizon_; v<(img_info_pair.first->height); v++ )
    {
      for( int u=0; u<(img_info_pair.first->width); u++ )
      {
        if( image[v*(img_info_pair.first->width)+u] > thresh_ )
        {
          cv_tmp = pcm.projectPixelTo3dRay( cv::Point2d(u,v) );
          proj_cloud.push_back( pcl::PointXYZ( cv_tmp.x, cv_tmp.y, cv_tmp.z ) );
        }
      }
    }
    
    pcl_ros::transformPointCloud(proj_cloud, proj_cloud, R_camera_to_ground_);
    pcl_tmp.x = T_camera_to_ground_.getOrigin().getX();
    pcl_tmp.y = T_camera_to_ground_.getOrigin().getY();
    pcl_tmp.z = T_camera_to_ground_.getOrigin().getZ();

    for( int i=0; i<proj_cloud.size(); i++ )
    {
      double t = ( offset_-pcl_tmp.z )/(proj_cloud[i].z);

      proj_cloud[i].x = pcl_tmp.x + t * ( proj_cloud[i].x );
      proj_cloud[i].y = pcl_tmp.y + t * ( proj_cloud[i].y );
      proj_cloud[i].z = pcl_tmp.z + t * ( proj_cloud[i].z );
    }

    pcl::PCLPointCloud2::Ptr ros_out;
    ros_out.reset( new pcl::PCLPointCloud2() );
    pcl::toPCLPointCloud2( proj_cloud, *ros_out );

    ros_out->header.frame_id = ground_frame_; //img_info_pair.first->header.frame_id;//
    ros_out->header.seq = img_info_pair.first->header.seq;

    point_cloud_pub_->publish( ros_out );

    return;
  }
}
