#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/gaussian_blur_plugin.h>
#include <usma_perception_plugins/images/image_plugin_base.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::GaussianBlurPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void GaussianBlurPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string GaussianBlurPlugin::setShortName()
  {
    return std::string( "gaussian_blur" );
  }

  bool GaussianBlurPlugin::isInputReady( std::vector<bool> has_new_image_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_image_data.size(); i++ )
      if( has_new_image_data[i] == true )
        return true;
  }

  void GaussianBlurPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    sigma_ = getOption<double>( "sigma", 0.5 );
    size_ = getOption<int>( "size", 3 );
  }

  void GaussianBlurPlugin::processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair< sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector<bool> &should_publish )
  {
    cv::Mat cv_image;
    try
    {
      cv_image = cv_bridge::toCvShare(in[0].first, in[0].first->encoding)->image;
    }
    catch (cv::Exception &e)
    {
      ROS_ERROR("Could not create cv::Mat");
      return;
    }

    cv::Mat buffer;
    cv::GaussianBlur(cv_image, buffer, cv::Size(size_,size_), sigma_);

    *(out[0].first) = *(cv_bridge::CvImage(in[0].first->header, in[0].first->encoding, buffer).toImageMsg());

    for( int i = 0; i < should_publish.size(); i++ )
       should_publish[i] = true;

    *(out[0].second) = *(in[0].second);

    return;
  }
 }
