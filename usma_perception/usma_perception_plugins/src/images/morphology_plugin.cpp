#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/images/morphology_plugin.h>
#include <usma_perception_plugins/images/image_plugin_base.h>

#include <opencv2/imgproc/imgproc.hpp>
#include <cv_bridge/cv_bridge.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::MorphologyPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void MorphologyPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string MorphologyPlugin::setShortName()
  {
    return std::string( "morph" );
  }

  bool MorphologyPlugin::isInputReady( std::vector<bool> has_new_image_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_image_data.size(); i++ )
      if( has_new_image_data[i] == true )
        return true;
  }

  void MorphologyPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    size_ = getOption<int>("kernel_size",1);
    type_ = getOption<std::string>("morph_type","open");
    iters_ = getOption<int>("iters",1);
  }

  void MorphologyPlugin::processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair< sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector<bool> &should_publish )
  {
    cv::Mat cv_image;
    try
    {
      cv_image = cv_bridge::toCvShare(in[0].first, in[0].first->encoding)->image;
    }
    catch (cv::Exception &e)
    {
      ROS_ERROR("Could not create cv::Mat");
      return;
    }

    cv::Mat buffer;
    cv::Mat strel = cv::getStructuringElement( cv::MORPH_RECT, cv::Size(size_, size_) );

    if( type_ == "open" )
      for( int i=0; i<iters_; i++ )
        cv::morphologyEx(cv_image, buffer, cv::MORPH_OPEN, strel);
    else if ( type_ == "close" )
      for( int i=0; i<iters_; i++ )
        cv::morphologyEx(cv_image, buffer, cv::MORPH_CLOSE, strel);
    else if ( type_ == "dilate" )
      for( int i=0; i<iters_; i++ )
        cv::dilate(cv_image, buffer, strel);
    else if ( type_ == "erode" )
      for( int i=0; i<iters_; i++ )
        cv::erode(cv_image, buffer, strel);
    else
      buffer = cv_image;

    *(out[0].first) = *(cv_bridge::CvImage(in[0].first->header, in[0].first->encoding, buffer).toImageMsg());

    for( int i = 0; i < should_publish.size(); i++ )
       should_publish[i] = true;

    *(out[0].second) = *(in[0].second);

    return;
  }
 }
