#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_cloud_generators/object_over_plane_gen_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::OOPPCGen, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void OOPPCGen::setShortName( std::string &sn )
  {
    // set the short name to be used
    sn = std::string("oop_pc_gen");
  }

  void OOPPCGen::processOptions()
  {
    goal_frame_ = getOption<std::string>("frame","map");
    x_size_ = getOption<double>("x_size",10.0);
    y_size_ = getOption<double>("y_size",10.0);
    resolution_ = getOption<double>("resolution",0.05);

    num_objects_ = getOption<int>("num_objects", 2);
    object_radius_ = getOption<double>("object_radius", 0.5);
    path_radius_ = getOption<double>("path_radius", 5.0);
    period_ = getOption<double>("period", 0.5);
  }

  void OOPPCGen::genPC( pcl::PCLPointCloud2::Ptr &out )
  {
    pcl::PointCloud<pcl::PointXYZ> output_pcl_cloud;

    int x_steps = x_size_ / resolution_;
    int y_steps = y_size_ / resolution_;

    std::vector<double> cur_obj_x;
    std::vector<double> cur_obj_y;

    for ( int i = 0; i < num_objects_; i++ )
    {
      cur_obj_x.push_back( path_radius_ * cos( 2.0 * M_PI / period_ * ((double)ros::Time::now().toSec()) + ( 2 * M_PI ) * i / num_objects_ ) );
      cur_obj_y.push_back( path_radius_ * sin( 2.0 * M_PI / period_ * ((double)ros::Time::now().toSec()) + ( 2 * M_PI ) * i / num_objects_ ) );
    }

    for ( int x = -(x_steps/2); x < x_steps / 2; x++ )
    {
      for ( int y = -(y_steps/2); y < y_steps / 2; y++ )
      {
        double x_coor = (double) x * resolution_;
        double y_coor = (double) y * resolution_;
        double z_coor = -10.0;

        for ( int i = 0; i < num_objects_; i++ )
          if( dist( x_coor, y_coor, cur_obj_x[i], cur_obj_y[i] ) <= object_radius_ )
            z_coor = -9.5;

        pcl::PointXYZ tmp;
        tmp.x = x_coor;
        tmp.y = y_coor;
        tmp.z = z_coor;
        output_pcl_cloud.push_back(tmp);
      }
    }

    pcl::toPCLPointCloud2( output_pcl_cloud, *out );

    out->header.frame_id = goal_frame_;

    // increase the time step
    time_step_num_++;

    return;
  }
}
