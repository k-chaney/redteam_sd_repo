#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_cloud_generators/random_point_cloud_plugin.h>
#include <usma_perception_plugins/perception_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::RandPCGen, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void RandPCGen::setShortName( std::string &sn )
  {
    // set the short name to be used
    sn = std::string("random_pc_gen");
  }

  void RandPCGen::processOptions()
  {
    goal_frame_ = getOption<std::string>("frame","map");
    cloud_size_ = getOption<int>("size",1000);
  }

  void RandPCGen::genPC( pcl::PCLPointCloud2::Ptr &out )
  {

    pcl::PointCloud<pcl::PointXYZRGB> output_pcl_cloud;

    for( int i=0; i<cloud_size_; i++ )
    {
        pcl::PointXYZRGB tmp(rand() % 255, rand() % 255, rand() % 255); 
        tmp.x = ((float) (rand() % 100))/100.0;
        tmp.y = ((float) (rand() % 100))/100.0;
        tmp.z = ((float) (rand() % 100))/100.0;

        output_pcl_cloud.push_back(tmp);
    }

    pcl::toPCLPointCloud2( output_pcl_cloud, *out );

    out->header.frame_id = goal_frame_;

    return;
  }
}
