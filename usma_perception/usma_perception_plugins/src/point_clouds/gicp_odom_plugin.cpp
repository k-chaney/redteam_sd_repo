#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/gicp_odom_plugin.h>

#include <math.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::GICPOdomPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void GICPOdomPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
  {
    sn = std::string("gicp");
    // In topic types
    it.push_back( std::string( "pcl::PCLPointCloud2" )); // input cloud
    it.push_back( std::string( "sensor_msgs::Imu" )); // input cloud

    // Out topic types
    ot.push_back( std::string( "nav_msgs::Odometry" )); // Odom
    ot.push_back( std::string( "pcl::PCLPointCloud2" )); // Transformed PointCloud out
  }

  bool GICPOdomPlugin::checkInputReady()
  {
    return ( imu_sub_->dataReady() );
  }

  void GICPOdomPlugin::processOptions()
  {
    odom_frame_ = getOption<std::string>("odom_frame", "odom");
    base_link_frame_ = getOption<std::string>("base_link_frame", "base_link");

    double max_correspondence_dist = getOption("max_correspondence_distance",0.5);
    double ransac_thresh = getOption("RANSAC_outlier_rejection_threshold",0.1);
    int max_iters = getOption("max_iterations", 40);
    double trans_epsilon = getOption("transformation_epsilon",1e-12);
    int euclidean_fit_eps = getOption("euclidean_fitness_epsilon",1);

    icp_.setMaxCorrespondenceDistance(max_correspondence_dist);
    icp_.setRANSACOutlierRejectionThreshold(ransac_thresh);
    icp_.setMaximumIterations(max_iters);
    icp_.setTransformationEpsilon(trans_epsilon);
    icp_.setEuclideanFitnessEpsilon(euclidean_fit_eps);

    publish_tf_ = getOption<bool>("publish_tf", false);
  }

  void GICPOdomPlugin::init()
  {
    processOptions();

    in_new_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >(subs_[0]);
    imu_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > >(subs_[1]);
    odom_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
    pc_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[1]);

    tf_broadcaster_.reset( new tf::TransformBroadcaster() );

    odom_tf_lock_.lock();
    tf_pub_thread_.reset(new boost::thread( &GICPOdomPlugin::tfPublishThread, this ));
  }

  void GICPOdomPlugin::workLoad()
  {
    sensor_msgs::Imu::ConstPtr new_imu_reading = imu_sub_->getData();
    if( !in_new_cloud_sub_->dataReady() )
      return;

    // load a new input cloud if one is ready
    pcl::PCLPointCloud2::ConstPtr new_input_cloud = in_new_cloud_sub_->getData();
    boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> >  new_cloud;
    new_cloud.reset( new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::fromPCLPointCloud2(*(new_input_cloud), *(new_cloud));

    if( !ref_cloud_ )
    {
      ROS_INFO("Setting up reference frame");

      odom_tf_lock_.unlock();
      odom_tf_ = Eigen::Matrix4f::Identity();

      ref_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,new_imu_reading->orientation.x,
          new_imu_reading->orientation.y,new_imu_reading->orientation.z);

      ref_cloud_ = new_cloud;

      return;
    }

    Eigen::Quaternion<float> new_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,
        new_imu_reading->orientation.x,
        new_imu_reading->orientation.y,
        new_imu_reading->orientation.z);

    Eigen::Matrix4f cur_odom_to_base;
    cur_odom_to_base = Eigen::Matrix4f::Identity();
    cur_odom_to_base.block<3,3>(0,0) = (ref_cloud_orientation_.inverse() * new_cloud_orientation_).toRotationMatrix();

    icp_.setInputSource( new_cloud );
    icp_.setInputTarget( ref_cloud_ );
    pcl::PointCloud<pcl::PointXYZ> aligned_;
    icp_.align( aligned_, cur_odom_to_base );
    cur_odom_to_base = icp_.getFinalTransformation();

    odom_tf_lock_.lock();
    odom_tf_ = odom_tf_*cur_odom_to_base;

    for(int i=0; i<3; i++)
    {
      for(int j=0; j<3; j++)
      {
        if(fabs(fabs(odom_tf_(i,j))-fabs(odom_tf_(j,i)))>std::numeric_limits<float>::epsilon())
        {
          float mean = (fabs(odom_tf_(i,j)) + fabs(odom_tf_(j,i)))/2.0;
          odom_tf_(i,j) = mean * (boost::math::signbit(odom_tf_(i,j))?-1.0:1.0);
          odom_tf_(j,i) = mean * (boost::math::signbit(odom_tf_(j,i))?-1.0:1.0);
        }
      }
    }

    cur_odom_.header.seq = new_input_cloud->header.seq;
    cur_odom_.header = pcl_conversions::fromPCL(new_input_cloud->header);

    cur_odom_.child_frame_id = new_input_cloud->header.frame_id;
    cur_odom_.header.frame_id = odom_frame_;

    tf::Transform ctf = getTFfromEigen(odom_tf_);
    tf::poseTFToMsg( ctf, cur_odom_.pose.pose );

    odom_tf_lock_.unlock();

    if ( odom_pub_->getNumSubscribers() )
    {
      odom_pub_->publish( cur_odom_ );
    }

    // setup the ref cloud and reset the guess transform
    ref_cloud_ = new_cloud;
    ref_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,new_imu_reading->orientation.x,
        new_imu_reading->orientation.y,new_imu_reading->orientation.z);

    if ( pc_pub_->getNumSubscribers() )
    {
      pcl::PointCloud<pcl::PointXYZ> pcl_co;
      pcl::PCLPointCloud2 ros_co;

      pcl_ros::transformPointCloud(*new_cloud, pcl_co, ctf);
      pcl::toPCLPointCloud2(pcl_co, ros_co);
      ros_co.header.frame_id = odom_frame_;
      pc_pub_->publish(ros_co);
    }

    cur_odom_to_base = cur_odom_to_base.Identity( 4, 4 );
  }

  bool GICPOdomPlugin::refineTransform(boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > new_cloud, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > ref_cloud, Eigen::Matrix4f &refined_tf)
  {
    return true;
  }

  void GICPOdomPlugin::tfPublishThread()
  {
    ros::Rate r(50.0);
    while( ros::ok() )
    {
      r.sleep();
      if( publish_tf_ )
      {
        odom_tf_lock_.lock();
        tf::Transform ctf = getTFfromEigen(odom_tf_);
        tf::StampedTransform time_corrected_stamped_odom( ctf,cur_odom_.header.stamp,odom_frame_+std::string("_tc"),base_link_frame_+std::string("_tc") );
        tf_broadcaster_->sendTransform( time_corrected_stamped_odom );
        tf::StampedTransform stamped_odom( ctf,ros::Time::now(),odom_frame_,base_link_frame_ );
        tf_broadcaster_->sendTransform( stamped_odom );
        odom_tf_lock_.unlock();
      }
    }
  }
}
