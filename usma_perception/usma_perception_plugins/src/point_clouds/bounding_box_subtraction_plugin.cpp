#include <pluginlib/class_list_macros.h>
#include <usma_perception_plugins/point_clouds/bounding_box_subtraction_plugin.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::BoundingBoxSubtractionPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void BoundingBoxSubtractionPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
  {
    sn = std::string("box_subtraction");
    it.push_back( std::string( "pcl::PCLPointCloud2" )); // input cloud -- should maintain from a constant source and frame
    it.push_back( std::string( "usma_perception_msgs::TrackedBoundingBoxArray" )); // Bounding box array

    ot.push_back( std::string( "pcl::PCLPointCloud2" )); 
  }

  bool BoundingBoxSubtractionPlugin::checkInputReady()
  {
    return ( in_cloud_sub_->dataReady() || in_box_sub_->dataReady() );
  }

  void BoundingBoxSubtractionPlugin::processOptions()
  {
    min_velocity_ = getOption<double>("min_velocity", 0.05);
  }

  void BoundingBoxSubtractionPlugin::init()
  {
    processOptions();
    // topics and services initialization
    in_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >(subs_[0]);
    in_box_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::TrackedBoundingBoxArray::ConstPtr> > >(subs_[1]);
    out_cloud_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
  }

  void BoundingBoxSubtractionPlugin::workLoad()
  {
   if ( in_box_sub_->dataReady() )
   {
     box_pairs_.clear();
     usma_perception_msgs::TrackedBoundingBoxArray::ConstPtr ba = in_box_sub_->getData();

     for( int i = 0; i < ba->boxes.size(); i++ )
     {
       if( dist( 0.0, 0.0, 0.0, ba->boxes[i].twist.linear.x, ba->boxes[i].twist.linear.y, 0.0 ) > min_velocity_ ) //ba->boxes[i].twist.linear.z ) > min_velocity_ ) // figure out why this hack works
       {
         pcl::PointXYZ min, max;
         min.x = ba->boxes[i].min.x + ba->boxes[i].pose.position.x;
         min.y = ba->boxes[i].min.y + ba->boxes[i].pose.position.y;
         min.z = ba->boxes[i].min.z + ba->boxes[i].pose.position.z;
         max.x = ba->boxes[i].max.x + ba->boxes[i].pose.position.x;
         max.y = ba->boxes[i].max.y + ba->boxes[i].pose.position.y;
         max.z = ba->boxes[i].max.z + ba->boxes[i].pose.position.z;
         box_pairs_.push_back( std::pair< pcl::PointXYZ, pcl::PointXYZ >( min, max ) );
       }
     }
   }

   // if we have tracked boxes -- do the processing
   if ( in_cloud_sub_->dataReady() && box_pairs_.size() > 0 )
   {
     pcl::PCLPointCloud2::ConstPtr in = in_cloud_sub_->getData();
     pcl::PCLPointCloud2::Ptr out;
     out.reset( new pcl::PCLPointCloud2 );
     pcl::PointCloud<pcl::PointXYZ> cloud_in;
     pcl::fromPCLPointCloud2(*(in), cloud_in);

     int num_points = cloud_in.size();
     int point_step = in->point_step;
     int num_kept = 0;

     for( int i = 0; i < num_points; i++ )
     {
       if ( !isPointInBoxes( cloud_in.at(i) ) )
       {
         num_kept++;
         out->data.insert( out->data.end(), in->data.begin()+(i*point_step), in->data.begin()+((i+1)*point_step) );
       }
     }

     // places the necessary metadata into the message
     out->height = 1;
     out->width = num_kept;
     out->header = in->header;
     out->fields = in->fields;
     out->point_step = in->point_step;
     out->row_step = num_kept*in->point_step;

     out_cloud_pub_->publish( out );

   }
   else if ( in_cloud_sub_->dataReady() ) // if we don't have any tracked boxes -- this becomes a pass through
   {
     out_cloud_pub_->publish( in_cloud_sub_->getData() );
   }

  }

  inline bool BoundingBoxSubtractionPlugin::isPointInBoxes( pcl::PointXYZ &p )
  {
    bool in_box_ = false;
    int n = box_pairs_.size();
    for( int i = 0; i < n; i++ )
    {
      if ( box_pairs_[i].first.x <= p.x && box_pairs_[i].second.x >= p.x &&
           box_pairs_[i].first.y <= p.y && box_pairs_[i].second.y >= p.y &&
           box_pairs_[i].first.z <= p.z && box_pairs_[i].second.z >= p.z )
      {
        in_box_ = true;
        break;
      }
    }
    return in_box_;
  }
  inline double BoundingBoxSubtractionPlugin::dist( double x1, double y1, double z1, double x2, double y2, double z2 )
  {
    return sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
  }
}
