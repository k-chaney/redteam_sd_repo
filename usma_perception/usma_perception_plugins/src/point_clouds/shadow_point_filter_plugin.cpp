#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/shadow_point_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ShadowPointPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ShadowPointPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string ShadowPointPlugin::setShortName()
  {
    return std::string( "shadow_point" );
  }

  bool ShadowPointPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void ShadowPointPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    thresh_ = getOption("threshold",1.0);
    search_rad_ = getOption("search_radius",0.1);
  }

  void ShadowPointPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_in;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_out;
    cloud_in.reset ( new pcl::PointCloud<pcl::PointXYZ>() );
    cloud_out.reset( new pcl::PointCloud<pcl::PointXYZ>() );
    pcl::fromPCLPointCloud2(*(in[0]), *cloud_in);

    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> ne;
    ne.setInputCloud (cloud_in);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ> ());
    ne.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch (search_rad_);

    // Compute the features
    ne.compute (*cloud_normals);
   
    pcl::ShadowPoints<pcl::PointXYZ, pcl::Normal> spf;
    spf.setThreshold( thresh_ );
    spf.setInputCloud( cloud_in );
    spf.setNormals( cloud_normals );
    spf.filter( *cloud_out );

    pcl::toPCLPointCloud2(*cloud_out, *(out[0]));
    should_publish[0] = true;

//    if ( in[0]->fields.size() == 4 && in[0]->fields[3].name == "intensity")
//    {
//      pcl::PointCloud<pcl::PointXYZI> cloud_in;
//      pcl::PointCloud<pcl::PointXYZI> cloud_out;
//      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
//
//      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
//      should_publish[0] = true;
//    }
//    else if( in[0]->fields.size() == 4 && in[0]->fields[3].name == "rgb" )
//    {
//      pcl::PointCloud<pcl::PointXYZRGB> cloud_in;
//      pcl::PointCloud<pcl::PointXYZRGB> cloud_out;
//      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
//
//      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
//      should_publish[0] = true;
//    } else if ( in[0]->fields.size() == 3 ) {
//      pcl::PointCloud<pcl::PointXYZ> cloud_in;
//      pcl::PointCloud<pcl::PointXYZ> cloud_out;
//      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
//
//      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
//      should_publish[0] = true;
//    }

      out[0]->header = in[0]->header;

    return;
  }
 }
