#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/freq_divider_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::FreqDividerPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void FreqDividerPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string FreqDividerPlugin::setShortName()
  {
    return std::string( "freq_divider" );
  }

  bool FreqDividerPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void FreqDividerPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    freq_divider_ = getOption<int>("freq_divider",1);
  }

  void FreqDividerPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    if ( count_ % freq_divider_ == 0 )
    {
      *(out[0]) = *(in[0]);
      should_publish[0] = true;
    }
    count_++;

    return;
  }
 }
