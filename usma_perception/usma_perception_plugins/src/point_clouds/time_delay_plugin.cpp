#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/time_delay_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::TimeDelayPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void TimeDelayPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string TimeDelayPlugin::setShortName()
  {
    return std::string( "time_delay" );
  }

  bool TimeDelayPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void TimeDelayPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    count_delay_ = getOption<int>("count_delay",1);
  }

  void TimeDelayPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    if ( delay_vector_.size() == count_delay_ )
    {
      *(out[0]) = *( delay_vector_.front() );
      delay_vector_.pop();
      should_publish[0] = true;
    }
    delay_vector_.push(in[0]);

    return;
  }
 }
