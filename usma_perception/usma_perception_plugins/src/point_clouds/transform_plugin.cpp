#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/transform_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::TransformPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void TransformPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string TransformPlugin::setShortName()
  {
    return std::string( "transformer" );
  }

  bool TransformPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void TransformPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    goal_frame_ = getOption("goal_frame",std::string("base_link"));
    sensor_frame_ = getOption("sensor_frame",std::string(""));
    tf_fudge_factor_ = getOption("tf_fudge_factor",0.1);
  }

  void TransformPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    if ( sensor_frame_ == "" )
      sensor_frame_ = std::string(in[0]->header.frame_id);

    ros::Time tt = ros::Time( pcl_conversions::fromPCL( in[0]->header ).stamp ) - ros::Duration(tf_fudge_factor_);

    tf::StampedTransform old_to_new;
    std::string er_msg;
    if ( tf_listener_.canTransform(goal_frame_,sensor_frame_,tt,&er_msg) && goal_frame_ !=  sensor_frame_)
      tf_listener_.lookupTransform(goal_frame_,sensor_frame_,tt, old_to_new);
    else
    {
      ROS_ERROR("[%s]::Can not complete transform::%s", getShortName().c_str(), er_msg.c_str());
      return;
    }

    // no need to transform if it's the same frame
    if (goal_frame_ == std::string(in[0]->header.frame_id) )
    {
      *(out[0]) = *(in[0]);
      should_publish[0] = true;
    }
    else if ( ( in[0]->fields.size() == 5 || in[0]->fields.size() == 4 ) && in[0]->fields[3].name == "intensity")
    {
      pcl::PointCloud<pcl::PointXYZI> cloud_in;
      pcl::PointCloud<pcl::PointXYZI> cloud_out;
      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
      pcl_ros::transformPointCloud(cloud_in, cloud_out, static_cast<tf::Transform>(old_to_new));
      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
      should_publish[0] = true;
    }
    else if( in[0]->fields.size() == 4 && in[0]->fields[3].name == "rgb" )
    {
      pcl::PointCloud<pcl::PointXYZRGB> cloud_in;
      pcl::PointCloud<pcl::PointXYZRGB> cloud_out;
      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
      pcl_ros::transformPointCloud(cloud_in, cloud_out, static_cast<tf::Transform>(old_to_new));
      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
      should_publish[0] = true;
    } else if ( in[0]->fields.size() == 3 ) {
      pcl::PointCloud<pcl::PointXYZ> cloud_in;
      pcl::PointCloud<pcl::PointXYZ> cloud_out;
      pcl::fromPCLPointCloud2(*(in[0]), cloud_in);
      pcl_ros::transformPointCloud(cloud_in, cloud_out, static_cast<tf::Transform>(old_to_new));
      pcl::toPCLPointCloud2(cloud_out, *(out[0]));
      should_publish[0] = true;
    }

    out[0]->header = in[0]->header;
    out[0]->header.frame_id = goal_frame_;

    return;
  }
 }
