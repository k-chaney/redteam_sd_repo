#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/statistical_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::StatisticalOutlierFilterPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void StatisticalOutlierFilterPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string StatisticalOutlierFilterPlugin::setShortName()
  {
    return std::string( "outlier_remover" );
  }

  bool StatisticalOutlierFilterPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void StatisticalOutlierFilterPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    std_dev_ = getOption<double>("std_dev_thresh",0.1);
    mean_k_ = getOption<int>("mean_k",20);

    sor_.setMeanK( mean_k_ );
    sor_.setStddevMulThresh( std_dev_ );
  }

  void StatisticalOutlierFilterPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    sor_.setInputCloud( in[0] );
    sor_.filter( *(out[0]) );
    should_publish[0] = true;

    return;
  }
 }
