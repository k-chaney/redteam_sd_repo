#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::RadiusOutlierFilterPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void RadiusOutlierFilterPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string RadiusOutlierFilterPlugin::setShortName()
  {
    return std::string( "radius_outlier_filter" );
  }

  bool RadiusOutlierFilterPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void RadiusOutlierFilterPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    search_radius_ = getOption<double>("search_radius",0.1);
    min_pts_ = getOption<int>("min_pts",10);

    ror_.setRadiusSearch( search_radius_ );
    ror_.setMinNeighborsInRadius( min_pts_ );
  }

  void RadiusOutlierFilterPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    ror_.setInputCloud( in[0] );
    ror_.filter( *(out[0]) );
    should_publish[0] = true;

    return;
  }
 }
