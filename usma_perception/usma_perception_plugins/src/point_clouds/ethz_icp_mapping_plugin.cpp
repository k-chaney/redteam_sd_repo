#include <usma_perception_plugins/point_clouds/ethz_icp_mapping_plugin.h>

namespace usma_perception
{
  void ethzMappingPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
  {
    sn = std::string("ethz_icp_map");
    // In topic types
    it.push_back( std::string( "sensor_msgs::PointCloud2" )); // input cloud
    it.push_back( std::string( "sensor_msgs::PointCloud2" )); // input map
    it.push_back( std::string( "nav_msgs::Odometry" )); // Odom -> input est

    // Out topic types
    ot.push_back( std::string( "sensor_msgs::PointCloud2" )); // Map
    ot.push_back( std::string( "nav_msgs::Odometry" )); // Odom
    ot.push_back( std::string( "nav_msgs::Odometry" )); // OdomError
  }

  bool ethzMappingPlugin::checkInputReady()
  {
    return in_cloud_sub_->dataReady() || in_map_sub_->dataReady();
  }

  void ethzMappingPlugin::processOptions()
  {
    localizing = getOption<bool>("localizing", true);
    mapping = getOption<bool>("mapping", true);
    min_reading_point_count_ = getOption<int>("min_reading_point_count", 500);
    min_map_point_count_ = getOption<int>("min_map_point_count", 500);
    min_overlap_ = getOption<double>("min_overlap", 0.3);
    max_overlap_to_merge_ = getOption<double>("max_overlap_to_merge", 0.99);
    odom_frame_ = getOption<std::string>("odom_frame", "odom");
    map_frame_ = getOption<std::string>("map_frame", "map");

    // Ensure proper states
    if(localizing == false)
      mapping = false;
    if(mapping == true)
      localizing = true;
  }

  void ethzMappingPlugin::init()
  {
    ros::NodeHandle n;
    ros::NodeHandle pn;

    transformation.reset( PM::get().REG(Transformation).create("RigidTransformation") );
    TOdomToMap = PM::TransformationParameters::Identity(4, 4);
    tf_listener_.reset( new tf::TransformListener(ros::Duration(30)) );

    processOptions();

    std::string configFileName = getOption<std::string>("config_file_name", ""); // only loaded at the beginning

    if ( configFileName != "" )
    {
      std::ifstream ifs(configFileName.c_str());

      if (ifs.good())
      {
        icp.loadFromYaml(ifs);
      }
      else
      {
        ROS_ERROR("Bad config file given, setting defaults");
        icp.setDefault();
      }
    }
    else
    {
      ROS_ERROR("No config file given, setting defaults");
      icp.setDefault();
    }

    in_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > >(subs_[0]);
    in_map_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > >(subs_[1]);
    odom_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<nav_msgs::Odometry::ConstPtr> > >(subs_[2]);

    map_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
    odom_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[1]);
    odom_error_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[2]);

    map_tf_.setIdentity();
    tf_broadcaster_.reset(new tf::TransformBroadcaster());
    tf_pub_thread_.reset(new boost::thread( &ethzMappingPlugin::tfPublishThread, this ));
  }

  void ethzMappingPlugin::workLoad()
  {
    // load a new map if there is one ready
    if(in_map_sub_->dataReady())
    {
      sensor_msgs::PointCloud2::ConstPtr c_in_ = in_map_sub_->getData();
      boost::shared_ptr<DP> cloud(new DP( PointMatcher_ros::rosMsgToPointMatcherCloud<float>(*(c_in_)) ));

      const int dim = cloud->features.rows();
      const int nbPts = cloud->features.cols();

      setMap(cloud);
    }

    // load a new input cloud if one is ready
    if(localizing && in_cloud_sub_->dataReady())
    {
      sensor_msgs::PointCloud2::ConstPtr c_in_ = in_cloud_sub_->getData();
      boost::shared_ptr<DP> cloud(new DP(PointMatcher_ros::rosMsgToPointMatcherCloud<float>(*(c_in_))));

      processCloud(cloud, c_in_->header.frame_id, c_in_->header.stamp, c_in_->header.seq);
    }
  }

  void ethzMappingPlugin::processCloud(boost::shared_ptr<DP> newPointCloud, const std::string& scannerFrame, const ros::Time& stamp, uint32_t seq)
  {
    // IMPORTANT:  We need to receive the point clouds in local coordinates (scanner or robot)
    timer t;

    // Convert point cloud
    const size_t goodCount(newPointCloud->features.cols());
    if (goodCount == 0)
    {
      ROS_ERROR("[%s]::I found no good points in the cloud", getShortName().c_str());
      return;
    }

    // Dimension of the point cloud, important since we handle 2D and 3D
    const int dimp1(newPointCloud->features.rows());

    ROS_INFO("[%s]::Processing new point cloud", getShortName().c_str());
    {
      // Apply filters to incoming cloud, in scanner coordinates
      inputFilters.apply(*newPointCloud);
    }

    // Ensure a minimum amount of point after filtering
    const int ptsCount = newPointCloud->features.cols();
    if(ptsCount < min_reading_point_count_)
    {
      ROS_ERROR_STREAM("[" << getShortName().c_str() << "]::Not enough points in newPointCloud: only " << ptsCount << " pts.");
      return;
    }

    // Initialize the map if empty
    if(!icp.hasMap())
    {
      ROS_INFO_STREAM("[" << getShortName().c_str() << "]::Creating an initial map");

      TOdomToMap = PM::TransformationParameters::Identity(dimp1, dimp1);
      setMap(updateMap(newPointCloud, TOdomToMap, false));
      map_pub_->publish(PointMatcher_ros::pointMatcherCloudToRosMsg<float>(*mapPointCloud, map_frame_, ros::Time::now()));

      return;
    }

    ROS_INFO_STREAM("[" << getShortName().c_str() << "]::TOdomToMap (est) \n" << TOdomToMap);

    // Check dimension
    if (newPointCloud->features.rows() != icp.getInternalMap().features.rows())
    {
      ROS_ERROR_STREAM("[" << getShortName().c_str() << "]::Dimensionality missmatch: incoming cloud is " << newPointCloud->features.rows()-1 << " while map is " << icp.getInternalMap().features.rows()-1);
      return;
    }

    try
    {
      // Apply ICP
      PM::TransformationParameters Ticp;

      Ticp = icp(*newPointCloud, TOdomToMap);

      ROS_INFO_STREAM("[" << getShortName().c_str() << "]::Ticp (refined):\n" << Ticp);

      // Ensure minimum overlap between scans
      const double estimatedOverlap = icp.errorMinimizer->getOverlap();
      ROS_INFO_STREAM("[" << getShortName().c_str() << "]::Overlap: " << estimatedOverlap);
      if (estimatedOverlap < min_overlap_)
      {
        ROS_ERROR_STREAM("[" << getShortName().c_str() << "]::Estimated overlap too small, ignoring ICP correction!");
        return;
      }

      // Compute tf
      TOdomToMap = Ticp;

      ROS_INFO_STREAM("TOdomToMap:\n" << TOdomToMap);
      map_tf_lock_.lock();
      map_tf_ = PointMatcher_ros::eigenMatrixToTransform<float>(TOdomToMap);
      map_time_ = stamp;
      map_tf_lock_.unlock();

      // Publish odometry
      if (odom_pub_->getNumSubscribers())
        odom_pub_->publish(PointMatcher_ros::eigenMatrixToOdomMsg<float>(Ticp, map_frame_, stamp));

      // Publish error on odometry
      if (odom_error_pub_->getNumSubscribers())
        odom_error_pub_->publish(PointMatcher_ros::eigenMatrixToOdomMsg<float>(TOdomToMap, map_frame_, stamp));

      // check if news points should be added to the map
      if (mapping)
      {
        setMap(updateMap( newPointCloud, Ticp, true));

        // Publish map point cloud
        if( map_pub_->getNumSubscribers() )
          map_pub_->publish(PointMatcher_ros::pointMatcherCloudToRosMsg<float>(*mapPointCloud, map_frame_, ros::Time::now()));
      }
    }
    catch (PM::ConvergenceError error)
    {
      ROS_ERROR_STREAM("[" << getShortName().c_str() << "]::ICP failed to converge: " << error.what());
      return;
    }

    ROS_INFO_STREAM("[" << getShortName().c_str() << "]::[TIME] Total ICP took: " << t.elapsed() << " [s]");
  }

  void ethzMappingPlugin::setMap(boost::shared_ptr<DP> newPointCloud)
  {
    // delete old map
    if (mapPointCloud)
      mapPointCloud.reset();

    // set new map
    mapPointCloud = newPointCloud;
    icp.setMap(*mapPointCloud);

  }

  boost::shared_ptr<ethzMappingPlugin::DP> ethzMappingPlugin::updateMap(boost::shared_ptr<DP> newPointCloud, const PM::TransformationParameters Ticp, bool updateExisting)
  {
    timer t;

    // Correct new points using ICP result
    *newPointCloud = transformation->compute(*newPointCloud, Ticp);

    // Merge point clouds to map
    if (updateExisting)
      newPointCloud->concatenate(*mapPointCloud);

    //    ROS_INFO_STREAM("[" << getShortName().c_str() << "]::[TIME] New map available (" << newPointCloud->features.cols() << " pts), update took " << t.elapsed() << " [s]");

    return newPointCloud;
  }

  void ethzMappingPlugin::tfPublishThread()
  {
    ros::Rate r(50.0);
    while( ros::ok() )
    {
      r.sleep();
      map_tf_lock_.lock();
      tf::StampedTransform tc_stamped_world(map_tf_,map_time_,map_frame_+std::string("_tc"),odom_frame_+std::string("_tc"));
      tf_broadcaster_->sendTransform( tc_stamped_world );

      tf::StampedTransform stamped_world(map_tf_,ros::Time::now(),map_frame_,odom_frame_);
      tf_broadcaster_->sendTransform( stamped_world );
      map_tf_lock_.unlock();
    }
  }
}
