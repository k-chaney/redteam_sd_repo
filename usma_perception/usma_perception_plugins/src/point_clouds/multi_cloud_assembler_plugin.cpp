#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/multi_cloud_assembler_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::MCAssemblerPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void MCAssemblerPlugin::getNumInOut( int &in, int &out )
  {
    in = getOption<int>("num_in",3);
    out = 1;
  }

  std::string MCAssemblerPlugin::setShortName()
  {
    return std::string( "mc_assembler" );
  }

  bool MCAssemblerPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical and statement (if anything is false, return false)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == false )
        return false;
     return true;
  }

  void MCAssemblerPlugin::processOptions()
  {
  }

  void MCAssemblerPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    pcl::PointCloud<pcl::PointXYZ> out_pcl;
    std::vector<pcl::PointCloud<pcl::PointXYZ> > in_pcl;
    in_pcl.resize( in.size() );

    for( int i = 0; i < in.size(); i++ )
    {
      pcl::fromPCLPointCloud2( *(in[i]), in_pcl[i] );
      out_pcl += in_pcl[i];
    }

    pcl::toPCLPointCloud2(out_pcl,*(out[0]));

    out[0]->header = in[0]->header;

    should_publish[0] = true;

    return;
  }
  
 }
