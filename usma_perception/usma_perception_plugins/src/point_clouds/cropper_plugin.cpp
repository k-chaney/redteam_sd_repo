#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::CropperPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void CropperPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string CropperPlugin::setShortName()
  {
    return std::string( "cropper" );
  }

  bool CropperPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void CropperPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    min_pt_[0] = getOption("x_min",-10.0);
    min_pt_[1] = getOption("y_min",-10.0);
    min_pt_[2] = getOption("z_min",-10.0);
    max_pt_[0] = getOption("x_max",10.0);
    max_pt_[1] = getOption("y_max",10.0);
    max_pt_[2] = getOption("z_max",10.0);

  }

  void CropperPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    crop_box_.setInputCloud( in[0] );
    crop_box_.setMin(min_pt_);
    crop_box_.setMax(max_pt_);
    crop_box_.filter( *(out[0]) );
    out[0]->header = in[0]->header;
    should_publish[0] = true;

    return;
  }
 }
