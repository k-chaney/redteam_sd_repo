#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/ethz_icp_odom_plugin.h>

#include <math.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::ethzOdomPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void ethzOdomPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
  {
    sn = std::string("ethz_icp");
    // In topic types
    it.push_back( std::string( "sensor_msgs::PointCloud2" )); // input cloud
    it.push_back( std::string( "sensor_msgs::Imu" )); // input cloud

    // Out topic types
    ot.push_back( std::string( "nav_msgs::Odometry" )); // Odom
    ot.push_back( std::string( "sensor_msgs::PointCloud2" )); // Transformed PointCloud out
  }

  bool ethzOdomPlugin::checkInputReady()
  {
    return ( imu_sub_->dataReady() );
  }

  void ethzOdomPlugin::processOptions()
  {
    odom_frame_ = getOption<std::string>("odom_frame", "odom");
    sensor_frame_ = getOption<std::string>("sensor_frame", "");
    base_link_frame_ = getOption<std::string>("base_link_frame", "base_link");
    min_new_good_ratio_ = getOption<double>("min_good_ratio", 0.5);
  }

  void ethzOdomPlugin::init()
  {
    ros::NodeHandle n;
    ros::NodeHandle pn;


    processOptions();

    std::string config_file_name = getOption<std::string>("config_file_name", ""); // only loaded at the beginning

    PointMatcherSupport::setLogger(new PointMatcherSupport::ROSLogger);

    if ( config_file_name != "" )
    {
      std::ifstream ifs(config_file_name.c_str());

      if (ifs.good())
      {
        icp.loadFromYaml(ifs);
      }
      else
      {
        ROS_ERROR("Bad config file given, setting defaults");
        icp.setDefault();
      }
    }
    else
    {
      ROS_ERROR("No config file given, setting defaults");
      icp.setDefault();
    }

    in_new_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > >(subs_[0]);
    imu_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > >(subs_[1]);
    odom_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
    pc_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[1]);


    tf_broadcaster_.reset( new tf::TransformBroadcaster() );
    tf_listener_.reset( new tf::TransformListener( ros::Duration(30.0) ) );
    tf_listener_->waitForTransform( base_link_frame_, sensor_frame_, ros::Time(0), ros::Duration(5.0) );

    std::string er_msg;
    if( tf_listener_->canTransform( base_link_frame_, sensor_frame_, ros::Time(0), &er_msg ) )
    {
      tf_listener_->lookupTransform( base_link_frame_, sensor_frame_, ros::Time(0), base_to_sensor_ );
    }
    else
    {
      ROS_ERROR("No transform base to sensor -- assuming identity -- %s", er_msg.c_str());
      base_to_sensor_.setIdentity();
    }

    ref_transformer_.reset( PM::get().REG(Transformation).create("RigidTransformation") );

    odom_tf_lock_.lock();
    tf_pub_thread_.reset(new boost::thread( &ethzOdomPlugin::tfPublishThread, this ));
  }

  void ethzOdomPlugin::workLoad()
  {
    sensor_msgs::Imu::ConstPtr new_imu_reading = imu_sub_->getData();
    if( !in_new_cloud_sub_->dataReady() )
      return;

    // load a new input cloud if one is ready
    sensor_msgs::PointCloud2::ConstPtr new_input_cloud = in_new_cloud_sub_->getData();

    boost::shared_ptr<DP> new_cloud(new DP(PointMatcher_ros::rosMsgToPointMatcherCloud<float>(*(new_input_cloud))));
    unsigned int new_point_count( new_input_cloud->width * new_input_cloud->height );
    unsigned int new_good_count( new_cloud->features.cols() );
    double new_ratio = double(new_good_count)/double(new_point_count);

    if( new_good_count == 0 || new_point_count == 0 )
    {
      ROS_ERROR("No good points in the new cloud.");
      return;
    }
    if( new_ratio < min_new_good_ratio_ )
    {
      ROS_ERROR("Too few good points in new cloud--%f", new_ratio);
      return;
    }

    if( !ref_cloud_ )
    {
      ROS_INFO("Setting up reference frame");

      odom_tf_lock_.unlock();
      odom_tf_ = odom_tf_.Identity( new_cloud->features.rows(), new_cloud->features.rows() );

      ref_cloud_ = new_cloud;
      ref_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,new_imu_reading->orientation.x,
          new_imu_reading->orientation.y,new_imu_reading->orientation.z);
      return;
    }

    Eigen::Quaternion<float> new_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,
        new_imu_reading->orientation.x,
        new_imu_reading->orientation.y,
        new_imu_reading->orientation.z);

    PM::TransformationParameters cur_odom_to_base;
    cur_odom_to_base = cur_odom_to_base.Identity( new_cloud->features.rows(), new_cloud->features.rows() );
    cur_odom_to_base.block<3,3>(0,0) = (ref_cloud_orientation_.inverse() * new_cloud_orientation_).toRotationMatrix();
    if( !refineTransform( new_cloud, ref_cloud_, cur_odom_to_base ) )
      return;


    odom_tf_lock_.lock();
    odom_tf_ = odom_tf_*cur_odom_to_base;

    for(int i=0; i<new_cloud->features.rows()-1; i++)
    {
      for(int j=0; j<new_cloud->features.rows()-1; j++)
      {
        if(fabs(fabs(odom_tf_(i,j))-fabs(odom_tf_(j,i)))>std::numeric_limits<float>::epsilon())
        {
          float mean = (fabs(odom_tf_(i,j)) + fabs(odom_tf_(j,i)))/2.0;
          odom_tf_(i,j) = mean * (boost::math::signbit(odom_tf_(i,j))?-1.0:1.0);
          odom_tf_(j,i) = mean * (boost::math::signbit(odom_tf_(j,i))?-1.0:1.0);
        }
      }
    }

    cur_odom_.header = new_input_cloud->header;
    cur_odom_.child_frame_id = odom_frame_;
    tf::poseTFToMsg( PointMatcher_ros::eigenMatrixToTransform<float>(odom_tf_), cur_odom_.pose.pose );

    if ( odom_pub_->getNumSubscribers() )
    {
      odom_pub_->publish( cur_odom_ );
    }
    odom_tf_lock_.unlock();

    // setup the ref cloud and reset the guess transform
    ref_cloud_ = new_cloud;
    ref_cloud_orientation_ = Eigen::Quaternion<float>(new_imu_reading->orientation.w,new_imu_reading->orientation.x,
        new_imu_reading->orientation.y,new_imu_reading->orientation.z);

    if ( pc_pub_->getNumSubscribers() )
    {
      // need to santize this input!!!!
      ROS_INFO_STREAM( "[odom]::aggregate transform\n" << odom_tf_ );
      DP tmp_cloud = ref_transformer_->compute( *new_cloud, odom_tf_ );
      pc_pub_->publish(PointMatcher_ros::pointMatcherCloudToRosMsg<float>(tmp_cloud, odom_frame_, ros::Time::now()));
    }

    cur_odom_to_base = cur_odom_to_base.Identity( new_cloud->features.rows(), new_cloud->features.rows() );
  }

  bool ethzOdomPlugin::refineTransform(boost::shared_ptr<DP> new_cloud, boost::shared_ptr<DP> ref_cloud, PM::TransformationParameters &refined_tf)
  {
    try
    {
      refined_tf = icp( *new_cloud, *ref_cloud, refined_tf );
    }
    catch (PM::ConvergenceError error)
    {
      ROS_ERROR("ICP failed to converge: %s", error.what());
      return false;
    }
    return true;
  }

  void ethzOdomPlugin::tfPublishThread()
  {
    ros::Rate r(50.0);
    while( ros::ok() )
    {
      r.sleep();
      odom_tf_lock_.lock();
      tf::StampedTransform tc_stamped_odom( PointMatcher_ros::eigenMatrixToTransform<float>(odom_tf_) ,ros::Time(cur_odom_.header.stamp),odom_frame_+std::string("_tc"),base_link_frame_);
      tf_broadcaster_->sendTransform( tc_stamped_odom );
      tf::StampedTransform stamped_odom( PointMatcher_ros::eigenMatrixToTransform<float>(odom_tf_) ,ros::Time::now(),odom_frame_,base_link_frame_);
      tf_broadcaster_->sendTransform( stamped_odom );
      odom_tf_lock_.unlock();
    }
  }
}
