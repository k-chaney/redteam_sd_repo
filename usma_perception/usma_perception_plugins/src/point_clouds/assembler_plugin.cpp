#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/assembler_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::AssemblerPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void AssemblerPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string AssemblerPlugin::setShortName()
  {
    return std::string( "assembler" );
  }

  bool AssemblerPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void AssemblerPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    rolling_frame_ = getOption("rolling_frame",true);
    num_frames_ = getOption("num_frames",10);
  }

  void AssemblerPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {

    frame_buffer_.push_front( in[0] );

    while( frame_buffer_.size() > num_frames_ )
    {
      frame_buffer_.pop_back();
    }

    if( frame_buffer_.size() == num_frames_ )
    {
      out[0]->fields = in[0]->fields;
      // adds in each piece of the frame buffer and ensures that all of the fields match up
      for( int i=0; i<frame_buffer_.size(); i++ )
      {
        // puts the data from the frame buffer into the data in the out cloud
        if( out[0]->fields.size() != frame_buffer_[i]->fields.size() )
        {
          ROS_ERROR("[assembler]::input field sizes don't match");
          return;
        }
        for( int j=0; j<out[0]->fields.size(); j++ )
        {
          if( out[0]->fields[j].name != frame_buffer_[i]->fields[j].name )
          {
            ROS_ERROR("[assembler]::input fields don't match");
            return;
          }
        }
        // inserts the next frame in the buffer into the out cloud
        out[0]->data.insert( out[0]->data.end(), frame_buffer_[i]->data.begin(), frame_buffer_[i]->data.end() );
        out[0]->width += frame_buffer_[i]->width*frame_buffer_[i]->height;

      }

      // sets the header to the most recent
      out[0]->height = 1;
      out[0]->header = in[0]->header;
      out[0]->point_step = in[0]->point_step;
      out[0]->row_step = out[0]->point_step * out[0]->width;

      // tells the base to publish this point cloud
      should_publish[0] = true;

      if( rolling_frame_ == true )
      {
        frame_buffer_.pop_back();
      }
      else
      {
        frame_buffer_.clear();
      }

    }
    return;
  }
 }
