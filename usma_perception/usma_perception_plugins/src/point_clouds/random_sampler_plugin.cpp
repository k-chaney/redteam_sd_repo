#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/random_sampler_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::RandomSamplerPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void RandomSamplerPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string RandomSamplerPlugin::setShortName()
  {
    return std::string( "rand_sample" );
  }

  bool RandomSamplerPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void RandomSamplerPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    keep_probability_ = getOption("keep_probability",0.5f);

    srand (time(NULL));
  }

  void RandomSamplerPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    int num_points = in[0]->height * in[0]->width;
    int point_step = in[0]->point_step;

    int num_kept = 0;

//    std::vector<pcl::uint8_t>::iterator  in_cloud_begin_it = in[0]->data.begin();

    for( int i = 0; i < num_points; i++ )
    {
      // check if this point should be included
      if( keep_probability_ > rand() / double(RAND_MAX) )
      {
        // if so insert it into the output cloud
        out[0]->data.insert( out[0]->data.end(), in[0]->data.begin()+(i*point_step), in[0]->data.begin()+((i+1)*point_step) );
        // keep track of how many points are kept
        num_kept++;
      }
    }

    // places the necessary metadata into the message
    out[0]->height = 1;
    out[0]->width = num_kept;
    out[0]->header = in[0]->header;
    out[0]->fields = in[0]->fields;
    out[0]->point_step = in[0]->point_step;
    out[0]->row_step = num_kept*in[0]->point_step;

    should_publish[0] = true;

    return;
  }
 }
