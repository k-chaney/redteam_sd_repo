#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/rtt_check_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::RTTCheckPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void RTTCheckPlugin::getNumInOut( int &in, int &out )
  {
    in = 2;
    out = 1;
  }

  std::string RTTCheckPlugin::setShortName()
  {
    return std::string( "RTT_Check" );
  }

  bool RTTCheckPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    if(first_ == 0)
    {
      if( has_new_cloud_data[0] == true )
        return true;
    }
    else
    {
      if( has_new_cloud_data[1] == true )
        return true;
    }
    return false;
  }

  void RTTCheckPlugin::processOptions()
  {
    first_ = 0;
    prev_time_ = ros::Time::now();
  }

  void RTTCheckPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    ros::Time cur_time = ros::Time::now();
    ROS_INFO("[RTT Check]::Time %f", (cur_time-prev_time_).toSec());
    prev_time_ = cur_time;

    if(first_ == 0)
    {
      *(out[0]) = *(in[0]);
      first_ = 1;
    }
    else
    {
      *(out[0]) = *(in[1]);
    }

    should_publish[0] = true;

    return;
  }
 }
