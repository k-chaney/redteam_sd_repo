#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/voxel_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::VoxelPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void VoxelPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string VoxelPlugin::setShortName()
  {
    return std::string( "voxelizer" );
  }

  bool VoxelPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void VoxelPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    x_leaf_size_ = getOption("x",0.1f);
    y_leaf_size_ = getOption("y",0.1f);
    z_leaf_size_ = getOption("z",0.1f);

    vg_.setLeafSize( x_leaf_size_, y_leaf_size_, z_leaf_size_ );
  }

  void VoxelPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    if ( x_leaf_size_ != 0.0 && y_leaf_size_ != 0.0 && z_leaf_size_ != 0.0 )
    {
      vg_.setInputCloud( in[0] );
      vg_.filter( *(out[0]) );
      out[0]->header = in[0]->header;
      should_publish[0] = true;
    }
    else
    {
      *(out[0]) = *(in[0]);
      should_publish[0] = true;
    }

    return;
  }
 }
