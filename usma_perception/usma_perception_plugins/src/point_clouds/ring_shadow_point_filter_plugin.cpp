#include <pluginlib/class_list_macros.h>

#include <usma_perception_plugins/point_clouds/ring_shadow_point_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::RingShadowPointFilterPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void RingShadowPointFilterPlugin::getNumInOut( int &in, int &out )
  {
    in = 1;
    out = 1;
  }

  std::string RingShadowPointFilterPlugin::setShortName()
  {
    return std::string( "shadow_point" );
  }

  bool RingShadowPointFilterPlugin::isInputReady( std::vector<bool> has_new_cloud_data )
  {
    // logical or statement (if anything is true, return true)
    for( int i=0; i<has_new_cloud_data.size(); i++ )
      if( has_new_cloud_data[i] == true )
        return true;
  }

  void RingShadowPointFilterPlugin::processOptions()
  {
    // initialize pieces specific to the plugin
    diff_thresh_ = getOption("diff_threshold",0.1);
    diff_2_thresh_ = getOption("diff_2_threshold",0.1);
    num_rings_ = getOption("num_rings",32);
    dist_gain_ = getOption("dist_gain",0.5);
  }

  void RingShadowPointFilterPlugin::processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector<bool> &should_publish )
  {
    pcl::PointCloud<velodyne_pointcloud::PointXYZIR> cloud_in;
    pcl::PointCloud<velodyne_pointcloud::PointXYZIR> cloud_out;
    pcl::fromPCLPointCloud2(*(in[0]), cloud_in);

    // a vector of pointers to the actual points in memory
    std::vector< pcl::PointCloud<velodyne_pointcloud::PointXYZIR> > clouds;
    // the max index used for each ring
    std::vector<int> cloud_inds;
    clouds.resize( num_rings_ );
    cloud_inds.resize( num_rings_ );
    for( int i = 0; i<num_rings_; i++ )
    {
      // points should be roughly evenly distributed so this should allocate
      // more than enough points in each cloud
      clouds[i].resize( (int) (cloud_in.size() / (num_rings_/8)) );
      cloud_inds[i] = 0;
    }
    cloud_out.resize( (int) cloud_in.size() );

    // sort the cloud into each individual ring
    velodyne_pointcloud::PointXYZIR* cp;
    for( int i=0; i<cloud_in.size(); i++ )
    {
      cp = &(cloud_in[i]);

      clouds[cp->ring][cloud_inds[cp->ring]].x = cp->x;
      clouds[cp->ring][cloud_inds[cp->ring]].y = cp->y;
      clouds[cp->ring][cloud_inds[cp->ring]].z = cp->z;
      clouds[cp->ring][cloud_inds[cp->ring]].intensity = cp->intensity;
      clouds[cp->ring][cloud_inds[cp->ring]].ring = cp->ring;
      cloud_inds[cp->ring]++;
    }

    unsigned int co_ind = 0;
    double curd, prevd, pprevd, nextd, nnextd;
    for( int i=0; i<num_rings_; i++ )
    {
      if( cloud_inds[i] > 5 )
      {
        clouds[i].resize( cloud_inds[i] );

        pprevd = dist3Dsquared(clouds[i][0]);
        prevd = dist3Dsquared(clouds[i][1]);
        curd = dist3Dsquared(clouds[i][2]);
        nextd = dist3Dsquared(clouds[i][3]);

        for( int j=2; j<clouds[i].size()-2; j++ )
        {
          nnextd = dist3Dsquared(clouds[i][j + 2]);
          if( 
              ( fabs(curd - prevd)  / (dist_gain_ * curd) ) < diff_thresh_ &&
              ( fabs(curd - pprevd) / (dist_gain_ * curd) ) < diff_thresh_ &&
              ( fabs(curd - nextd)  / (dist_gain_ * curd) ) < diff_thresh_ &&
              ( fabs(curd - nnextd) / (dist_gain_ * curd) ) < diff_thresh_ &&
              ( fabs( (nnextd - nextd)-(nextd - curd) ) / (dist_gain_ * curd) ) < diff_2_thresh_ &&
              ( fabs( (nextd - curd)-(curd - prevd) )   / (dist_gain_ * curd) ) < diff_2_thresh_ &&
              ( fabs( (curd - prevd)-(prevd - pprevd) ) / (dist_gain_ * curd) ) < diff_2_thresh_
            )
          {
            cloud_out[co_ind++] = clouds[i][j];
          }
          pprevd = prevd;
          prevd = curd;
          curd = nextd;
          nextd = nnextd;
        }
      }
    }

    cloud_out.resize( co_ind );

    pcl::toPCLPointCloud2(cloud_out, *(out[0]));
    should_publish[0] = true;
    out[0]->header = in[0]->header;

    return;
  }
 }
