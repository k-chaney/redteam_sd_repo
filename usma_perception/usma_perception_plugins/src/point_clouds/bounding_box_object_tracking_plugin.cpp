#include <pluginlib/class_list_macros.h>
#include <usma_perception_plugins/point_clouds/bounding_box_object_tracking_plugin.h>

PLUGINLIB_EXPORT_CLASS(usma_perception::BoundingBoxObjectTrackingPlugin, usma_perception::PerceptionPluginBase)

namespace usma_perception
{
  void BoundingBoxObjectTrackingPlugin::setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
  {
    sn = std::string("box_tracking");
    // In topic types
    it.push_back( std::string( "pcl::PCLPointCloud2" )); // input cloud -- should maintain from a constant source and frame

    // Out topic types
    ot.push_back( std::string( "usma_perception_msgs::TrackedBoundingBoxArray" )); // Bounding box array
    ot.push_back( std::string( "visualization_msgs::MarkerArray" ));
    ot.push_back( std::string( "pcl::PCLPointCloud2" )); 
  }

  bool BoundingBoxObjectTrackingPlugin::checkInputReady()
  {
    return in_cloud_sub_->dataReady();
  }

  void BoundingBoxObjectTrackingPlugin::processOptions()
  {
    twist_smoothing_alpha_ = getOption<double>("twist_smoothing_alpha", 0.5);
    pose_smoothing_alpha_ = getOption<double>("pose_smoothing_alpha", 0.9);
    max_dist_between_pts_ = getOption<double>("max_dist", 0.1);
    min_pts_ = getOption<int>("min_pts", 20);
    ec_.setClusterTolerance ( max_dist_between_pts_ );
    ec_.setMinClusterSize ( min_pts_ );
    ec_.setMaxClusterSize (25000);
  }

  void BoundingBoxObjectTrackingPlugin::init()
  {
    processOptions();
    // topics and services initialization
    in_cloud_sub_ = boost::any_cast<boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >(subs_[0]);
    out_bounding_box_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
    out_marker_array_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[1]);
    out_point_cloud_pub_ =  boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[2]);
  }

  void BoundingBoxObjectTrackingPlugin::workLoad()
  {
    bool publish_vis_markers = out_marker_array_pub_->getNumSubscribers();
    bool publish_vis_pc = out_point_cloud_pub_->getNumSubscribers();

    if( new_options_ready_ )
    {
      options_mtx_.lock();
      processOptions();
      new_options_ready_ = false;
      options_mtx_.unlock();
    }

    pcl::PCLPointCloud2::ConstPtr ros_in = in_cloud_sub_->getData();

    if ( ros_in->data.size() == 0 )
      return;

    pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_in;
    pcl_in.reset( new pcl::PointCloud<pcl::PointXYZ> );
    pcl::fromPCLPointCloud2( *(ros_in), *(pcl_in) );

    usma_perception_msgs::TrackedBoundingBoxArray cur_box_array;

    pcl::KdTreeFLANN< pcl::PointXYZ > kd_tree;
    kd_tree.setInputCloud( pcl_in );

    // Creating the KdTree object for the search method of the extraction
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud ( pcl_in );

    std::vector<pcl::PointIndices> cluster_indices;
    ec_.setSearchMethod (tree);
    ec_.setInputCloud (pcl_in);
    ec_.extract (cluster_indices);

    cur_box_array.boxes.resize( cluster_indices.size() );
    cur_box_array.header.frame_id = ros_in->header.frame_id;
    cur_box_array.header.seq = ros_in->header.seq;
    cur_box_array.header.stamp = ros::Time::now();

    pcl::PointCloud<pcl::PointXYZ> pcl_out;
    pcl::PCLPointCloud2 ros_out;
    std::vector<int> ids_to_delete;

    for( int i = 0; i < cluster_indices.size(); i++ )
    {
      if ( publish_vis_pc )
      {
        for( int j =  0; j < cluster_indices[i].indices.size(); j++ )
        {
          pcl_out.push_back( pcl_in->at(cluster_indices[i].indices[j]) );
        }
      }

      // compute center, min, and max
      Eigen::Vector4f centroid, min, max;
      pcl::compute3DCentroid( *pcl_in, cluster_indices[i].indices, centroid  );
      pcl::getMinMax3D( *pcl_in, cluster_indices[i].indices, min, max );

      cur_box_array.boxes[i].header = cur_box_array.header;
      cur_box_array.boxes[i].id = rand(); // generate a random id -- to be matched later

      cur_box_array.boxes[i].min.x = min[0] - centroid[0] - max_dist_between_pts_;
      cur_box_array.boxes[i].min.y = min[1] - centroid[1] - max_dist_between_pts_;
      cur_box_array.boxes[i].min.z = min[2] - centroid[2] - max_dist_between_pts_;

      cur_box_array.boxes[i].max.x = max[0] - centroid[0] + max_dist_between_pts_;
      cur_box_array.boxes[i].max.y = max[1] - centroid[1] + max_dist_between_pts_;
      cur_box_array.boxes[i].max.z = max[2] - centroid[2] + max_dist_between_pts_;

      cur_box_array.boxes[i].pose.position.x = centroid[0];
      cur_box_array.boxes[i].pose.position.y = centroid[1];
      cur_box_array.boxes[i].pose.position.z = centroid[2];
    }

    if( box_array_vector_.size() > 0 )
    {
      std::vector< std::pair<int, int> > box_pairs;
      for( int i = 0; i < cur_box_array.boxes.size(); i++ )
      {
        int m = findBoxMatch( cur_box_array.boxes[i], box_array_vector_.back() );
        if( m < box_array_vector_.back().boxes.size() )
          box_pairs.push_back( std::pair<int, int>( i, m ) );
      }
      for( int i = 0; i < box_pairs.size(); i++ )
      {
        poseSmoothing( box_array_vector_.back().boxes[ box_pairs[i].second ].pose, cur_box_array.boxes[ box_pairs[i].first ].pose, pose_smoothing_alpha_ );

        twistFromPoses(cur_box_array.boxes[ box_pairs[i].first ].twist, 
            box_array_vector_.back().boxes[ box_pairs[i].second ].pose, cur_box_array.boxes[ box_pairs[i].first ].pose, 
            ( box_array_vector_.back().boxes[ box_pairs[i].second ].header.stamp - cur_box_array.boxes[ box_pairs[i].first ].header.stamp ).toSec() );

        twistSmoothing( box_array_vector_.back().boxes[ box_pairs[i].second ].twist, cur_box_array.boxes[ box_pairs[i].first ].twist, twist_smoothing_alpha_ );

        cur_box_array.boxes[ box_pairs[i].first ].id = box_array_vector_.back().boxes[ box_pairs[i].second ].id;
        cur_box_array.boxes[ box_pairs[i].first ].min.x = std::min(cur_box_array.boxes[ box_pairs[i].first ].min.x, box_array_vector_.back().boxes[ box_pairs[i].second ].min.x);
        cur_box_array.boxes[ box_pairs[i].first ].min.y = std::min(cur_box_array.boxes[ box_pairs[i].first ].min.y, box_array_vector_.back().boxes[ box_pairs[i].second ].min.y);
        cur_box_array.boxes[ box_pairs[i].first ].min.z = std::min(cur_box_array.boxes[ box_pairs[i].first ].min.z, box_array_vector_.back().boxes[ box_pairs[i].second ].min.z);
        cur_box_array.boxes[ box_pairs[i].first ].max.x = std::max(cur_box_array.boxes[ box_pairs[i].first ].max.x, box_array_vector_.back().boxes[ box_pairs[i].second ].max.x);
        cur_box_array.boxes[ box_pairs[i].first ].max.y = std::max(cur_box_array.boxes[ box_pairs[i].first ].max.y, box_array_vector_.back().boxes[ box_pairs[i].second ].max.y);
        cur_box_array.boxes[ box_pairs[i].first ].max.z = std::max(cur_box_array.boxes[ box_pairs[i].first ].max.z, box_array_vector_.back().boxes[ box_pairs[i].second ].max.z);
      }

    for( int i = 0; i < box_array_vector_.back().boxes.size(); i++ )
    {
      bool tracked = false;
      for( int j = 0; j < box_pairs.size(); j++ )
      {
        if( i == box_pairs[j].first )
          tracked = true;
      }
      if ( !tracked )
        ids_to_delete.push_back( box_array_vector_.back().boxes[i].id );
    }

      // only track based on the previous one
      box_array_vector_.clear();


    }
    box_array_vector_.push_back( cur_box_array );

    // bounding boxes for passing to other nodes
    if( out_bounding_box_pub_->getNumSubscribers() )
      out_bounding_box_pub_->publish( cur_box_array );

    // visualize the bounding boxes
    if( publish_vis_markers )
      publishVisualizationMarkers( cur_box_array, ids_to_delete );

    // useful for checking which points are being tracked
    if( publish_vis_pc )
    {
      pcl::toPCLPointCloud2( pcl_out, ros_out );
      ros_out.header.frame_id = cur_box_array.header.frame_id;
      ros_out.header.seq = cur_box_array.header.seq;
      out_point_cloud_pub_->publish( ros_out );
    }

    return;
  }

  inline void BoundingBoxObjectTrackingPlugin::twistFromPoses( geometry_msgs::Twist &t, geometry_msgs::Pose &pp, geometry_msgs::Pose &np, double dt )
  {
    t.linear.x = ( np.position.x - pp.position.x )/dt;
    t.linear.y = ( np.position.y - pp.position.y )/dt;
    t.linear.z = ( np.position.z - pp.position.z )/dt;

    return;
  }

  inline void BoundingBoxObjectTrackingPlugin::poseSmoothing( geometry_msgs::Pose &pp, geometry_msgs::Pose &np, double alpha )
  {
    np.position.x = ( (alpha) * np.position.x + (1.0 - alpha) * pp.position.x );
    np.position.y = ( (alpha) * np.position.y + (1.0 - alpha) * pp.position.y );
    np.position.z = ( (alpha) * np.position.z + (1.0 - alpha) * pp.position.z );

    return;
  }

  inline void BoundingBoxObjectTrackingPlugin::twistSmoothing( geometry_msgs::Twist &pt, geometry_msgs::Twist &nt, double alpha )
  {
    nt.linear.x = ( (alpha) * nt.linear.x + (1.0 - alpha) * pt.linear.x );
    nt.linear.y = ( (alpha) * nt.linear.y + (1.0 - alpha) * pt.linear.y );
    nt.linear.z = ( (alpha) * nt.linear.z + (1.0 - alpha) * pt.linear.z );

    return;
  }

  inline double BoundingBoxObjectTrackingPlugin::dist( double x1, double y1, double z1, double x2, double y2, double z2 )
  {
    return sqrt( (x2-x1)*(x2-x1) + (y2-y1)*(y2-y1) + (z2-z1)*(z2-z1) );
  }

  inline int BoundingBoxObjectTrackingPlugin::findBoxMatch( usma_perception_msgs::TrackedBoundingBox &b, usma_perception_msgs::TrackedBoundingBoxArray &ba )
  {
    int match = ba.boxes.size();

    for ( int i = 0; i < ba.boxes.size(); i++ )
    {
      double d = dist( b.pose.position.x, b.pose.position.y, b.pose.position.z, ba.boxes[i].pose.position.x, ba.boxes[i].pose.position.y, ba.boxes[i].pose.position.z );
      double d_min = dist( b.min.x, b.min.y, b.min.z, 0.0, 0.0, 0.0 );
      double d_max = dist( b.max.x, b.max.y, b.max.z, 0.0, 0.0, 0.0 );
      double t = 2 * abs(( b.header.stamp - ba.boxes[i].header.stamp ).toSec()); // should be somewhere within 2 times the delayed time
      double d_check = dist( 0.0, 0.0, 0.0, ba.boxes[i].twist.linear.x * t, ba.boxes[i].twist.linear.y * t, ba.boxes[i].twist.linear.z * t ) + ((d_min>d_max)? (d_min) : (d_max));
      if( d < d_check )
        match = i;
    }

    return match;
  }

  void BoundingBoxObjectTrackingPlugin::publishVisualizationMarkers( usma_perception_msgs::TrackedBoundingBoxArray &ca, std::vector<int> ids_to_delete )
  {
    visualization_msgs::MarkerArray tracked_objects;
    tracked_objects.markers.resize( ca.boxes.size() );

    for( int i = 0; i < ca.boxes.size(); i++ )
    {
      tracked_objects.markers[i].header = ca.header;
      tracked_objects.markers[i].ns = ca.header.frame_id;
      tracked_objects.markers[i].id = ca.boxes[i].id;
      tracked_objects.markers[i].type = visualization_msgs::Marker::CUBE;
      tracked_objects.markers[i].action = visualization_msgs::Marker::ADD;
      tracked_objects.markers[i].pose = ca.boxes[i].pose;
      tracked_objects.markers[i].scale.x = (ca.boxes[i].max.x - ca.boxes[i].min.x);
      tracked_objects.markers[i].scale.y = (ca.boxes[i].max.y - ca.boxes[i].min.y);
      tracked_objects.markers[i].scale.z = (ca.boxes[i].max.z - ca.boxes[i].min.z);
      tracked_objects.markers[i].color.a = 1.0;
      // generate a pseudo unique color based on the id of the box
      // if the ID changes the color should too
      tracked_objects.markers[i].color.r = ( (double) (ca.boxes[i].id % 11) )/( (double) 10.0 );
      tracked_objects.markers[i].color.g = ( (double) ((ca.boxes[i].id/11) % 11) )/( (double) 10.0 );
      tracked_objects.markers[i].color.b = ( (double) ((ca.boxes[i].id/121) % 11) )/( (double) 10.0 );
    }

    if( ids_to_delete.size() > 0 )
    {
    tracked_objects.markers.resize( ca.boxes.size() + ids_to_delete.size() );
    for( int i = 0; i < (ids_to_delete.size()); i++ )
    {
      tracked_objects.markers[i + ca.boxes.size()].header = ca.header;
      tracked_objects.markers[i + ca.boxes.size()].ns = ca.header.frame_id;
      tracked_objects.markers[i + ca.boxes.size()].id = (ids_to_delete[i]);
      tracked_objects.markers[i + ca.boxes.size()].action = visualization_msgs::Marker::DELETE;
    }
    }

    out_marker_array_pub_->publish( tracked_objects );
  }
  
  inline pcl::PointXYZ BoundingBoxObjectTrackingPlugin::subtractPoints( pcl::PointXYZ &a, pcl::PointXYZ &b )
  {
    pcl::PointXYZ t;
    t.x = a.x - b.x;
    t.y = a.y - b.y;
    t.z = a.z - b.z;
    return t;
  }

  inline void BoundingBoxObjectTrackingPlugin::updateMinMax( pcl::PointXYZ &p, pcl::PointXYZ &min, pcl::PointXYZ &max )
  {
    if( p.x < min.x )
      min.x = p.x;
    else if( p.x > max.x )
      max.x = p.x;

    if( p.y < min.y )
      min.y = p.y;
    else if( p.x > max.y )
      max.y = p.y;

    if( p.z < min.z )
      min.z = p.z;
    else if( p.z > max.z )
      max.z = p.z;

    return;
  }
}
