/*
 *
 * Yet again another terrible unit test file because data was copied directly in via a matlab script
 *
 * TODO
 *
 * Read from a text file that has all of the feature maps in a useful format.
 *
 */


#include <usma_perception_plugins/images/msckf.h>
#include <usma_perception_plugins/images/msckf_utils.h>
#include <ros/ros.h>

#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace usma_perception::msckf;

int loadKittiMeasFile( std::string file_name, measurement &mk )
{
  std::string line;
  std::ifstream mf( file_name.c_str() );
  int ln = 0;
  if( mf.is_open() )
  {
    while( std::getline( mf, line ) )
    {
      std::string ele;
      std::istringstream iss(line);

      if( ln == 0 )
      {
        mk.dT_ = atof( line.c_str() );
      }
      else if( ln == 1 )
      {
        int ec = 0;
        while( std::getline( iss, ele, ',' ) )
        {
          mk.omega_[ ec++ ] =  atof(ele.c_str());
        }
      }
      else if( ln == 2 )
      {
        int ec = 0;
        while( std::getline( iss, ele, ',' ) )
        {
          mk.v_[ ec++ ] =  atof(ele.c_str());
        }
      }
      else
      {
        int idx;
        Eigen::Matrix<double,2,1> e;
        std::getline(iss,ele,',');
        idx = atoi(ele.c_str());
        std::getline(iss,ele,',');
        e(0) = atof(ele.c_str());
        std::getline(iss,ele,',');
        e(1) = atof(ele.c_str());
        mk.y_.insert( std::make_pair(idx, e) );
      }
      ln++;
    }
  }
  else
  {
    std::cout << file_name << " Not Opened" << std::endl;
    return 1;
  }
  mf.close();
  return 0;
}

void loadKittiMeasurements( std::string folder_name, std::vector<measurement> &measurements )
{
  int start = 0;
  int end = 99;
  char buf[33];

  measurements.resize( end - start );

  for( int i = start; i < end; i++ )
  {
    snprintf(buf, sizeof(buf), "%d", i);
    int ret = loadKittiMeasFile( folder_name + std::string("/measurement_frame_") + std::string(buf), measurements[i - start] );
    if ( ret != 0 )
      exit(ret);
  }

}

int msckfTest( int num_frames )
{
  camera c; 
  c.cu_ = 609.559300; 
  c.cv_ = 172.854000; 
  c.fu_ = 721.537700; 
  c.fv_ = 721.537700; 
  c.b_ = 0.537151; 
  c.q_CI_ << 0.499209, -0.496573, 0.502908, -0.501288; 
  c.p_CI_ << 1.083302, -0.309889, 0.729920; 

  imuState is; 
  is.q_IG_ << 0.000967, -0.000954, -0.002936, 0.999995; 
  is.p_IG_ << 0.630851, -0.000605, 0.003137; 
  is.b_g_.setZero();
  is.b_v_.setZero();

  int ks = 2; 

  noiseParams np; 
  np.u_var_prime_ = 0.000232; 
  np.v_var_prime_ = 0.000232; 
  np.Q_imu_ << 0.040000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.040000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.040000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.040000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.040000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.040000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001; 
  np.initial_IMU_covar_ << 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001, 0.000000, 
    0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000001; 

  msckfParams mp; 
  mp.min_track_length_ = 5; 
  mp.max_track_length_ = 15; 
  mp.max_gn_cost_norm_ = 0.010000; 
  mp.min_rcond_ = 0.000000; 
  mp.do_null_space_trick_ = true; 
  mp.do_qr_decomp_ = true; 
  mp.is_velocity_ = true; 

  std::vector<measurement> measurements;

  loadKittiMeasurements( "/home/ken/catkin_ws/src/redteam_sd_repo/system_launch/kitti_tagged_frames", measurements );
  std::cout << "Loaded " << measurements.size() << " measurments" << std::endl;

  MSCKF msckf_test_object( is, measurements.front(), c, ks, np, mp );

  for( int i = 1; i < num_frames; i++ )
  {
    msckf_test_object.update( measurements[i] );
    std::cout << (msckf_test_object.getMSCKFState())->imu_state_.p_IG_.transpose() << std::endl;
  }

  return 0;
}

int main( int argc, char** argv )
{
  ros::init(argc,argv,"msckf_test");
  return msckfTest( atoi( argv[1] ) );
}
