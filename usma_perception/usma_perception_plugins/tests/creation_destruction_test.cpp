#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>

// generator
#include <usma_perception_plugins/point_cloud_generators/random_point_cloud_plugin.h>

// point cloud filter
#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>


int main( int argc, char** argv )
{
  ros::init(argc, argv, "point_cloud_filters_test");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  ros::AsyncSpinner spinner(8);
  spinner.start();

  int num_tests = 100;
  ros::Rate r_create(100.0);

  /////////////////////// DATA SOURCE
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  out_topics.push_back("in");
  options.push_back( std::pair<std::string, std::string>("rate","100.0") );
  options.push_back( std::pair<std::string, std::string>("size","10") );

  usma_perception::RandPCGen *data_source;
  data_source = new usma_perception::RandPCGen;
  data_source->initialize( in_topics, out_topics, in_types, options, n );
  /////////////////////// DATA SOURCE

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("out");

  // crate test plugins -- this one has been an issue being destroyed in the past
  std::vector<boost::shared_ptr<usma_perception::RadiusOutlierFilterPlugin> > filter_vector;
  for( int j = 0; j < num_tests; j++ )
  {
    boost::shared_ptr<usma_perception::RadiusOutlierFilterPlugin> tmp;
    tmp.reset( new usma_perception::RadiusOutlierFilterPlugin );
    out_topics[0] = std::string("out")+boost::to_string(j);
    tmp->initialize( in_topics, out_topics, in_types, options, n );
    filter_vector.push_back( tmp );
    r_create.sleep();
  }

  // wait 5 seconds
  ros::Rate r(1.0);
  int i = 5;
  while ( ros::ok() && (i-- > 0) )
    r.sleep();

  for( int j = 0; j < num_tests; j++ )
  {
    ROS_ERROR("Filter Vector Removed %d", j);
    filter_vector[j]->stop();
  }

  for( int j = 0; j < num_tests; j++ )
    filter_vector.pop_back();
 
  spinner.stop();
  ros::shutdown(); 

  return 0;
}
