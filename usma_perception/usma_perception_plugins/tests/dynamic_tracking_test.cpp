#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>
#include <usma_perception_plugins/point_cloud_generators/object_over_plane_gen_plugin.h>
#include <usma_perception_plugins/octomap/octomap_plugin.h>
#include <usma_perception_plugins/octomap/octomap_background_subtraction_plugin.h>
#include <usma_perception_plugins/point_clouds/bounding_box_object_tracking_plugin.h>
#include <usma_perception_plugins/point_clouds/bounding_box_subtraction_plugin.h>

int main( int argc, char** argv )
{
  /*
   * ---data
   * |   \/
   * |  box subtraction <------
   * |   \/                   |
   * |  octomap               |
   * |   \/                   |
   * -->octomap subtraction   |
   *     \/                   |
   *    box tracking-----------
   */
  int i = 0;
  ros::init(argc, argv, "dynamic_object_test");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  /////////////////////// DATA SOURCE
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  out_topics.push_back("in");
  options.push_back(std::pair<std::string, std::string>("frame","map"));
  options.push_back(std::pair<std::string, std::string>("resolution","0.05"));
  options.push_back(std::pair<std::string, std::string>("path_radius","2.0"));
  options.push_back(std::pair<std::string, std::string>("period","10.0"));
  options.push_back(std::pair<std::string, std::string>("rate","1.0"));
  options.push_back(std::pair<std::string, std::string>("num_objects","2"));

  usma_perception::OOPPCGen *data_source;
  data_source = new usma_perception::OOPPCGen;
  data_source->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
  /////////////////////// DATA SOURCE

  /////////////////////// BOX SUBTRACTION
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_topics.push_back("non_static_objects/bounding_boxes");
  in_types.push_back("pcl::PCLPointCloud2");
  in_types.push_back("usma_perception_msgs::TrackedBoundingBoxArray");
  out_topics.push_back("in/box_sub");

  usma_perception::BoundingBoxSubtractionPlugin *box_sub;
  box_sub = new usma_perception::BoundingBoxSubtractionPlugin;
  box_sub->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
  /////////////////////// BOX SUBTRACTION

  /////////////////////// OCTOMAP
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/box_sub");
  out_topics.push_back("octomap_full");
  out_topics.push_back("octomap_full_vis_markers");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("world_frame","map") );
  options.push_back( std::pair<std::string,std::string>("sensor_frame","map") );
  options.push_back( std::pair<std::string,std::string>("resolution","0.1") );
  options.push_back( std::pair<std::string,std::string>("sensor_hit","0.6") );
  options.push_back( std::pair<std::string,std::string>("sensor_miss","0.3") );
  options.push_back( std::pair<std::string,std::string>("sensor_min","0.12") );
  options.push_back( std::pair<std::string,std::string>("sensor_max","0.6") );
  options.push_back( std::pair<std::string,std::string>("sensor_max_range","25.0") );
  options.push_back( std::pair<std::string,std::string>("compress_map","1") );

  usma_perception::OctomapPlugin *octomap_plugin;
  octomap_plugin = new usma_perception::OctomapPlugin;
  octomap_plugin->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
  /////////////////////// OCTOMAP

  /////////////////////// OCTOMAP background subtraction
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_topics.push_back("octomap_full");
  out_topics.push_back("non_static_objects");
  in_types.push_back("pcl::PCLPointCloud2");
  in_types.push_back("octomap_msgs::Octomap");
  options.push_back( std::pair<std::string,std::string>("search_resolution","0.2") );
  options.push_back( std::pair<std::string,std::string>("thresh_probability","0.4") );
  options.push_back( std::pair<std::string,std::string>("octomap_freq_divider","4") );

  usma_perception::OctomapBackgroundSubtractionPlugin *octomap_background_sub_plugin;
  octomap_background_sub_plugin = new usma_perception::OctomapBackgroundSubtractionPlugin;
  octomap_background_sub_plugin->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
  /////////////////////// OCTOMAP background subtraction

  /////////////////////// bounding box object tracking
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("non_static_objects");
  out_topics.push_back("non_static_objects/bounding_boxes");
  out_topics.push_back("non_static_objects/bounding_boxes_vis");
  out_topics.push_back("non_static_objects/bounding_boxes_pc");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("max_dist","0.2") );
  options.push_back( std::pair<std::string,std::string>("min_pts","40") );

  usma_perception::BoundingBoxObjectTrackingPlugin *tracking_plugin;
  tracking_plugin = new usma_perception::BoundingBoxObjectTrackingPlugin;
  tracking_plugin->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
  /////////////////////// bounding box object tracking

  ros::AsyncSpinner spinner(8);
  spinner.start();

  ros::Rate r(1.0);
  int cycles = 30;
  while ( ros::ok() && (cycles-- > 0) )
    r.sleep();
  
  spinner.stop();
  ros::shutdown();

  return 0;
}
