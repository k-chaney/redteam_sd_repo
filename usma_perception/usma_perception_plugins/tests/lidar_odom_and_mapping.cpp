#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>

// Used for Odometry and Mapping
#include <usma_perception_plugins/point_clouds/ring_shadow_point_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/ring_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/voxel_plugin.h>
#include <usma_perception_plugins/point_clouds/transform_plugin.h>
#include <usma_perception_plugins/point_clouds/gicp_odom_plugin.h>
#include <usma_perception_plugins/point_clouds/ethz_icp_mapping_plugin.h>

// Used for Octomap generation
#include <usma_perception_plugins/point_clouds/transform_plugin.h>
#include <usma_perception_plugins/point_clouds/freq_divider_plugin.h>
#include <usma_perception_plugins/point_clouds/time_delay_plugin.h>
#include <usma_perception_plugins/octomap/octomap_plugin.h>

// Used for background subtraction + dynamic obstacles
#include <usma_perception_plugins/octomap/octomap_background_subtraction_plugin.h>
#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>

int main( int argc, char** argv )
{

  int i = 0;
  ros::init(argc, argv, "loam_pipeline");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  // ---------------------------------
  //    LOAD RING SHADOW POINT PLUGIN
  // ---------------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring");
  out_topics.push_back("in/ring/rs");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("diff_threshold","0.2") );
  options.push_back( std::pair<std::string,std::string>("diff_2_threshold","0.1") );
  options.push_back( std::pair<std::string,std::string>("dist_gain","0.15") );
  options.push_back( std::pair<std::string,std::string>("num_rings","32") );

  usma_perception::RingShadowPointFilterPlugin *rsp;
  rsp = new usma_perception::RingShadowPointFilterPlugin();
  rsp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD RING PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points");
  out_topics.push_back("in/ring");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back( std::pair<std::string,std::string>("num_rings","32") );

  for( int j=0; j<32; j++ )
  {
    if(j%4==0 || j<10)
      options.push_back( std::pair<std::string,std::string>(std::string("ring_")+boost::lexical_cast<std::string>(j),"0") );
    else
      options.push_back( std::pair<std::string,std::string>(std::string("ring_")+boost::lexical_cast<std::string>(j),"1") );
  }

  usma_perception::RingFilterPlugin *rp;
  rp = new usma_perception::RingFilterPlugin();
  rp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD CROPPER PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs");
  out_topics.push_back("in/ring/rs/crop");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x_min","-25.0") );
  options.push_back( std::pair<std::string,std::string>("y_min","-25.0") );
  options.push_back( std::pair<std::string,std::string>("z_min","-25.0") );
  options.push_back( std::pair<std::string,std::string>("x_max","25.0") );
  options.push_back( std::pair<std::string,std::string>("y_max","25.0") );
  options.push_back( std::pair<std::string,std::string>("z_max","25.0") );

  usma_perception::CropperPlugin *cp;
  cp = new usma_perception::CropperPlugin();
  cp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/crop");
  out_topics.push_back("in/ring/rs/crop/voxel");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.05") );
  options.push_back( std::pair<std::string,std::string>("y","0.05") );
  options.push_back( std::pair<std::string,std::string>("z","0.05") );

  usma_perception::VoxelPlugin *vp;
  vp = new usma_perception::VoxelPlugin();
  vp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/crop/voxel");
  out_topics.push_back("in/ring/rs/crop/voxel/tf");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back(std::pair<std::string,std::string>("tf_fudge_factor","1.0"));

  usma_perception::TransformPlugin *tp;
  tp = new usma_perception::TransformPlugin();
  tp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ---------------------------
  //    LOAD ODOMETRY PLUGIN
  // ---------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/crop/voxel/tf");
  in_topics.push_back("/imu/data");
  out_topics.push_back("odom_est");
  out_topics.push_back("in/ring/rs/crop/voxel/tf/odom");
  in_types.push_back("pcl::PCLPointCloud2");
  //in_types.push_back("sensor_msgs::PointCloud2");
  in_types.push_back("sensor_msgs::Imu");

  options.push_back( std::pair<std::string,std::string>("config_file_name","/home/user1/catkin_ws/src/redteam_sd_repo/system_launch/launch/icp_odom.yaml") );
  options.push_back( std::pair<std::string,std::string>("min_overlap","0.3") );
  options.push_back( std::pair<std::string,std::string>("sensor_frame","velodyne") );

  //usma_perception::ethzOdomPlugin *eop;
  //eop = new usma_perception::ethzOdomPlugin();
  usma_perception::GICPOdomPlugin *eop;
  eop = new usma_perception::GICPOdomPlugin();
  eop->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD FREQ DIV PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/crop/voxel/tf/odom");
  out_topics.push_back("in/ring/rs/crop/voxel/tf/odom/freq");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("freq_divider","10") );

  usma_perception::FreqDividerPlugin *fdp;
  fdp = new usma_perception::FreqDividerPlugin();
  fdp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ---------------------------
  //    LOAD MAPPING PLUGIN
  // ---------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/crop/voxel/tf/odom/freq");
  in_topics.push_back("map_in");
  in_topics.push_back("odom_est");

  out_topics.push_back("map_out");
  out_topics.push_back("odom");
  out_topics.push_back("odom_err");

  in_types.push_back("sensor_msgs::PointCloud2");
  in_types.push_back("sensor_msgs::PointCloud2");
  in_types.push_back("nav_msgs::Odometry");

  options.push_back( std::pair<std::string,std::string>("config_file_name","/home/user1/catkin_ws/src/redteam_sd_repo/system_launch/launch/icp.yaml") );
  options.push_back( std::pair<std::string,std::string>("min_overlap","0.3") );
  options.push_back( std::pair<std::string,std::string>("min_reading_point_count","500") );
  options.push_back( std::pair<std::string,std::string>("sensor_frame","velodyne") );

  usma_perception::ethzMappingPlugin *emp;
  emp = new usma_perception::ethzMappingPlugin();
  emp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("map_out");
  out_topics.push_back("map_in");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.05") );
  options.push_back( std::pair<std::string,std::string>("y","0.05") );
  options.push_back( std::pair<std::string,std::string>("z","0.05") );

  usma_perception::VoxelPlugin *vp2;
  vp2 = new usma_perception::VoxelPlugin();
  vp2->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/freq/time_delay");
  out_topics.push_back("in/ring/rs/freq/time_delay/tf_bl");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("sensor_frame","velodyne"));
  options.push_back(std::pair<std::string,std::string>("goal_frame","base_link"));

  usma_perception::TransformPlugin *tp3;
  tp3 = new usma_perception::TransformPlugin();
  tp3->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "in/ring/rs/freq/time_delay/tf_bl");
  out_topics.push_back("in/ring/rs/freq/time_delay/tf_bl/tf_map");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("sensor_frame","base_link_tc"));
  options.push_back(std::pair<std::string,std::string>("goal_frame","map_tc"));

  usma_perception::TransformPlugin *tp2;
  tp2 = new usma_perception::TransformPlugin();
  tp2->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD Time delay PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/freq");
  out_topics.push_back("in/ring/rs/freq/time_delay");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("count_delay","4") );

  usma_perception::TimeDelayPlugin *tdp;
  tdp = new usma_perception::TimeDelayPlugin();
  tdp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD FREQ DIV PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs");
  out_topics.push_back("in/ring/rs/freq");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("freq_divider","20") );

  usma_perception::FreqDividerPlugin *fdp2;
  fdp2 = new usma_perception::FreqDividerPlugin();
  fdp2->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  /////////////////////// OCTOMAP
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/ring/rs/freq/time_delay/tf_bl/tf_map");
  out_topics.push_back("octomap_full");
  out_topics.push_back("octomap_full_vis_markers");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("publish_frame","map") );
  options.push_back( std::pair<std::string,std::string>("world_frame","map_tc") );
  options.push_back( std::pair<std::string,std::string>("sensor_frame","velodyne") );
  options.push_back( std::pair<std::string,std::string>("resolution","0.2") );
  options.push_back( std::pair<std::string,std::string>("sensor_hit","0.6") );
  options.push_back( std::pair<std::string,std::string>("sensor_miss","0.05") );
  options.push_back( std::pair<std::string,std::string>("sensor_min","0.1") );
  options.push_back( std::pair<std::string,std::string>("sensor_max","0.6") );
  options.push_back( std::pair<std::string,std::string>("sensor_max_range","20.0") );
  options.push_back( std::pair<std::string,std::string>("compress_map","1") );

  usma_perception::OctomapPlugin *octomap_plugin;
  octomap_plugin = new usma_perception::OctomapPlugin;
  octomap_plugin->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD CROPPER PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back ("/velodyne_points");
  out_topics.push_back("in/crop");
  in_types.push_back("pcl::PCLPointCloud2");
   options.push_back( std::pair<std::string,std::string>("x_max","2.0") );
  options.push_back( std::pair<std::string,std::string>("x_min","-2.0") );
   options.push_back( std::pair<std::string,std::string>("y_max","2.0") );
  options.push_back( std::pair<std::string,std::string>("y_min","-2.0") );
   options.push_back( std::pair<std::string,std::string>("z_max","0.5") );
  options.push_back( std::pair<std::string,std::string>("z_min","-0.2") );

  usma_perception::CropperPlugin *cp2;
  cp2 = new usma_perception::CropperPlugin();
  cp2->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "in/crop");
  out_topics.push_back("in/crop/voxel");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.1") );
  options.push_back( std::pair<std::string,std::string>("y","0.1") );
  options.push_back( std::pair<std::string,std::string>("z","0.1") );

  usma_perception::VoxelPlugin *vp3;
  vp3 = new usma_perception::VoxelPlugin();
  vp3->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "in/crop/voxel");
  out_topics.push_back("in/crop/voxel/tf_bl");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("sensor_frame","velodyne"));
  options.push_back(std::pair<std::string,std::string>("goal_frame","base_link"));

  usma_perception::TransformPlugin *tp5;
  tp5 = new usma_perception::TransformPlugin();
  tp5->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );



  // Start the ros spinner and wait for shutdown
  ros::AsyncSpinner spinner(8); // Use 8 threads
  spinner.start();
  ros::waitForShutdown();

  return 0;
}
