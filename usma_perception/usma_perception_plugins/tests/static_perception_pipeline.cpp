#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>

#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/voxel_plugin.h>
#include <usma_perception_plugins/point_clouds/assembler_plugin.h>
#include <usma_perception_plugins/point_clouds/transform_plugin.h>
#include <usma_perception_plugins/point_clouds/random_sampler_plugin.h>
#include <usma_perception_plugins/point_clouds/freq_divider_plugin.h>
#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/statistical_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/bounding_box_object_tracking_plugin.h>
#include <usma_perception_plugins/point_clouds/bounding_box_subtraction_plugin.h>
#include <usma_perception_plugins/point_clouds/time_delay_plugin.h>

#include <usma_perception_plugins/octomap/octomap_plugin.h>
#include <usma_perception_plugins/octomap/octomap_background_subtraction_plugin.h>

#include <usma_perception_plugins/images/viso_plugin.h>
#include <usma_perception_plugins/images/resize_plugin.h>

int main( int argc, char** argv )
{

  /*
   *
   *     INPUT -- velodyne
   *      \/
   *     VOXEL
   *      \/     
   *     CROP 
   *      \/ 
   *   TRANSFORM --> FREQ DIV
   *                     |
   *                  OCTOMAP
   *
   */

  int i = 0;
  ros::init(argc, argv, "static_perception_pipeline");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points");
  out_topics.push_back("in/voxel");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.05") );
  options.push_back( std::pair<std::string,std::string>("y","0.05") );
  options.push_back( std::pair<std::string,std::string>("z","0.05") );

  usma_perception::VoxelPlugin *vp;
  vp = new usma_perception::VoxelPlugin();
  vp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD CROPPER PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/voxel");
  out_topics.push_back("in/voxel/cropper");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x_min","-20.0") );
  options.push_back( std::pair<std::string,std::string>("y_min","-20.0") );
  options.push_back( std::pair<std::string,std::string>("z_min","-10.0") );
  options.push_back( std::pair<std::string,std::string>("x_max","20.0") );
  options.push_back( std::pair<std::string,std::string>("y_max","20.0") );
  options.push_back( std::pair<std::string,std::string>("z_max","10.0") );

  usma_perception::CropperPlugin *cp;
  cp = new usma_perception::CropperPlugin();
  cp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD SOF PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/voxel/cropper");
  out_topics.push_back("in/voxel/cropper/sof");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("mean_k","40") );
  options.push_back( std::pair<std::string,std::string>("std_dev_thresh","0.1") );

  usma_perception::StatisticalOutlierFilterPlugin *sofp;
  sofp = new usma_perception::StatisticalOutlierFilterPlugin();
  sofp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/voxel/cropper/sof");
  out_topics.push_back("in/voxel/cropper/sof/transform");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("goal_frame","map") );
  options.push_back( std::pair<std::string,std::string>("tf_fudge_factor","0.1") );

  usma_perception::TransformPlugin *tp;
  tp = new usma_perception::TransformPlugin();
  tp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD FREQ DIV PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/voxel/cropper/sof/transform");
  out_topics.push_back("in/voxel/cropper/sof/transform/freqdiv");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("freq_divider","10") );

  usma_perception::FreqDividerPlugin *fdp;
  fdp = new usma_perception::FreqDividerPlugin();
  fdp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );
 
  // -----------------------------
  //    LOAD OCTOMAP PLUGIN
  // -----------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in/voxel/cropper/sof/transform/freqdiv");
  out_topics.push_back("octomap_full");
  out_topics.push_back("octomap_full_vis");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("world_frame","map") );
  options.push_back( std::pair<std::string,std::string>("publish_frame","map") );
  options.push_back( std::pair<std::string,std::string>("sensor_frame","velodyne") );
  options.push_back( std::pair<std::string,std::string>("resolution","0.05") );
  options.push_back( std::pair<std::string,std::string>("sensor_hit","0.9") );
  options.push_back( std::pair<std::string,std::string>("sensor_miss","0.1") );
  options.push_back( std::pair<std::string,std::string>("sensor_min","0.12") );
  options.push_back( std::pair<std::string,std::string>("sensor_max","0.97") );
  options.push_back( std::pair<std::string,std::string>("sensor_max_range","25.0") );
  options.push_back( std::pair<std::string,std::string>("compress_map","1") );


  usma_perception::OctomapPlugin *op;
  op = new usma_perception::OctomapPlugin();
  op->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // Start the ros spinner and wait for shutdown
  ros::AsyncSpinner spinner(8); // Use 8 threads
  spinner.start();
  ros::waitForShutdown();

  return 0;
}
