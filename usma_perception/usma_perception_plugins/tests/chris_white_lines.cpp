#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>

#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/voxel_plugin.h>
#include <usma_perception_plugins/point_clouds/transform_plugin.h>
#include <usma_perception_plugins/point_clouds/gicp_odom_plugin.h>

#include <usma_perception_plugins/images/resize_plugin.h>
#include <usma_perception_plugins/images/threshold_plugin.h>
#include <usma_perception_plugins/images/color_conversion_plugin.h>
#include <usma_perception_plugins/images/plane_projection_plugin.h>
#include <usma_perception_plugins/images/gaussian_blur_plugin.h>
#include <usma_perception_plugins/images/morphology_plugin.h>
#include <usma_perception_plugins/point_clouds/gicp_odom_plugin.h>
#include <usma_perception_plugins/point_clouds/ring_shadow_point_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/ring_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>


#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/multi_cloud_assembler_plugin.h>
#include <usma_perception_plugins/octomap/octomap_plugin.h>


int main( int argc, char** argv )
{

  int i = 0;
  ros::init(argc, argv, "chris_white_lines_pipeline");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  // ------------------------ 
  //    LOAD RING PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points");
  out_topics.push_back("/velodyne_points/ring");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back( std::pair<std::string,std::string>("num_rings","32") );

  for( int j=0; j<32; j++ )
  {
    if(j%4==0 || j<10)
      options.push_back( std::pair<std::string,std::string>(std::string("ring_")+boost::lexical_cast<std::string>(j),"0") );
    else
      options.push_back( std::pair<std::string,std::string>(std::string("ring_")+boost::lexical_cast<std::string>(j),"1") );
  }

  usma_perception::RingFilterPlugin *rp;
  rp = new usma_perception::RingFilterPlugin();
  rp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ---------------------------------
  //    LOAD RING SHADOW POINT PLUGIN
  // ---------------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/velodyne_points/ring");
  out_topics.push_back("/velodyne_points/ring/rs");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("diff_threshold","0.2") );
  options.push_back( std::pair<std::string,std::string>("diff_2_threshold","0.1") );
  options.push_back( std::pair<std::string,std::string>("dist_gain","0.15") );
  options.push_back( std::pair<std::string,std::string>("num_rings","32") );

  usma_perception::RingShadowPointFilterPlugin *rsp;
  rsp = new usma_perception::RingShadowPointFilterPlugin();
  rsp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD TRANSFORM PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/velodyne_points/ring/rs");
  out_topics.push_back("/velodyne_points/ring/rs/tf");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("sensor_frame","velodyne"));
  options.push_back(std::pair<std::string,std::string>("goal_frame","base_link"));

  usma_perception::TransformPlugin *tfp;
  tfp = new usma_perception::TransformPlugin();
  tfp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD CROPPEr PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/velodyne_points/ring/rs/tf");
  out_topics.push_back("/velodyne_points/ring/rs/tf/crop");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("x_min","-25.0"));
  options.push_back(std::pair<std::string,std::string>("y_min","-25.0"));
  options.push_back(std::pair<std::string,std::string>("z_min","0.15"));
  options.push_back(std::pair<std::string,std::string>("x_max","25.0"));
  options.push_back(std::pair<std::string,std::string>("y_max","25.0"));
  options.push_back(std::pair<std::string,std::string>("z_max","0.75"));

  usma_perception::CropperPlugin *cp;
  cp = new usma_perception::CropperPlugin();
  cp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points/ring/rs/tf");
  out_topics.push_back("velodyne_points/ring/rs/tf/voxel");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.1") );
  options.push_back( std::pair<std::string,std::string>("y","0.1") );
  options.push_back( std::pair<std::string,std::string>("z","0.1") );

  usma_perception::VoxelPlugin *vp;
  vp = new usma_perception::VoxelPlugin();
  vp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ---------------------------
  //    LOAD ODOMETRY PLUGIN
  // ---------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points/ring/rs/tf/voxel");
  in_topics.push_back("/imu/data");
  out_topics.push_back("gicp_odom_est");
  out_topics.push_back("/velodyne_points/ring/rs/tf/voxel/odom");
  in_types.push_back("pcl::PCLPointCloud2");
  in_types.push_back("sensor_msgs::Imu");

  options.push_back(std::pair<std::string,std::string>("publish_tf","0"));

  usma_perception::GICPOdomPlugin *eop;
  eop = new usma_perception::GICPOdomPlugin();
  eop->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  
  // ------------------------ 
  //    LOAD RESIZE PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left/image_rect_color");
  out_topics.push_back("/USER_IMAGE/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("divider","2.0"));

  usma_perception::ImageResizePlugin *irp;
  irp = new usma_perception::ImageResizePlugin();
  irp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD GAUSS PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left/image_rect_color");
  out_topics.push_back("/stereo/left/gauss/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("size","5"));
  options.push_back(std::pair<std::string,std::string>("sigma","1.5"));

  usma_perception::GaussianBlurPlugin *lgbp;
  lgbp = new usma_perception::GaussianBlurPlugin();
  lgbp->initialize( in_topics, out_topics, in_types, options, std::string("left")+boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD COLOR CVT PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left//gauss/image_raw");
  out_topics.push_back("/stereo/left/gauss/bw/image_raw");
  in_types.push_back("image_transport::Camera");

  usma_perception::ColorConversionPlugin *lcvtp;
  lcvtp = new usma_perception::ColorConversionPlugin();
  lcvtp->initialize( in_topics, out_topics, in_types, options, std::string("left")+boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD THRESH PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left/gauss/bw/image_raw");
  out_topics.push_back("/stereo/left/gauss/bw/thresh/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("num_channels","1"));
  options.push_back(std::pair<std::string,std::string>("min_0","0.7"));
  options.push_back(std::pair<std::string,std::string>("max_0","1.0"));

  usma_perception::ImageThresholdPlugin *lthp;
  lthp = new usma_perception::ImageThresholdPlugin();
  lthp->initialize( in_topics, out_topics, in_types, options, std::string("left")+boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD MORPH PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left/gauss/bw/thresh/image_raw");
  out_topics.push_back("/stereo/left/gauss/bw/thresh/morph/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("kernel_size","9"));
  options.push_back(std::pair<std::string,std::string>("morph_type","open"));
  options.push_back(std::pair<std::string,std::string>("iters","2"));

  usma_perception::MorphologyPlugin *lmp;
  lmp = new usma_perception::MorphologyPlugin();
  lmp->initialize( in_topics, out_topics, in_types, options, std::string("left")+boost::lexical_cast<std::string>(i++), n );

  // -------------------------------- 
  //    LOAD Plane Projection PLUGIN
  // --------------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/left/gauss/bw/thresh/morph/image_raw");
  out_topics.push_back("/stereo/left/gauss/bw/thresh/morph/plane_proj/points");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("horizon_location","0.9"));
  options.push_back(std::pair<std::string,std::string>("ground_offset","-0.15"));

  usma_perception::PlaneProjectionPlugin *lppp;
  lppp = new usma_perception::PlaneProjectionPlugin();
  lppp->initialize( in_topics, out_topics, in_types, options, std::string("left")+boost::lexical_cast<std::string>(i++), n );

  
  // ------------------------ 
  //    LOAD GAUSS PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/right/image_rect_color");
  out_topics.push_back("/stereo/right/gauss/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("size","5"));
  options.push_back(std::pair<std::string,std::string>("sigma","1.5"));

  usma_perception::GaussianBlurPlugin *rgbp;
  rgbp = new usma_perception::GaussianBlurPlugin();
  rgbp->initialize( in_topics, out_topics, in_types, options, std::string("right")+boost::lexical_cast<std::string>(i++), n );


  // ------------------------ 
  //    LOAD COLOR CVT PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/right//gauss/image_raw");
  out_topics.push_back("/stereo/right/gauss/bw/image_raw");
  in_types.push_back("image_transport::Camera");

  usma_perception::ColorConversionPlugin *rcvtp;
  rcvtp = new usma_perception::ColorConversionPlugin();
  rcvtp->initialize( in_topics, out_topics, in_types, options, std::string("right")+boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD THRESH PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/right/gauss/bw/image_raw");
  out_topics.push_back("/stereo/right/gauss/bw/thresh/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("num_channels","1"));
  options.push_back(std::pair<std::string,std::string>("min_0","0.7"));
  options.push_back(std::pair<std::string,std::string>("max_0","1.0"));

  usma_perception::ImageThresholdPlugin *rthp;
  rthp = new usma_perception::ImageThresholdPlugin();
  rthp->initialize( in_topics, out_topics, in_types, options, std::string("right")+boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD MORPH PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/right/gauss/bw/thresh/image_raw");
  out_topics.push_back("/stereo/right/gauss/bw/thresh/morph/image_raw");
  in_types.push_back("image_transport::Camera");

  options.push_back(std::pair<std::string,std::string>("kernel_size","9"));
  options.push_back(std::pair<std::string,std::string>("morph_type","open"));
  options.push_back(std::pair<std::string,std::string>("iters","2"));

  usma_perception::MorphologyPlugin *rmp;
  rmp = new usma_perception::MorphologyPlugin();
  rmp->initialize( in_topics, out_topics, in_types, options, std::string("right")+boost::lexical_cast<std::string>(i++), n );


  // -------------------------------- 
  //    LOAD Plane Projection PLUGIN
  // --------------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back( "/stereo/right/gauss/bw/thresh/morph/image_raw");
  out_topics.push_back("/stereo/right/gauss/bw/thresh/morph/plane_proj/points");
  in_types.push_back("image_transport::Camera");
  options.push_back(std::pair<std::string,std::string>("horizon_location","0.9"));
  options.push_back(std::pair<std::string,std::string>("ground_offset","-0.15"));

  usma_perception::PlaneProjectionPlugin *rppp;
  rppp = new usma_perception::PlaneProjectionPlugin();
  rppp->initialize( in_topics, out_topics, in_types, options, std::string("right")+boost::lexical_cast<std::string>(i++), n );

  // -------------------------------- 
  //    LOAD MCA PLUGIN
  // --------------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/velodyne_points/ring/rs/tf/crop");
  in_topics.push_back("/stereo/right/gauss/bw/thresh/morph/plane_proj/points");
  in_topics.push_back("/stereo/left/gauss/bw/thresh/morph/plane_proj/points");
  out_topics.push_back("/obstacles_assembled");
  in_types.push_back("pcl::PCLPointCloud2");
  in_types.push_back("pcl::PCLPointCloud2");
  in_types.push_back("pcl::PCLPointCloud2");

  options.push_back(std::pair<std::string,std::string>("num_in","3"));

  usma_perception::MCAssemblerPlugin *mcap;
  mcap = new usma_perception::MCAssemblerPlugin();
  mcap->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD VOXEL PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/obstacles_assembled");
  out_topics.push_back("/obstacles_assembled/voxel");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("x","0.1") );
  options.push_back( std::pair<std::string,std::string>("y","0.1") );
  options.push_back( std::pair<std::string,std::string>("z","0.1") );

  usma_perception::VoxelPlugin *vp2;
  vp2 = new usma_perception::VoxelPlugin();
  vp2->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // ------------------------ 
  //    LOAD RADIUS PLUGIN
  // ------------------------

  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("/obstacles_assembled/voxel");
  out_topics.push_back("/obstacles_assembled/voxel/rad");
  in_types.push_back("pcl::PCLPointCloud2");
  options.push_back( std::pair<std::string,std::string>("search_radius","0.1") );
  options.push_back( std::pair<std::string,std::string>("min_pts","5") );

  usma_perception::RadiusOutlierFilterPlugin *rfp;
  rfp = new usma_perception::RadiusOutlierFilterPlugin();
  rfp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i++), n );

  // Start the ros spinner and wait for shutdown
  ros::AsyncSpinner spinner(8); // Use 8 threads
  spinner.start();
  ros::waitForShutdown();

  return 0;
}
