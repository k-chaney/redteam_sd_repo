#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>

// generators
#include <usma_perception_plugins/point_cloud_generators/object_over_plane_gen_plugin.h>

// point cloud filters and tools
#include <usma_perception_plugins/point_clouds/assembler_plugin.h>
#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/freq_divider_plugin.h>
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>
#include <usma_perception_plugins/point_clouds/radius_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/random_sampler_plugin.h>
#include <usma_perception_plugins/point_clouds/statistical_outlier_filter_plugin.h>
#include <usma_perception_plugins/point_clouds/voxel_plugin.h>


int main( int argc, char** argv )
{
  ros::init(argc, argv, "point_cloud_filters_test");

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;

  /////////////////////// DATA SOURCE
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  out_topics.push_back("in");

  usma_perception::OOPPCGen *data_source;
  data_source = new usma_perception::OOPPCGen;
  data_source->initialize( in_topics, out_topics, in_types, options, n );
  /////////////////////// DATA SOURCE
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/1");

  usma_perception::AssemblerPlugin *p1;
  p1 = new usma_perception::AssemblerPlugin;
  p1->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/2");

  usma_perception::CropperPlugin *p2;
  p2 = new usma_perception::CropperPlugin;
  p2->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/3");

  usma_perception::FreqDividerPlugin *p3;
  p3 = new usma_perception::FreqDividerPlugin;
  p3->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/4");

  usma_perception::RadiusOutlierFilterPlugin *p4;
  p4 = new usma_perception::RadiusOutlierFilterPlugin;
  p4->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/5");

  usma_perception::VoxelPlugin *p5;
  p5 = new usma_perception::VoxelPlugin;
  p5->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/6");

  usma_perception::StatisticalOutlierFilterPlugin *p6;
  p6 = new usma_perception::StatisticalOutlierFilterPlugin;
  p6->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
  in_topics.clear();
  in_types.clear();
  out_topics.clear();
  options.clear();

  in_topics.push_back("in");
  in_types.push_back("pcl::PCLPointCloud2");
  out_topics.push_back("in/7");

  usma_perception::RandomSamplerPlugin *p7;
  p7 = new usma_perception::RandomSamplerPlugin;
  p7->initialize( in_topics, out_topics, in_types, options, n );
//////////////////////////////////////////////////////////////////////////


  ros::AsyncSpinner spinner(8);
  spinner.start();

  ros::Rate r(1.0);
  int i = 5;
  while ( ros::ok() && (i-- > 0) )
    r.sleep();
  
  spinner.stop();
  ros::shutdown();

  p1->stop();
  p2->stop();
  p3->stop();
  p4->stop();
  p5->stop();
  p6->stop();
  p7->stop();

  delete p1;
  delete p2;
  delete p3;
  delete p4;
  delete p5;
  delete p6;
  delete p7;

  return 0;
}
