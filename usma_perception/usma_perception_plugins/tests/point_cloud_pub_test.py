#!/usr/bin/env python
import rospy
import sensor_msgs.point_cloud2 as pc2
from sensor_msgs.msg import PointCloud2, PointField

#talktest
def test():
    rospy.init_node('talktest', anonymous=True)
    pub_cloud = rospy.Publisher("/in", PointCloud2)
    cloud_plane = [ [x/10.0,y/10.0,0] for x in range(0,10) for y in range(0,10) ]
    while not rospy.is_shutdown():
        pcloud = PointCloud2()
        # make point cloud
        pcloud = pc2.create_cloud_xyz32(pcloud.header, cloud_plane)
        pcloud.header.frame_id = "map"
        pub_cloud.publish(pcloud)
        rospy.sleep(10.0)

if __name__ == '__main__':
    try:
        test()
    except rospy.ROSInterruptException:
        pass
