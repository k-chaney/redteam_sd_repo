#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <pluginlib/class_loader.h>
#include <usma_perception_plugins/perception_plugin_base.h>

int main( int argc, char** argv )
{
  ros::init(argc, argv, "point_cloud_filters_test");

  ros::NodeHandle n;

  pluginlib::ClassLoader<usma_perception::PerceptionPluginBase> plugin_loader_("usma_perception_plugins", "usma_perception::PerceptionPluginBase");

  std::vector< std::string > available_classes = plugin_loader_.getDeclaredClasses();

  int return_status = 0;

  for(int i = 0; i < available_classes.size(); i++)
  {
    if(plugin_loader_.isClassAvailable(available_classes[i]) && !plugin_loader_.isClassLoaded(available_classes[i]))
    {
      plugin_loader_.loadLibraryForClass(available_classes[i]);
    }
    if(plugin_loader_.isClassAvailable(available_classes[i]) && !plugin_loader_.isClassLoaded(available_classes[i]))
    {
      ROS_ERROR( "[%s]:::%sloaded::%savailable", available_classes[i].c_str(), (plugin_loader_.isClassLoaded( available_classes[i]) ? "" : "not "), (plugin_loader_.isClassAvailable( available_classes[i]) ? "" : "not ") );
      return_status = 1;
    }
    else
    {
      ROS_INFO( "[%s]:::%sloaded::%savailable", available_classes[i].c_str(), (plugin_loader_.isClassLoaded( available_classes[i]) ? "" : "not "), (plugin_loader_.isClassAvailable( available_classes[i]) ? "" : "not ") );
    }


  }

  return return_status;
}
