#include <ros/ros.h>
#include <ros/callback_queue.h>

#define ALL_SUPPORT

#include <usma_perception_plugins/perception_plugin_base.h>
#include <usma_perception_plugins/point_clouds/cropper_plugin.h>
#include <usma_perception_plugins/point_clouds/rtt_check_plugin.h>

int main( int argc, char** argv )
{
  ros::init(argc, argv, (std::string("static_perception_pipeline")+std::string(argv[1])+std::string(argv[2])).c_str());

  ros::NodeHandle n;
  std::vector< std::string > in_topics;
  std::vector< std::string > in_types;
  std::vector< std::string > out_topics;
  std::vector< std::pair<std::string, std::string> > options;
  // Use same options for all
  options.clear();
  options.push_back( std::pair<std::string,std::string>("x_min","-10.0") );
  options.push_back( std::pair<std::string,std::string>("y_min","-10.0") );
  options.push_back( std::pair<std::string,std::string>("z_min","-10.0") );
  options.push_back( std::pair<std::string,std::string>("x_max","10.0") );
  options.push_back( std::pair<std::string,std::string>("y_max","10.0") );
  options.push_back( std::pair<std::string,std::string>("z_max","10.0") );
  in_types.clear();
  in_types.push_back("pcl::PCLPointCloud2");

  int START_NODE_NUM = atoi(argv[1]);
  int END_NODE_NUM = atoi(argv[2]);
  int NUM_NODES = END_NODE_NUM - START_NODE_NUM;

  std::vector<usma_perception::CropperPlugin*> cp;
  cp.resize(NUM_NODES);

  for( int i = START_NODE_NUM; i < END_NODE_NUM; i++ )
  {
    in_topics.clear();
    out_topics.clear();

    in_topics.push_back(std::string("crop") + boost::lexical_cast<std::string>(i) );
    out_topics.push_back(std::string("crop") + boost::lexical_cast<std::string>(i+1) );

    cp[i] = new usma_perception::CropperPlugin();
    cp[i]->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(i), n );
  }

  usma_perception::RTTCheckPlugin* rttcp;

  int RTT_SUB_NUM = atoi(argv[3]);
  int RTT_PUB_NUM = atoi(argv[4]);
  

  if( RTT_SUB_NUM != RTT_PUB_NUM )
  {
    options.clear();
    in_topics.clear();
    out_topics.clear();

    in_types.clear();
    in_types.push_back("pcl::PCLPointCloud2");
    in_types.push_back("pcl::PCLPointCloud2");

    in_topics.push_back("/velodyne_points");
    in_topics.push_back(std::string("crop") + boost::lexical_cast<std::string>(RTT_SUB_NUM) );
    out_topics.push_back(std::string("crop") + boost::lexical_cast<std::string>(RTT_PUB_NUM) );

    ROS_INFO("%s", in_topics[0].c_str());
    ROS_INFO("%s", in_topics[1].c_str());
    ROS_INFO("%s", out_topics[0].c_str());

    rttcp = new usma_perception::RTTCheckPlugin();
    rttcp->initialize( in_topics, out_topics, in_types, options, boost::lexical_cast<std::string>(END_NODE_NUM+10), n );
  }

  // Start the ros spinner and wait for shutdown
  ros::AsyncSpinner spinner(NUM_NODES); // Use 8 threads
  spinner.start();
  ros::waitForShutdown();

  return 0;
}
