#ifndef PERCEPTION_PLUGIN_BASE_H_
#define PERCEPTION_PLUGIN_BASE_H_

#include <vector>
#include <string>
#include <typeinfo>

#include <boost/thread.hpp>
#include <boost/any.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>
#include <boost/lexical_cast.hpp>

#include <ros/ros.h>
#include <std_msgs/String.h>

#if defined(PC_SUPPORT) || defined(ALL_SUPPORT)
#include <sensor_msgs/PointCloud2.h>
#include <pcl_conversions/pcl_conversions.h>
#endif

#if defined(NAV_SUPPORT) || defined(ALL_SUPPORT)
#include <nav_msgs/Odometry.h>
#include <sensor_msgs/Imu.h>
#include <usma_perception_msgs/FeatureArray.h>
#endif

#if defined(OCTOMAP_SUPPORT) || defined(ALL_SUPPORT)
#include <octomap_msgs/Octomap.h>
#endif

#if defined(MARKER_VIS_SUPPORT) || defined(ALL_SUPPORT)
#include <visualization_msgs/MarkerArray.h>
#include <std_msgs/ColorRGBA.h>
#endif

#if defined(BOUNDING_BOX_SUPPORT) || defined(ALL_SUPPORT)
#include <usma_perception_msgs/TrackedBoundingBoxArray.h>
#endif

#if defined(IMAGE_TRANSPORT_SUPPORT) || defined(ALL_SUPPORT)
#include <usma_perception_plugins/managed_camera_subscriber.h>
#include <image_transport/camera_publisher.h>
#endif

#include <usma_perception_plugins/managed_subscriber.h>
#include <usma_perception_msgs/DynamicOptionsService.h>

/** A convinence error statement that can be placed anywhere to produce the filename, function, and line number in the ROS_ERROR stream */
#define FFL_ERROR ROS_ERROR("%s::%s::%d", __FILE__, __func__, __LINE__)

namespace usma_perception
{
  // the base of the plugin that all perception plugins inherit -- supplies infrastructure for starting a plugin up
  class PerceptionPluginBase
  {
    public:

      /**
        * /brief Initializes the plugin after it was constructed
        *
        * /param in_topic_names The input topic names that are being published to
        * /param out_topic_names The output topic names that are being published to
        * /param in_msg_types The msg types that are being input into this plugin (provided to error check)
        * /param options Option pair vector should contain pairs of names and values as strings
        * /param n NodeHandle that is used to create all ros communication pieces
        *
        * /return Returns if the initialization was successful
        *
        */
      bool initialize( std::vector<std::string> &in_topic_names, std::vector<std::string> &out_topic_names, std::vector<std::string> &in_msg_types, std::vector< std::pair<std::string, std::string> > &options, ros::NodeHandle &n )
      {

        return initialize( in_topic_names, out_topic_names, in_msg_types, options, boost::lexical_cast<std::string>(rand()), n);

      }

      /**
        * /brief Initializes the plugin after it was constructed
        *
        * /param in_topic_names The input topic names that are being published to
        * /param out_topic_names The output topic names that are being published to
        * /param in_msg_types The msg types that are being input into this plugin (provided to error check)
        * /param options Option pair vector should contain pairs of names and values as strings
        * /param unique_id Unique id for this specific module
        * /param n NodeHandle that is used to create all ros communication pieces
        *
        * /return Returns if the initialization was successful
        *
        * Verifys that the all of the ins and outs match up between what the higher level is expecting and what the module needs/produces.
        * After it is verifyed, the subscribers and publishers are generated and stored.
        * The worker thread is started and off we go.
        *
        */
      bool initialize( std::vector<std::string> &in_topic_names, std::vector<std::string> &out_topic_names, std::vector<std::string> &in_msg_types, std::vector< std::pair<std::string, std::string> > &options, std::string unique_id, ros::NodeHandle &n )
      {
        // Provides options immediately. This means number of inputs can be set at runtime
        for( int i=0; i<options.size(); i++ )
          option_pairs_[options[i].first] = options[i].second;
        new_options_ready_ = true;

        if( !io_types_set_ )
        {
          setPluginIOTypes( in_types_, out_types_, short_name_ );
          io_types_set_ = true;
        }
        // verify that the requested IO types and topics make sense
        if( in_topic_names.size() != in_msg_types.size() )
        { 
          ROS_ERROR( "[%s PLUGIN]::PLUGIN INIT FAILED--MISMATCHED INPUT", getShortName().c_str() ); 
          return false;
        }
        if( out_topic_names.size() != out_types_.size() )
        {
          ROS_ERROR( "[%s PLUGIN]::PLUGIN INIT FAILED--MISMATCHED OUTPUT", getShortName().c_str() ); 
          return false;
        }
        if( !verifyPluginIO( in_msg_types ) )
        {
          ROS_ERROR( "[%s PLUGIN]::PLUGIN INIT FAILED--IO VERIFICATION FAILED", getShortName().c_str() ); 
          return false;
        }

        unique_id_ = unique_id;

        // generate the new condition variable before sending to the subscribers
        calc_spike_.reset( new boost::condition_variable() );

        nh_ = n;

        in_topics_ = in_topic_names;
        out_topics_ = out_topic_names;

        genSubscribers( in_topic_names, n );
        genPublishers( out_topic_names, n );
        dynamic_config_srv_.reset( new ros::ServiceServer(n.advertiseService(this->getShortName(), &PerceptionPluginBase::dynamicOptionsSrvCallback, this)));

        worker_ = boost::thread( &PerceptionPluginBase::workerThread, this );

        ROS_INFO( "[%s PLUGIN]::Started Successfully", getShortName().c_str() );

        return true;
      }

      /**
        * /brief A getter for the shortname of the plugin, makes sure the ioTypes are actually setup before returning.
        */
      std::string getShortName() 
      { 
        if( !io_types_set_ )
        {
          setPluginIOTypes( in_types_, out_types_, short_name_ );
          io_types_set_ = true;
        }
        return std::string(short_name_+"_"+unique_id_); 
      }

      /**
        * /brief A getter for the input types of the plugin, makes sure the ioTypes are actually setup before returning.
        */
      std::vector<std::string> getInTypes() 
      { 
        if( !io_types_set_ )
        {
          setPluginIOTypes( in_types_, out_types_, short_name_ );
          io_types_set_ = true;
        }
        return in_types_; 
      }

      /**
        * /brief A getter for the output types of the plugin, makes sure the ioTypes are actually setup before returning.
        */
      std::vector<std::string> getOutTypes() 
      { 
        if( !io_types_set_ )
        {
          setPluginIOTypes( in_types_, out_types_, short_name_ );
          io_types_set_ = true;
        }
        return out_types_; 
      }

      /**
        * /brief A getter for the input topic names
        */
      std::vector<std::string> getInputTopicNames()
      {
        return in_topics_;
      }

      /**
        * /brief A getter for the output topic names
        */
      std::vector<std::string> getOutputTopicNames()
      {
        return out_topics_;
      }

      /**
        * /brief A getter for the time it takes to process a single cycle
        */
      double getProcessingTime() 
      {
        return processing_time_;
      }

      /**
        * /brief This is a blocking function that will stop the node and prep for deconstruction
        */
      void stop()
      { 
        // notify the thread to continue and then lock the mutex
        is_alive_ = false;
        calc_spike_->notify_one();
        alive_.lock();
        alive_.unlock();
      };

      /**
        * /brief The destructor
        */
      ~PerceptionPluginBase()
      { 
        if(is_alive_ == true)
          stop();

        worker_.join(); 
      };

    protected:
      /**
        * /brief The constructor makes sure io_types_set_ is false
        */
      PerceptionPluginBase() : io_types_set_(false) {};

      ros::NodeHandle nh_;
      // TODO :: work this into a boost any and condense the image_pubs_ with this -- samething with image_subs_ -- this way for consistency
      std::vector< boost::any > pubs_; ///< Vector of publishers used for publishing the requested topics
      std::vector< boost::any > subs_; ///< Vector of boost::shared_ptrs<ManagedSubscribers<T> > stored as boost:any because of the templating required for that class. Should be cast using boost::any_cast

      boost::shared_ptr<ros::ServiceServer> dynamic_config_srv_;
      boost::mutex ss_mtx_; ///< Mutex to create lock for the condition variable
      boost::shared_ptr<boost::condition_variable> calc_spike_; ///< Condition varialbe to wait on for executing the workload
      bool new_options_ready_;
      boost::mutex options_mtx_;

      // have the plugin load it's requested types to check against
      virtual void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn) = 0;

      /** 
        * /brief Allow for the plugin to determine if the worker should run
        */
      virtual bool checkInputReady() = 0;

      /**
        * /brief The init method runs at the start of the worker thread. Used to set everything up before the start of the main loop.
        */
      virtual void init() = 0;

      /**
        * /brief Where all of the work should happen.
        */
      virtual void workLoad() = 0;

      /**
        * /brief Function to get options out of list of options provided.
        *
        * /param name The name of the option that is being requested
        * /param default_value The default value if the option doesn't exist. The typename comes from this parameter.
        *
        * /return Returns the properly cast option value that has been requested if it exists as an option that was passed in. Otherwise, it returns the default value given.
        *
        */
      template<typename T>
        T getOption(std::string name, T default_value)
        {
          option_name_type_pairings_[name] = typeid(default_value).name();
          std::map<std::string, std::string>::iterator it;
          it = option_pairs_.find(name);
          try
          {
            if(it != option_pairs_.end())
                return boost::lexical_cast<T>(option_pairs_[name]);
            option_pairs_[name]=boost::lexical_cast<std::string>(default_value);
            return default_value;
          }
          catch( const boost::bad_lexical_cast &ex )
          {
            ROS_ERROR("[%s]::Bad option -- boost::lexical_cast<%s>(%s)", getShortName().c_str(), typeid(default_value).name(), option_pairs_[name].c_str() );
            return default_value;
          }
        }

    private:
      boost::thread worker_; ///< The worker thread for the plugin
      std::string unique_id_; ///< A unique id to represent a specific module in naming schemes
      std::string short_name_; ///< The name used by the module for inteligent naming of topics
      std::vector< std::string > in_types_; ///< The input types needed by the module
      std::vector< std::string > in_topics_; ///< The input topics subscribed to
      std::vector< std::string > out_types_; ///< The output types needed by the module
      std::vector< std::string > out_topics_; ///< The output topics published to
      std::map<std::string, std::string> option_pairs_; ///< The options being passed into the 
      std::map<std::string, std::string> option_name_type_pairings_; ///< The set of option names and pairs that have been setup by the plugin
      bool io_types_set_; ///< Flag to check if in_tyeps_ and out_types are set

      double processing_time_; ///< Processing time per cycle

      boost::mutex alive_;
      bool is_alive_;

      /**
        * /brief The main loop of each plugin. Contains the logic for the condition variable and calls virtual functions defined further down.
        */
      void workerThread()
      {
        processing_time_ = 0.0;
        init();

        is_alive_ = true;

        while( is_alive_ && ros::ok() )
        {
          if( alive_.try_lock() )
          {
            boost::unique_lock<boost::mutex> l(ss_mtx_);
            while( is_alive_ && ros::ok() && !checkInputReady() )
            {
              calc_spike_->wait(l);
            }

            if( ros::ok() )
            {
              ros::Time t1 = ros::Time::now();
              workLoad();
              ros::Time t2 = ros::Time::now();

              processing_time_ = 0.875 * ( processing_time_ ) + 0.275 * ( (t2-t1).toSec() );
            }
            alive_.unlock();
          }
        }
      }

      /**
        * /brief generate the subscribers based on the options requested
        *
        * /param topic_names The names of the topics that you want to subscribe to.
        * /param n The NodeHandle that's being used to subscribe to things.
        *
        * Creates ManagedSubscribers that hold internal callbacks to manage the data that's being passed in.
        *
        */
      void genSubscribers( std::vector<std::string> &topic_names, ros::NodeHandle &n ) 
      {
        for( int i = 0; i < topic_names.size(); i++ )
        {
          if ( in_types_[i] == std::string( "std_msgs::String" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<std_msgs::String> > s;
            s.reset( new ManagedSubscriber<std_msgs::String>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          } 
#if defined(PC_SUPPORT) || defined(ALL_SUPPORT)
          else if ( in_types_[i] == std::string( "sensor_msgs::PointCloud2" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > s;
            s.reset( new ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          } 
          else if ( in_types_[i] == std::string( "pcl::PCLPointCloud2" ) )
          {
            boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > s;
            s.reset( new ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          } 
#endif
#if defined(NAV_SUPPORT) || defined(ALL_SUPPORT)
          else if ( in_types_[i] == std::string( "nav_msgs::Odometry" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<nav_msgs::Odometry::ConstPtr> > s;
            s.reset( new ManagedSubscriber<nav_msgs::Odometry::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          } 
          else if ( in_types_[i] == std::string( "sensor_msgs::Imu" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > s;
            s.reset( new ManagedSubscriber<sensor_msgs::Imu::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          }
          else if ( in_types_[i] == std::string( "usma_perception_msgs::FeatureArray" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::FeatureArray::ConstPtr> > s;
            s.reset( new ManagedSubscriber<usma_perception_msgs::FeatureArray::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          }
#endif
#if defined(OCTOMAP_SUPPORT) || defined(ALL_SUPPORT)
          else if ( in_types_[i] == std::string( "octomap_msgs::Octomap" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<octomap_msgs::Octomap::ConstPtr> > s;
            s.reset( new ManagedSubscriber<octomap_msgs::Octomap::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          }
#endif
#if defined(BOUNDING_BOX_SUPPORT) || defined(ALL_SUPPORT)
          else if ( in_types_[i] == std::string( "usma_perception_msgs::TrackedBoundingBoxArray" ) ) 
          {
            boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::TrackedBoundingBoxArray::ConstPtr> > s;
            s.reset( new ManagedSubscriber<usma_perception_msgs::TrackedBoundingBoxArray::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          }
#endif
#if defined(IMAGE_TRANSPORT_SUPPORT) || defined(ALL_SUPPORT)
          else if ( in_types_[i] == std::string( "image_transport::Camera" ) ) 
          {
            boost::shared_ptr<ManagedCameraSubscriber> s;
            s.reset( new ManagedCameraSubscriber( in_types_[i], topic_names[i], n, calc_spike_ ) );
            subs_.push_back( s );
          }
#endif

         
//          BLANK STUB           
//          else if ( in_types_[i] == std::string( "" ) ) {
//            boost::shared_ptr<ManagedSubscriber<::ConstPtr> > s;
//            s.reset( new ManagedSubscriber<::ConstPtr>( in_types_[i], topic_names[i], n, calc_spike_ ) );
//            subs_.push_back( s );
//          }
        }
      }

      /**
        * /brief Generate the publishers based on the options requested
        *
        * /param topic_names The names of the topics that you want to subscribe to.
        * /param n The NodeHandle that's being used to subscribe to things.
        *
        */
      void genPublishers( std::vector<std::string> &topic_names, ros::NodeHandle &n ) 
      {
        for( int i = 0; i < topic_names.size(); i++ )
        {
          boost::shared_ptr<ros::Publisher> p;
          if( out_types_[i] == std::string( "std_msgs::String" ) )
            p.reset( new ros::Publisher( n.advertise<std_msgs::String>(topic_names[i], 10) ) );
#if defined(PC_SUPPORT) || defined(ALL_SUPPORT)
          else if( out_types_[i] == std::string( "sensor_msgs::PointCloud2" ) )
            p.reset( new ros::Publisher( n.advertise<sensor_msgs::PointCloud2>(topic_names[i], 10) ) );
          else if( out_types_[i] == std::string( "pcl::PCLPointCloud2" ) )
            p.reset( new ros::Publisher( n.advertise<pcl::PCLPointCloud2>(topic_names[i], 10) ) );
#endif
#if defined(NAV_SUPPORT) || defined(ALL_SUPPORT)
          else if( out_types_[i] == std::string( "nav_msgs::Odometry" ) )
            p.reset( new ros::Publisher( n.advertise<nav_msgs::Odometry>(topic_names[i], 10) ) );
          else if( out_types_[i] == std::string( "sensor_msgs::Imu" ) )
            p.reset( new ros::Publisher( n.advertise<sensor_msgs::Imu>(topic_names[i], 10) ) );
          else if( out_types_[i] == std::string( "usma_perception_msgs::FeatureArray" ) )
            p.reset( new ros::Publisher( n.advertise<usma_perception_msgs::FeatureArray>(topic_names[i], 10) ) );
#endif
#if defined(OCTOMAP_SUPPORT) || defined(ALL_SUPPORT)
          else if( out_types_[i] == std::string( "octomap_msgs::Octomap" ) )
            p.reset( new ros::Publisher( n.advertise<octomap_msgs::Octomap>(topic_names[i], 10) ) );
#endif
#if defined(MARKER_VIS_SUPPORT) || defined(ALL_SUPPORT)
          else if( out_types_[i] == std::string( "visualization_msgs::MarkerArray" ) )
            p.reset( new ros::Publisher( n.advertise<visualization_msgs::MarkerArray>(topic_names[i], 10) ) );
#endif
#if defined(BOUNDING_BOX_SUPPORT) || defined(ALL_SUPPORT)
          else if( out_types_[i] == std::string( "usma_perception_msgs::TrackedBoundingBoxArray" ) )
            p.reset( new ros::Publisher( n.advertise<usma_perception_msgs::TrackedBoundingBoxArray>(topic_names[i], 10) ) );
#endif
#if defined(IMAGE_TRANSPORT_SUPPORT) || defined(ALL_SUPPORT)
          else if ( out_types_[i] == std::string( "image_transport::Camera" ) ) 
          {
            boost::shared_ptr<image_transport::CameraPublisher> itp;
            image_transport::ImageTransport it( n );
            itp.reset( new image_transport::CameraPublisher( it.advertiseCamera( topic_names[i], 10 ) ) );
            pubs_.push_back( itp );
            
            continue;
          }
#endif


//        BLANK STUB -- delete comments and put new type in
//          else if( out_types_[i] == std::string( "" ) )
//            p.reset( new ros::Publisher( n.advertise<>(topic_names[i], 10) ) );

          pubs_.push_back( p );
        }
      }

      /**
        * /brief Verifies that the given inputs are what is actually needed by the plugin
        *
        * /param in The input types that will be subscribed to.
        *
        */
      bool verifyPluginIO( std::vector<std::string> in ){
        if( !( in.size() == in_types_.size()) )
          return false;

        for( int i = 0; i < in.size(); i++ )
        {
          if( !( in[i] == in_types_[i] ) )
            return false;
        }

        return true;
      }

      /**
        * /brief Verifies that the given inputs are what is actually needed by the plugin
        *
        * /param req The request being sent in--contains options to be written into the plugin
        * /param res The response being sent out--contains the options that the node is looking for and current values
        *
        */
      bool dynamicOptionsSrvCallback(usma_perception_msgs::DynamicOptionsService::Request &req,
                                     usma_perception_msgs::DynamicOptionsService::Response &res)
      {
        options_mtx_.lock();
        for( int i=0; i<req.input_pairs.size(); i++)
        {
          option_pairs_[req.input_pairs[i].option_name]=req.input_pairs[i].option_value;
        }
        options_mtx_.unlock();
        new_options_ready_ = true;
        for (std::map<std::string,std::string>::iterator it=option_name_type_pairings_.begin(); it!=option_name_type_pairings_.end(); ++it)
        {
          usma_perception_msgs::OptionTypeSet tmp;
          tmp.option_name = it->first;
          tmp.option_type = it->second;
          tmp.option_value = option_pairs_[it->first];
          res.output_set.push_back(tmp);
        }
        return true;
      }

  };
};
#endif
