#ifndef ETHZ_ICP_MAPPING_PLUGIN_H_
#define ETHZ_ICP_MAPPING_PLUGIN_H_

#include <fstream>

#include <boost/version.hpp>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <ros/console.h>
#include <pointmatcher/PointMatcher.h>
#include <pointmatcher/Timer.h>

#include <pointmatcher_ros/point_cloud.h>
#include <pointmatcher_ros/transform.h>
#include <pointmatcher_ros/ros_logger.h>

#include <tf_conversions/tf_eigen.h>
#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

// ROS Pieces
#include <sensor_msgs/PointCloud2.h>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Geometry>

// include the plugin base
#define PC_SUPPORT
#define NAV_SUPPORT
#include <usma_perception_plugins/perception_plugin_base.h>


namespace usma_perception
{
  using namespace PointMatcherSupport;

  class ethzMappingPlugin : public PerceptionPluginBase
  {
    public:

      // Creates the point cloud generator
      ethzMappingPlugin() { }

      // Destroys the point cloud generator
      ~ethzMappingPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
       * \param it The in types that are passed back up for wanted subscribers in the order given
       * \param ot The out types that are passed back up for wanted publishers in the order given
       * \param sn The shortname that is passed back for naming purposes
       *
       * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
       *
       */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
       * \return A single bool determining whether or not to process the inputs.
       */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void processOptions();

      typedef PointMatcher<float> PM;
      typedef PM::DataPoints DP;

      void processCloud(boost::shared_ptr<DP> cloud, const std::string& scannerFrame, const ros::Time& stamp, uint32_t seq);
      void setMap(boost::shared_ptr<DP> newPointCloud);
      boost::shared_ptr<DP> updateMap(boost::shared_ptr<DP> newPointCloud, const PM::TransformationParameters Ticp, bool updateExisting);

      boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > in_cloud_sub_;
      boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > in_map_sub_;
      boost::shared_ptr<ManagedSubscriber<nav_msgs::Odometry::ConstPtr> > odom_sub_;

      boost::shared_ptr<ros::Publisher> map_pub_;
      boost::shared_ptr<ros::Publisher> odom_pub_;
      boost::shared_ptr<ros::Publisher> odom_error_pub_;

      PM::DataPointsFilters inputFilters;
      boost::shared_ptr<PM::DataPoints> mapPointCloud;
      PM::ICPSequence icp;
      boost::shared_ptr<PM::Transformation> transformation;

      // Parameters
      bool localizing;
      bool mapping;
      int min_reading_point_count_;
      int min_map_point_count_;
      double min_overlap_;
      double max_overlap_to_merge_;
      std::string odom_frame_;
      std::string map_frame_;

      PM::TransformationParameters TOdomToMap;

      boost::shared_ptr<tf::TransformListener> tf_listener_;
      boost::shared_ptr<tf::TransformBroadcaster> tf_broadcaster_;

      tf::Transform map_tf_;
      ros::Time map_time_;
      boost::mutex map_tf_lock_;
      boost::shared_ptr<boost::thread> tf_pub_thread_;
      void tfPublishThread();
  };
}
#endif
