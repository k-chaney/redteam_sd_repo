#ifndef FREQ_DIVIDER_PLUGIN_H_
#define FREQ_DIVIDER_PLUGIN_H_

// include the point cloud plugin base
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

namespace usma_perception
{
  class FreqDividerPlugin : public PointCloudPluginBase
  {
    public:
 
      // Creates the voxel plugin
      FreqDividerPlugin() { }

      // Destroys the voxel plugin
      ~FreqDividerPlugin() { }     
      
      /** \brief Sets the number of point cloud ins and outs are needed
        * \param in passes back the number of point cloud inputs this plugin has
        * \param out passes back the number of point cloud outputs this plugin has
        */
      void getNumInOut( int &in, int &out );

      /** \brief Sets the shortname of the plugin
        * \return Returns back the name that is desired.
        *
        * Provides a method to give a shorter name than the classname to help with identification.
        *
        */
      std::string setShortName();


      /** \brief Provides the methodology to tell if the data should be processed.
        * \param has_new_cloud_data Tells whether the subscribers in the plugin have new data or still have old data
        * \return Single bool determining whether or not to process the inputs.
        *
        * Provides a method to give a shorter name than the classname to help with identification.
        *
        */
      bool isInputReady( std::vector<bool> has_new_cloud_data );


    private:
      /** \brief Processes the options given to the plugin.
        * \param options The parameters given to the module for run time configuration.
        *
        * This method is responsible for pulling in the options that will ultimately be used in the plugin. Convinience functions given in the PerceptionPluginBase class allow for an easy fetch of this data.
        *
        * This method is guaranteed to be run before the worker loop starts.
        *
        * This includes freq_divider_.
        *
        */
      void processOptions();

      /** \brief This is the actual work horse of the class.
        * \param in A vector of const shared ptrs that are fresh for each loop but are safe to store for later.
        * \param out A vector of shared ptrs that are fresh and already reset for each loop.
        * \param should_publish A vector of bools saying if a particular point cloud should publish after you're done processing.
        *
        * This function actually downsamples the data to the desired spec with two calls into the VoxelGrid object.
        *
        */
      void processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< boost::shared_ptr< pcl::PCLPointCloud2 > > &out, std::vector<bool> &should_publish );

      int freq_divider_; ///< The amount to divide the frequency by
      int count_;

  };
}; // end usma_perception namespace

#endif
