#ifndef LEVENBERG_ICP_ODOM_PLUGIN_H_
#define LEVENBERG_ICP_ODOM_PLUGIN_H_

#include <fstream>

#include <boost/thread.hpp>

#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_listener.h>
#include <eigen_conversions/eigen_msg.h>

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/registration/icp_nl.h>
#include <pcl_ros/transforms.h>


// include the plugin base
#define PC_SUPPORT
#define NAV_SUPPORT
#include <usma_perception_plugins/perception_plugin_base.h>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace usma_perception
{
  class LevenbergMarquardtOdomPlugin : public PerceptionPluginBase
  {
    public:

      // Creates the point cloud generator
      LevenbergMarquardtOdomPlugin() { }

      // Destroys the point cloud generator
      ~LevenbergMarquardtOdomPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
       * \param it The in types that are passed back up for wanted subscribers in the order given
       * \param ot The out types that are passed back up for wanted publishers in the order given
       * \param sn The shortname that is passed back for naming purposes
       *
       * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
       *
       */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
       * \return A single bool determining whether or not to process the inputs.
       */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void processOptions();

      boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > in_new_cloud_sub_;
      boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > imu_sub_;

      boost::shared_ptr<ros::Publisher> odom_pub_;
      nav_msgs::Odometry cur_odom_;

      boost::shared_ptr<ros::Publisher> pc_pub_;

      boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > ref_cloud_;
      Eigen::Quaternion<float> ref_cloud_orientation_;

      pcl::IterativeClosestPointNonLinear<pcl::PointXYZ, pcl::PointXYZ> icp_;
      Eigen::Matrix4f odom_tf_;
      boost::mutex odom_tf_lock_;
      boost::shared_ptr<boost::thread> tf_pub_thread_;
      void tfPublishThread();

      boost::shared_ptr<tf::TransformBroadcaster> tf_broadcaster_;

      // Parameters
      std::string base_link_frame_;
      std::string odom_frame_;

      bool refineTransform(boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > new_cloud, boost::shared_ptr<pcl::PointCloud<pcl::PointXYZ> > ref_cloud, Eigen::Matrix4f &refined_tf );

      tf::Transform getTFfromEigen( Eigen::Matrix4f &e )
      {
       tf::Transform t;
       t.setOrigin(tf::Vector3( e.matrix()(0,3), e.matrix()(1,3), e.matrix()(2,3)));
       t.setBasis(tf::Matrix3x3(e.matrix()(0,0), e.matrix()(0,1), e.matrix()(0,2),
                                e.matrix()(1,0), e.matrix()(1,1), e.matrix()(1,2),
                                e.matrix()(2,0), e.matrix()(2,1), e.matrix()(2,2)));
       return t;
      };
  };
}
#endif
