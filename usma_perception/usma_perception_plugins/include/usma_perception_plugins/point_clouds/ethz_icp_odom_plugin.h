#ifndef ETHZ_ICP_ODOM_PLUGIN_H_
#define ETHZ_ICP_ODOM_PLUGIN_H_

#include <fstream>

#include <boost/version.hpp>
#include <boost/thread.hpp>

#include <ros/ros.h>
#include <ros/console.h>
#include <pointmatcher/PointMatcher.h>
#include <pointmatcher/Timer.h>

#include <pointmatcher_ros/point_cloud.h>
#include <pointmatcher_ros/transform.h>
#include <pointmatcher_ros/get_params_from_server.h>
#include <pointmatcher_ros/ros_logger.h>

#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>
#include <tf/transform_listener.h>
#include <eigen_conversions/eigen_msg.h>

// Services
#include <std_msgs/String.h>
#include <std_srvs/Empty.h>
#include <map_msgs/GetPointMap.h>
#include <map_msgs/SaveMap.h>
#include <ethzasl_icp_mapper/LoadMap.h>
#include <ethzasl_icp_mapper/CorrectPose.h>
#include <ethzasl_icp_mapper/SetMode.h>

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

// include the plugin base
#define PC_SUPPORT
#define NAV_SUPPORT
#include <usma_perception_plugins/perception_plugin_base.h>

// Eigen
#include <Eigen/Dense>
#include <Eigen/Geometry>

namespace usma_perception
{
  using namespace PointMatcherSupport;

  class ethzOdomPlugin : public PerceptionPluginBase
  {
    public:

      // Creates the point cloud generator
      ethzOdomPlugin() { }

      // Destroys the point cloud generator
      ~ethzOdomPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
       * \param it The in types that are passed back up for wanted subscribers in the order given
       * \param ot The out types that are passed back up for wanted publishers in the order given
       * \param sn The shortname that is passed back for naming purposes
       *
       * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
       *
       */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
       * \return A single bool determining whether or not to process the inputs.
       */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void processOptions();

      typedef PointMatcher<float> PM;
      typedef PM::DataPoints DP;

      boost::shared_ptr<ManagedSubscriber<sensor_msgs::PointCloud2::ConstPtr> > in_new_cloud_sub_;
      boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > imu_sub_;

      boost::shared_ptr<ros::Publisher> odom_pub_;
      nav_msgs::Odometry cur_odom_;

      boost::shared_ptr<ros::Publisher> pc_pub_;

      boost::shared_ptr<DP> ref_cloud_;
      Eigen::Quaternion<float> ref_cloud_orientation_;

      PM::TransformationParameters odom_tf_;
      boost::mutex odom_tf_lock_;
      boost::shared_ptr<boost::thread> tf_pub_thread_;
      void tfPublishThread();

      PM::ICP icp;
      boost::shared_ptr<PM::Transformation> ref_transformer_;

      // Parameters
      std::string sensor_frame_;
      std::string base_link_frame_;
      std::string odom_frame_;
      double min_new_good_ratio_;

      boost::shared_ptr<tf::TransformListener> tf_listener_;
      boost::shared_ptr<tf::TransformBroadcaster> tf_broadcaster_;
      tf::StampedTransform base_to_sensor_;

      bool refineTransform(boost::shared_ptr<DP> new_cloud, boost::shared_ptr<DP> ref_cloud, PM::TransformationParameters &refined_tf );
  };
}
#endif
