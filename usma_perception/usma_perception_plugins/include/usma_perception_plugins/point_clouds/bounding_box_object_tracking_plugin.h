#ifndef BOUNDING_BOX_OBJECT_TRACKING_PLUGIN_H_
#define BOUNDING_BOX_OBJECT_TRACKING_PLUGIN_H_

#define PC_SUPPORT
#define MARKER_VIS_SUPPORT
#define BOUNDING_BOX_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/common/centroid.h>
#include <pcl/common/common.h>

namespace usma_perception
{
  class BoundingBoxObjectTrackingPlugin : public PerceptionPluginBase
  {
    public:

      // Creates the point cloud generator
      BoundingBoxObjectTrackingPlugin() { }

      // Destroys the point cloud generator
      ~BoundingBoxObjectTrackingPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void timingGenerator();
      void processOptions();

      inline void poseSmoothing( geometry_msgs::Pose &pp, geometry_msgs::Pose &np, double alpha );
      inline void twistSmoothing( geometry_msgs::Twist &pt, geometry_msgs::Twist &nt, double alpha );
      inline void twistFromPoses( geometry_msgs::Twist &t, geometry_msgs::Pose &pp, geometry_msgs::Pose &np, double dt );

      inline void updateMinMax( pcl::PointXYZ &p, pcl::PointXYZ &min, pcl::PointXYZ &max );
      inline pcl::PointXYZ subtractPoints( pcl::PointXYZ &a, pcl::PointXYZ &b );
      inline void publishVisualizationMarkers( usma_perception_msgs::TrackedBoundingBoxArray &ca, std::vector<int> ids_to_delete );
      inline int findBoxMatch( usma_perception_msgs::TrackedBoundingBox &b, usma_perception_msgs::TrackedBoundingBoxArray &ba );
      inline double dist( double x1, double y1, double z1, double x2, double y2, double z2  );

      double pose_smoothing_alpha_;
      double twist_smoothing_alpha_;
      double max_dist_between_pts_;
      int min_pts_;

      boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > in_cloud_sub_;

      boost::shared_ptr<ros::Publisher> out_bounding_box_pub_;
      boost::shared_ptr<ros::Publisher> out_marker_array_pub_;
      boost::shared_ptr<ros::Publisher> out_point_cloud_pub_;

      std::vector< usma_perception_msgs::TrackedBoundingBoxArray > box_array_vector_;
      pcl::EuclideanClusterExtraction<pcl::PointXYZ> ec_;
  };
}
#endif
