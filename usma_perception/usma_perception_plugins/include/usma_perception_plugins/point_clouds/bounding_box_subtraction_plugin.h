#ifndef BOUNDING_BOX_SUBTRACTION_PLUGIN_H_
#define BOUNDING_BOX_SUBTRACTION_PLUGIN_H_

#define PC_SUPPORT
#define BOUNDING_BOX_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

namespace usma_perception
{
  class BoundingBoxSubtractionPlugin : public PerceptionPluginBase
  {
    public:

      // Creates the point cloud generator
      BoundingBoxSubtractionPlugin() { }

      // Destroys the point cloud generator
      ~BoundingBoxSubtractionPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void timingGenerator();
      void processOptions();

      inline bool isPointInBoxes( pcl::PointXYZ &p );

      boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > in_cloud_sub_;
      boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::TrackedBoundingBoxArray::ConstPtr> > in_box_sub_;
      boost::shared_ptr<ros::Publisher> out_cloud_pub_;

      double min_velocity_;
      inline double dist( double x1, double y1, double z1, double x2, double y2, double z2 );
      std::vector< std::pair< pcl::PointXYZ, pcl::PointXYZ > > box_pairs_;
  };
}
#endif
