#ifndef POINT_CLOUD_PLUGIN_BASE_H_
#define POINT_CLOUD_PLUGIN_BASE_H_

#define PC_SUPPORT
#include <usma_perception_plugins/perception_plugin_base.h>

namespace usma_perception
{
  // A base class for a plugin that uses all point clouds as inputs and outputs
  class PointCloudPluginBase : public PerceptionPluginBase
  {
    public:
      ~PointCloudPluginBase(){};

      virtual void getNumInOut( int &in, int &out ) = 0;
      virtual std::string setShortName() = 0;

      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
      {
        int num_in, num_out;
        this->getNumInOut( num_in, num_out );
        sn = this->setShortName(); 

        it.clear();
        ot.clear();

        for( int i=0; i<num_in; i++ )
        {
          it.push_back("pcl::PCLPointCloud2");
        }

        for( int j=0; j<num_out; j++ )
        {
          ot.push_back("pcl::PCLPointCloud2");
        }
      }

      virtual bool isInputReady( std::vector<bool> has_new_cloud_data ) = 0;
      bool checkInputReady()
      {
        std::vector<bool> has_new_cloud_data;
        for( int i=0; i<input_cloud_subs_.size(); i++ )
          has_new_cloud_data.push_back( input_cloud_subs_[i]->dataReady() );

        return isInputReady( has_new_cloud_data );
      }

    protected:
      PointCloudPluginBase() {};


      std::string short_name_;
      std::vector< std::string > in_types_;
      std::vector< std::string > out_types_;

      std::vector< boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > > input_cloud_subs_;
      std::vector< boost::shared_ptr<ros::Publisher> > output_cloud_pubs_;

      virtual void processOptions() = 0;

      void init()
      {
        for( int i=0; i<subs_.size(); i++ )
        {
          input_cloud_subs_.push_back( boost::any_cast<boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > >(subs_[i]) );
        }
        for( int i=0; i<pubs_.size(); i++ )
        {
          output_cloud_pubs_.push_back( boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[i]) );
        }
      }

      virtual void processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< pcl::PCLPointCloud2::Ptr > &out, std::vector< bool > &should_publish ) = 0;

      void workLoad()
      {
        // if new options are ready, load them in
        if (new_options_ready_)
        {
          options_mtx_.lock();
          processOptions();
          options_mtx_.unlock();
          new_options_ready_ = false;
        }

        // setup the ros inputs and get the data from the subscribers
        std::vector< pcl::PCLPointCloud2::ConstPtr > ros_in;
        for( int i = 0; i<input_cloud_subs_.size(); i++ )
          ros_in.push_back( (input_cloud_subs_[i])->getData() );

        // setup the outputs
        std::vector< pcl::PCLPointCloud2::Ptr > ros_out;
        std::vector< bool > should_publish;
        ros_out.resize( pubs_.size() );
        for( int i=0; i<ros_out.size(); i++ )
        {
          should_publish.push_back(false);
          ros_out[i].reset( new pcl::PCLPointCloud2() );
        }

        // process the point clouds, expects the outputs to be in ros_out
        processPointClouds( ros_in, ros_out, should_publish );

        // publish the outputs
        for( int i=0; i<output_cloud_pubs_.size(); i++ )
          if( should_publish[i] )
            output_cloud_pubs_[i]->publish( ros_out[i] );

      }
  };
};
#endif
