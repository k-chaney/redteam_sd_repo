#ifndef RING_SHADOW_PLUGIN_H_
#define RING_SHADOW_PLUGIN_H_

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/filters/shadowpoints.h>
#include <pcl/features/normal_3d.h>
#include <velodyne_pointcloud/point_types.h>

// ROS Pieces
#include <tf/transform_listener.h>

// include the point cloud plugin base
#define PC_SUPPORT
#include <usma_perception_plugins/point_clouds/point_cloud_plugin_base.h>

namespace usma_perception
{
  class RingShadowPointFilterPlugin : public PointCloudPluginBase
  {
    public:
 
      // Creates the transform plugin
      RingShadowPointFilterPlugin() { }

      // Destroys the transform plugin
      ~RingShadowPointFilterPlugin() { }     
      
      /** \brief Sets the number of point cloud ins and outs are needed
        * \param in passes back the number of point cloud inputs this plugin has
        * \param out passes back the number of point cloud outputs this plugin has
        */
      void getNumInOut( int &in, int &out );

      /** \brief Sets the shortname of the plugin
        * \return Returns back the name that is desired.
        *
        * Provides a method to give a shorter name than the classname to help with identification.
        *
        */
      std::string setShortName();


      /** \brief Provides the methodology to tell if the data should be processed.
        * \param has_new_cloud_data Tells whether the subscribers in the plugin have new data or still have old data
        * \return Single bool determining whether or not to process the inputs.
        *
        * Provides a method to give a shorter name than the classname to help with identification.
        *
        */
      bool isInputReady( std::vector<bool> has_new_cloud_data );


    private:
      /** \brief Processes the options given to the plugin.
        * \param options The parameters given to the module for run time configuration.
        *
        * This method is responsible for pulling in the options that will ultimately be used in the plugin. Convinience functions given in the PerceptionPluginBase class allow for an easy fetch of this data.
        *
        * This method is guaranteed to be run before the worker loop starts.
        *
        * This includes goal_frame_  --  where you want to transform to.
        *
        */
      void processOptions();

      /** \brief This is the actual work horse of the class.
        * \param in A vector of const shared ptrs that are fresh for each loop but are safe to store for later.
        * \param out A vector of shared ptrs that are fresh and already reset for each loop.
        * \param should_publish A vector of bools saying if a particular point cloud should publish after you're done processing.
        *
        */
      void processPointClouds( std::vector< pcl::PCLPointCloud2::ConstPtr > &in, std::vector< boost::shared_ptr< pcl::PCLPointCloud2 > > &out, std::vector<bool> &should_publish );

      inline double dist3Dsquared( velodyne_pointcloud::PointXYZIR p )
      {
        return ( p.x*p.x + p.y*p.y );// + p.z*p.z ); // maybe we don't need to compute the z part
      };

      inline double angleFromOrigin( velodyne_pointcloud::PointXYZIR p )
      {
        return atan2( p.y, p.x );
      };

      double diff_thresh_;
      double diff_2_thresh_;
      int num_rings_;
      double dist_gain_;
      int filter_length_;
  };
}; // end usma_perception namespace

#endif
