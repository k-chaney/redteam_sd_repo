#ifndef MANAGED_IMAGE_TRANSPORT_H_
#define MANAGED_IMAGE_TRANSPORT_H_

#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>
#include <image_transport/image_transport.h>
#include <image_transport/camera_subscriber.h>

namespace usma_perception
{
  class ManagedCameraSubscriber
  {
    public:      
      /**
        * /brief Creates the ManagedCameraSubscriber with desired settings
        * /param type The type of data that is being subscribed to -- only used for external book keeping
        * /param topic The topic that is being subscribed to
        * /param n The node handle that the subscriber gets created from
        * /param cv The condition variable that is triggered in the callback
        *
        * Sets up the ManagedCameraSubscriber and is immediately ready for a ros::spin to feed data in.
        *
        */
      ManagedCameraSubscriber(std::string type, std::string topic, ros::NodeHandle &n, boost::shared_ptr<boost::condition_variable> &cv): 
        type_(type),
        topic_(topic),
        data_ready_(false)
      {
        data_recieved_ = cv;
        sub_.reset( new image_transport::CameraSubscriber );
        image_transport::ImageTransport it(n);
        *(sub_) = (it.subscribeCamera( topic_, 10, &ManagedCameraSubscriber::callback, this ));
      }

      ~ManagedCameraSubscriber()
      {
        sub_->shutdown();
      }

      /**
        * /brief Templated callback that triggers data_recieved_ when called
        *
        * Stores the messages that were recieved from the CameraSubscriber for another part of the system to grab later. Sets data_ready_ for the outside to see.
        *
        */
      void callback( const sensor_msgs::ImageConstPtr &im, const sensor_msgs::CameraInfoConstPtr &ci )
      { 
        data_mtx_.lock();
        data_ready_ = true;
        msg_ = std::make_pair(im,ci); 
        data_mtx_.unlock();
        data_recieved_->notify_one();
      }

      /**
        * /brief Returns if there is new data ready for the outside
        */
      bool dataReady() { return data_ready_; }

      /**
        * /brief Locks the data to prevent writing.
        */
      void lockData() { data_mtx_.lock(); }

      /**
        * /brief Unlocks the data to allow writing.
        */
      void unlockData() { data_mtx_.unlock(); }

      /**
        * /brief Passes back the most recent piece of data available.
        */
      std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > getData()
      { 
        lockData();
        data_ready_ = false;
        delivered_msg_ = msg_;
        unlockData();
        return delivered_msg_; 
      }

      /**
        * /brief Passes back the topic that is subscribed to.
        */
      std::string getTopic() { return topic_; }
 
      /**
        * /brief Passes back the type of data that is subscribed to.
        */
      std::string getType() { return type_; }

    private:
      std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > msg_; ///< Stores the most recent message received from the callback
      std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > delivered_msg_; ///< Stores the data given to the user


      boost::shared_ptr<image_transport::CameraSubscriber> sub_; ///< The ROS subscriber instance.
      std::string topic_; ///< The topic that is subscribed to.
      std::string type_; ///< The type of data that is subscribed to.
      boost::mutex data_mtx_; ///< A lock for the data to ensure thread safety
    
      boost::shared_ptr<boost::condition_variable> data_recieved_; ///< signal to worker thread that data is available
      bool data_ready_; ///< Flag that tells when the data is ready
 };
};

#endif
