#ifndef OCTOMAP_PLUGIN_H_
#define OCTOMAP_PLUGIN_H_

#define PC_SUPPORT
#define OCTOMAP_SUPPORT
#define MARKER_VIS_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

// octomap specifics
#include <octomap_ros/conversions.h>
#include <octomap_msgs/conversions.h>
#include <octomap/octomap.h>
#include <octomap/OcTreeKey.h>

#include <tf/transform_listener.h>
#include <tf/message_filter.h>

namespace usma_perception
{
  class OctomapPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      OctomapPlugin() { }

      // Destroys the point cloud generator
      ~OctomapPlugin() 
      {
       if ( octree_ != NULL )
        delete octree_;

       return;
      }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      void init();
      void workLoad();
      void processOptions();
      void publishVisualizationMarkers();
      std_msgs::ColorRGBA mapColor( double h );

      void insertCloud( pcl::PointCloud<pcl::PointXYZ> &in, ros::Time &stamp );

      tf::TransformListener tf_listener_;
      octomap::point3d sensor_origin_;

      boost::shared_ptr< ManagedSubscriber< pcl::PCLPointCloud2::ConstPtr > > in_cloud_sub_; /// < Managed subscriber for the input
      boost::shared_ptr< ros::Publisher > octomap_pub_; /// < Publisher for the octomap
      boost::shared_ptr< ros::Publisher > vis_marker_pub_; /// < Publisher for vis markers

      octomap::OcTree* octree_; /// < The pointer to our octree
      octomap::KeyRay key_ray_; /// < Temp storage for key casting

      bool compress_map_; /// < Whethor or not it prunes the tree

      std::string world_frame_; /// < The frame that the point cloud should be in upon being added to the octomap -- if not it is dropped
      std::string publish_frame_; /// < 
      std::string sensor_frame_; /// < The frame that the sensor is projecting from
      double resolution_; /// < resolution of the octomap

      double sensor_hit_; /// < 
      double sensor_miss_; /// <
      double sensor_min_; /// <
      double sensor_max_; /// <
      double sensor_max_range_; /// <

      int max_depth_; /// < Derived from the octree itself

      /** \brief Provides a method for clamping low
        */
      inline static void updateMinKey(const octomap::OcTreeKey& in, octomap::OcTreeKey& min) {
        for (unsigned i = 0; i < 3; ++i)
          min[i] = std::min(in[i], min[i]);
      };
 
      /** \brief Provides a method for clamping high
        */   
      inline static void updateMaxKey(const octomap::OcTreeKey& in, octomap::OcTreeKey& max) {
        for (unsigned i = 0; i < 3; ++i)
          max[i] = std::max(in[i], max[i]);
      };
    
  };
}; // end usma_perception namespace

#endif




