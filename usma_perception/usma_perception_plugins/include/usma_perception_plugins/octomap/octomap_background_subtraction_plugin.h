#ifndef OCTOMAP_BACKGROUND_SUBTRACTION_PLUGIN_H_
#define OCTOMAP_BACKGROUND_SUBTRACTION_PLUGIN_H_

#define OCTOMAP_SUPPORT
#define PC_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>
#include <octomap_msgs/Octomap.h>
#include <octomap/octomap.h>
#include <octomap/OcTree.h>
#include <octomap_msgs/conversions.h>

namespace usma_perception
{
  class OctomapBackgroundSubtractionPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      OctomapBackgroundSubtractionPlugin() { }

      // Destroys the point cloud generator
      ~OctomapBackgroundSubtractionPlugin() { 
        if( octree != NULL )
          delete octree;
      }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      boost::shared_ptr<ManagedSubscriber<pcl::PCLPointCloud2::ConstPtr> > in_cloud_sub_;
      boost::shared_ptr<ManagedSubscriber<octomap_msgs::Octomap::ConstPtr> > in_octomap_sub_;
      boost::shared_ptr<ros::Publisher> out_cloud_pub_;
      octomap::OcTree* octree;
      void init();
      void workLoad();
      void processOptions();

      int octomap_freq_divider_;
      double thresh_probability_;
      double search_resolution_;
      int octomap_counter;
  };
}; // end usma_perception namespace

#endif




