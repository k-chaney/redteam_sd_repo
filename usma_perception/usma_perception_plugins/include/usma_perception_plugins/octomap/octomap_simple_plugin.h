#ifndef OCTOMAP_SIMPLE_PLUGIN_H_
#define OCTOMAP_SIMPLE_PLUGIN_H_

#define OCTOMAP_SUPPORT
#define PC_SUPPORT
#define MARKER_VIS_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

// include the premade octomap server
#include <octomap_server/OctomapServer.h>

namespace usma_perception
{
  class OctomapSimplePlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      OctomapSimplePlugin() { }

      // Destroys the point cloud generator
      ~OctomapSimplePlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      boost::shared_ptr<octomap_server::OctomapServer> server_;
      void init();
      void workLoad();
  };
}; // end usma_perception namespace

#endif




