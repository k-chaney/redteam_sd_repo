#ifndef USMA_LIBVISO2_PLUGIN_H_
#define USMA_LIBVISO2_PLUGIN_H_

#define IMAGE_TRANSPORT_SUPPORT
#define NAV_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

#include <viso_mono.h>

#include <cv_bridge/cv_bridge.h>
#include <image_geometry/pinhole_camera_model.h>
#include <tf/transform_broadcaster.h>
#include <tf/transform_listener.h>

#include <sensor_msgs/image_encodings.h>

namespace usma_perception
{
  class LibVisoPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      LibVisoPlugin() { }

      // Destroys the point cloud generator
      ~LibVisoPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      boost::shared_ptr<ManagedCameraSubscriber> im_sub_;
      boost::shared_ptr<ros::Publisher> odom_pub_;
      boost::shared_ptr<ros::Publisher> delta_odom_pub_;

      boost::shared_ptr<VisualOdometryMono> visual_odometer_;
      VisualOdometryMono::parameters vo_params_;

      std::string odom_frame_;
      std::string base_link_frame_;
      std::string camera_frame_;
      bool publish_tf_;

      boost::shared_ptr<tf::TransformListener> tf_listener_;
      tf::TransformBroadcaster tf_broadcaster_;

      tf::Transform integrated_pose_;
      ros::Time prev_update_time_;

      void init();
      void workLoad();
      void processOptions();

      void processMonoParams( VisualOdometryMono::parameters& p );
      void processCommonParams( VisualOdometryMono::parameters& p );
      void processImageInfoPair( const sensor_msgs::ImageConstPtr& image_msg, const sensor_msgs::CameraInfoConstPtr& info_msg);

      std::string genMonoParamString( VisualOdometryMono::parameters p )
      {
        std::string out = "";
        out += std::string("\nmono   -- height                 -- ") + boost::to_string( p.height );
        out += std::string("\nmono   -- pitch                  -- ") + boost::to_string( p.pitch );
        out += std::string("\nmono   -- ransac_iters           -- ") + boost::to_string( p.ransac_iters );
        out += std::string("\nmono   -- inlier_threshold       -- ") + boost::to_string( p.inlier_threshold );
        out += std::string("\nmono   -- motion_threshold       -- ") + boost::to_string( p.motion_threshold );

        out += std::string("\nmatch  -- nms_n                  -- ") + boost::to_string( p.match.nms_n );
        out += std::string("\nmatch  -- nms_tau                -- ") + boost::to_string( p.match.nms_tau );
        out += std::string("\nmatch  -- match_binsize          -- ") + boost::to_string( p.match.match_binsize );
        out += std::string("\nmatch  -- match_radius           -- ") + boost::to_string( p.match.match_radius );
        out += std::string("\nmatch  -- match_disp_tolerance   -- ") + boost::to_string( p.match.match_disp_tolerance );
        out += std::string("\nmatch  -- outlier_disp_tolerance -- ") + boost::to_string( p.match.outlier_disp_tolerance );
        out += std::string("\nmatch  -- outlier_flow_tolearnce -- ") + boost::to_string( p.match.outlier_flow_tolerance );
        out += std::string("\nmatch  -- multistage             -- ") + boost::to_string( p.match.multi_stage );
        out += std::string("\nmatch  -- half_resoltion         -- ") + boost::to_string( p.match.half_resolution );
        out += std::string("\nmatch  -- refinement             -- ") + boost::to_string( p.match.refinement );

        out += std::string("\nbucket -- max_features           -- ") + boost::to_string( p.bucket.max_features );
        out += std::string("\nbucket -- bucket_width           -- ") + boost::to_string( p.bucket.bucket_width );
        out += std::string("\nbucket -- bucket_height          -- ") + boost::to_string( p.bucket.bucket_height );
        
        out += std::string("\ncalib -- f                       -- ") + boost::to_string( p.calib.f );
        out += std::string("\ncalib -- cu                      -- ") + boost::to_string( p.calib.cu );
        out += std::string("\ncalib -- cv                      -- ") + boost::to_string( p.calib.cv );
        return out;
      };
  };
}; // end usma_perception namespace

#endif
