/*
 *
 * Utilities found in github.com/utiasSTARS/msckf-swf-comparison
 *
 *      - /msckf/utils
 *      - /msckf
 *
 * The purpose is to implement all base functionality found there in C++ with Eigen
 *
 */

#ifndef USMA_MSCKF_UTILS_H_
#define USMA_MSCKF_UTILS_H_

#include <Eigen/Dense>
#include <Eigen/SVD>

#include <cassert>

#include <limits>

#include <vector>
#include <map>

namespace usma_perception
{
  namespace msckf
  {
    typedef Eigen::Matrix<double,12,12> imuCovar;
    typedef Eigen::Matrix<double,4,1> quaternion;
    typedef Eigen::Matrix<double,3,1> position;
    typedef Eigen::Matrix<double,2,1> pixelMeasurement;

    struct camera
    {
      double cu_; ///< principal pionts [u pixels]
      double cv_; ///< principal point [v pixels]
      double fu_; ///< focal length [u pixels]
      double fv_; ///< focal length [v pixels]
      double b_; ///< stereo baseline
      quaternion q_CI_; ///< quaternion imu to camera
      position p_CI_; ///< position imu to camera
    };

    struct imuState
    {
      quaternion q_IG_;
      position p_IG_;
      Eigen::Matrix<double,3,1> b_g_;
      Eigen::Matrix<double,3,1> b_v_;
    };

    struct cameraState
    {
      quaternion q_CG_;
      position p_CG_;
      int state_k_;
      std::vector<int> tracked_features_;
    };

    struct msckfState
    {
      imuState imu_state_;
      std::vector<cameraState> cam_states_;

      imuCovar imu_covar_;
      Eigen::MatrixXd cam_covar_;
      Eigen::MatrixXd imu_cam_covar_;
    };

    struct measurement
    {
      double dT_;
      std::map< int, pixelMeasurement > y_;
      Eigen::Matrix<double,3,1> omega_;
      Eigen::Matrix<double,3,1> v_;
      Eigen::Matrix<double,3,1> a_;
    };

    struct noiseParams
    {
      double u_var_prime_;
      double v_var_prime_;
      imuCovar Q_imu_;
      imuCovar initial_IMU_covar_;
    };

    struct msckfParams
    {
      int min_track_length_;
      int max_track_length_;
      double max_gn_cost_norm_;
      double min_rcond_;
      bool do_null_space_trick_;
      bool do_qr_decomp_;
      bool is_velocity_; // if not velocity then it's acceleration
    };

    struct gn_pos_est_ret
    {
      position p_f_G_;
      double J_;
      double reciprocal_condition_num_;
    };

    struct calc_hoj_ret
    {
      Eigen::MatrixXd H_o_j_;
      Eigen::MatrixXd A_j_;
      Eigen::MatrixXd H_x_j_;
      Eigen::MatrixXd H_f_j_;
    };

    struct remove_feature_ret
    {
      std::vector<cameraState> feat_cam_states_;
      std::vector<int> cam_state_indices_;
    };

    Eigen::MatrixXd calcK( Eigen::MatrixXd &T_H, Eigen::MatrixXd &P, Eigen::MatrixXd &R_n );
    void propagateMsckfStateAndCovar( msckfState &msckf_state, measurement &m_k, noiseParams &noise_params );
    void propagateImuState( imuState &is_k, measurement &m_k );
    inline imuCovar calcF( imuState &is_k, measurement &m_k );
    inline imuCovar calcG( imuState &is_k );
    gn_pos_est_ret calcGNPosEst( std::vector<cameraState> &camera_states, std::vector<pixelMeasurement> &observations, noiseParams &noise_params );
    void calcHoj( position &p_f_G_, msckfState &msckf_state, std::vector<int> &cam_state_indices, Eigen::MatrixXd &H_o_j, Eigen::MatrixXd &A_j, Eigen::MatrixXd &H_x_j );
    void augmentState( msckfState &msckf_state, camera &camera, int state_k );
    Eigen::MatrixXd calcJ( camera &cam, imuState &imu_state_, std::vector<cameraState> &cam_states );
    std::pair<Eigen::MatrixXd,Eigen::MatrixXd> calcTH( Eigen::MatrixXd &H_o );
    Eigen::MatrixXd calcResidual( position &p_f_G, std::vector<cameraState> &camera_states, std::vector<pixelMeasurement> &measurements );
    remove_feature_ret removeTrackedFeature( msckfState &msckf_state, int feat_id );
    msckfState updateState( msckfState &msckf_state, Eigen::MatrixXd &deltaX );
    void pruneStates( msckfState &msckf_state );

    Eigen::Matrix<double,3,3> crossMat( Eigen::Matrix<double,3,1> &vec );
    Eigen::Matrix<double,3,1> crossMatToVec( Eigen::Matrix<double,3,3> &mat );
    Eigen::Matrix<double,3,3> axisAngleToRotMat( Eigen::Matrix<double,3,1> &psi );
    quaternion buildUpdateQuat( Eigen::Matrix<double,3,1> &deltaTheta );
    void enforcePSD( imuCovar &m );
    Eigen::Matrix<double,4,4> omegaMat( Eigen::Matrix<double,3,1> &omega );
    quaternion quatInv( quaternion &q);
    Eigen::Matrix<double,4,4> quatLeftComp( quaternion &q );
    quaternion quatMult( quaternion &q1, quaternion &q2 );
    Eigen::Matrix<double,4,4> quatRightComp( quaternion &q );
    Eigen::Matrix<double,3,3> renormalizeRotMat( Eigen::Matrix<double,3,3> &m );

    template<typename T>
    std::vector< T > removeCells( std::vector<T> &in, std::vector<int> &delete_idx );

    quaternion rotMatToQuat( Eigen::Matrix<double,3,3> &R );
    Eigen::Matrix<double,3,3> quatToRotMat( quaternion &q );


    position triangulate( pixelMeasurement &o1, pixelMeasurement &o2, Eigen::Matrix<double,3,3> &R, Eigen::Matrix<double,3,1> &t );

    double calcRCOND( Eigen::Matrix<double,3,3> &m );
  }
}

#endif
