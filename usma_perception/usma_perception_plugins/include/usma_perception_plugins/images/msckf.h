#ifndef USMA_MSCKF_H_
#define USMA_MSCKF_H_

#include <usma_perception_plugins/images/msckf_utils.h>
#include <map>

namespace usma_perception
{
  namespace msckf
  {
    class MSCKF
    {
      public:
        MSCKF( imuState &is, measurement &m_k, camera &cam, int kStart, noiseParams &np, msckfParams &mp );
        void update( measurement &m_k );
        std::pair<Eigen::Matrix<double,3,1>, Eigen::Matrix<double,4,1> > getPose();

        const msckfState* getMSCKFState() { return &(msckf_state_); };

      private:
        int cur_state_k_;
        msckfParams msckf_params_;
        noiseParams noise_params_;
        camera camera_params_;
        msckfState msckf_state_;

        std::map<int, std::vector<Eigen::Matrix<double,2,1> > > feature_tracks_;

        measurement p_m_k_; //<<< Stores the previous measurment
    };
  }
}

#endif
