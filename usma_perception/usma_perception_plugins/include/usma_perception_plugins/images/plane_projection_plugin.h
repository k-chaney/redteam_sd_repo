#ifndef USMA_PlaneProjection_PLUGIN_H_
#define USMA_PlaneProjection_PLUGIN_H_

#define IMAGE_TRANSPORT_SUPPORT
#define NAV_SUPPORT
#define PC_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/image_encodings.h>
#include <image_geometry/pinhole_camera_model.h>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_ros/transforms.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf/transform_listener.h>

namespace usma_perception
{
  class PlaneProjectionPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      PlaneProjectionPlugin() { }

      // Destroys the point cloud generator
      ~PlaneProjectionPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      boost::shared_ptr<ManagedCameraSubscriber> im_sub_;
      boost::shared_ptr<ros::Publisher> point_cloud_pub_;

      void init();
      void workLoad();
      void processOptions();

      int thresh_;
      double horizon_;
      double offset_;

      std::string ground_frame_;
      std::string sensor_frame_;
      tf::TransformListener tf_listener_;

      tf::StampedTransform T_ground_to_camera_;
      tf::StampedTransform T_camera_to_ground_;
      tf::StampedTransform R_ground_to_camera_;
      tf::StampedTransform R_camera_to_ground_;


  };
}; // end usma_perception namespace

#endif
