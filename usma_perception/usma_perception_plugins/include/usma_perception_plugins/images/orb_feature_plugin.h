#ifndef USMA_ORB_PLUGIN_H_
#define USMA_ORB_PLUGIN_H_

#define IMAGE_TRANSPORT_SUPPORT
#define NAV_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

#include <cv_bridge/cv_bridge.h>

#include <sensor_msgs/image_encodings.h>

#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>

namespace usma_perception
{
  class ORBPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      ORBPlugin() { }

      // Destroys the point cloud generator
      ~ORBPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      int n_features_;
      float scale_factor_;
      int n_levels_;
      int edge_threshold_;
      int first_level_;
      int wta_k_;
      int score_type_;
      int patch_size_;
      double nn_max_dist_ratio_; ///< max ratio between the match distances
      double inlier_threshold_;

      int cur_tag_num_;

      boost::shared_ptr<cv::ORB> orb_detector_;
      boost::shared_ptr<cv::BFMatcher > matcher_;
      boost::shared_ptr<std::vector<cv::KeyPoint> > keypoints_p_; ///< Stores the keypoints in the previous frame
      boost::shared_ptr<std::vector<int> > kp_tags_p_; ///< Stores the tags for the keypoints in the current frame
      boost::shared_ptr<std::vector<int> > kp_feature_length_p_; ///< Stores how many frames the features has existed
      boost::shared_ptr<cv::Mat> descriptors_p_; ///< Stores the descriptors in the previous frame
      cv_bridge::CvImageConstPtr p_img_;

      boost::shared_ptr<std::vector<cv::KeyPoint> > keypoints_c_; ///< Stores the keypoints in the current frame
      boost::shared_ptr<std::vector<int> > kp_tags_c_; ///< Stores the tags for the keypoints in the current frame
      boost::shared_ptr<std::vector<int> > kp_feature_length_c_; ///< Stores how many frames the features has existed
      boost::shared_ptr<cv::Mat> descriptors_c_; ///< Stores the descriptors in the current frame
      cv::Mat homography_;
      cv::Mat keydrawn1, keydrawn2, matchdrawn;

      boost::shared_ptr<ManagedCameraSubscriber> im_sub_;
      boost::shared_ptr<ros::Publisher> feature_pub_;
      boost::shared_ptr<image_transport::CameraPublisher> image_overlay_pub_;

      void init();
      void workLoad();
      void processOptions();
  };
}; // end usma_perception namespace

#endif
