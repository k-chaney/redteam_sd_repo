#ifndef USMA_MSCKF_PLUGIN_H_
#define USMA_MSCKF_PLUGIN_H_

#define NAV_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>
#include <usma_perception_plugins/images/msckf.h>
#include <usma_perception_plugins/images/msckf_utils.h>

#include <Eigen/Dense>
#include <tf/transform_listener.h>

namespace usma_perception
{
  class MSCKFPlugin : public PerceptionPluginBase
  {
    public:
      // Creates the point cloud generator
      MSCKFPlugin() { }

      // Destroys the point cloud generator
      ~MSCKFPlugin() { }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn );

      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady();

    private:
      boost::shared_ptr<ManagedSubscriber<sensor_msgs::Imu::ConstPtr> > imu_sub_;
      boost::shared_ptr<ManagedSubscriber<usma_perception_msgs::FeatureArray::ConstPtr> > feature_sub_;

      boost::shared_ptr<msckf::MSCKF> msckf_;

      std::string imu_frame_; ///< The frame of the IMU
      std::string cam_frame_; ///< The frame of the camera
      msckf::camera cam_params_; ///< Camera parameters for initializing MSCKF
      msckf::measurement cur_measurement_; ///< Stores the current udpate for the MSCKF
      msckf::noiseParams noise_params_; ///< noise aprameters relating to the IMU and camera
      msckf::msckfParams msckf_params_; ///< parameters dictating how the MSCKF functions

      boost::shared_ptr<tf::TransformListener> tf_listener_;

      ros::Time previous_time_; ///< last time through workload loop

      void init();
      void workLoad();
      void processOptions();

      void populateIMUMeasurment( sensor_msgs::Imu::ConstPtr &imu, msckf::measurement &m_k );
      void populateFeatureMeasurment( usma_perception_msgs::FeatureArray::ConstPtr &features, msckf::measurement &m_k );
  };
}; // end usma_perception namespace

#endif
