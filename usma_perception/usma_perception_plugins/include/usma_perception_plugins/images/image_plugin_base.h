#ifndef IMAGE_PLUGIN_BASE_H_
#define IMAGE_PLUGIN_BASE_H_

#define IMAGE_TRANSPORT_SUPPORT
#include <usma_perception_plugins/perception_plugin_base.h>

namespace usma_perception
{
  // A base class for a plugin that uses all point clouds as inputs and outputs
  class ImagePluginBase : public PerceptionPluginBase
  {
    public:
      ~ImagePluginBase(){};

      virtual void getNumInOut( int &in, int &out ) = 0;
      virtual std::string setShortName() = 0;

      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn)
      {
        int num_in, num_out;
        this->getNumInOut( num_in, num_out );
        sn = this->setShortName(); 

        it.clear();
        ot.clear();

        for( int i=0; i<num_in; i++ )
        {
          it.push_back("image_transport::Camera");
        }

        for( int j=0; j<num_out; j++ )
        {
          ot.push_back("image_transport::Camera");
        }
      }

      virtual bool isInputReady( std::vector<bool> has_new_image_data ) = 0;
      bool checkInputReady()
      {
        std::vector<bool> has_new_image_data;
        for( int i=0; i<input_image_subs_.size(); i++ )
          has_new_image_data.push_back( input_image_subs_[i]->dataReady() );

        return isInputReady( has_new_image_data );
      }

    protected:
      ImagePluginBase() {};

      std::vector< boost::shared_ptr<ManagedCameraSubscriber> > input_image_subs_;
      std::vector< boost::shared_ptr<image_transport::CameraPublisher> > output_image_pubs_;

      std::string short_name_;
      std::vector< std::string > in_types_;
      std::vector< std::string > out_types_;

      virtual void processOptions() = 0;

      void init()
      {
        for(int i = 0; i < subs_.size(); i++)
          input_image_subs_.push_back( boost::any_cast<boost::shared_ptr<ManagedCameraSubscriber> >(subs_[i]) );

        for(int i = 0; i < pubs_.size(); i++)
          output_image_pubs_.push_back( boost::any_cast<boost::shared_ptr<image_transport::CameraPublisher> >(pubs_[i]) );
      }

      virtual void processImages( std::vector< std::pair< sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > &in, std::vector< std::pair<sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > &out, std::vector< bool > &should_publish ) = 0;

      void workLoad()
      {
        // if new options are ready, load them in
        if (new_options_ready_)
        {
          options_mtx_.lock();
          processOptions();
          options_mtx_.unlock();
          new_options_ready_ = false;
        }

        // setup the ros inputs and get the data from the subscribers
        std::vector< std::pair<sensor_msgs::ImageConstPtr, sensor_msgs::CameraInfoConstPtr > > ros_in;
        for( int i = 0; i<input_image_subs_.size(); i++ )
          ros_in.push_back( (input_image_subs_[i])->getData() );

        // setup the outputs
        std::vector< std::pair<sensor_msgs::ImagePtr, sensor_msgs::CameraInfoPtr > > ros_out;
        std::vector< bool > should_publish;
        ros_out.resize( output_image_pubs_.size() );
        for( int i=0; i<ros_out.size(); i++ )
        {
          should_publish.push_back(false);
          ros_out[i].first.reset( new sensor_msgs::Image() );
          ros_out[i].second.reset( new sensor_msgs::CameraInfo() );
        }

        // process the images, expects the outputs to be in ros_out
        processImages( ros_in, ros_out, should_publish );

        // publish the outputs
        for( int i=0; i<output_image_pubs_.size(); i++ )
          if( should_publish[i] )
            output_image_pubs_[i]->publish( ros_out[i].first, ros_out[i].second );

      }
  };
};
#endif
