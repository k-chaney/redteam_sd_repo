#ifndef PCGEN_BASE_PLUGIN_H_
#define PCGEN_BASE_PLUGIN_H_

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

// ROS Pieces
#include <sensor_msgs/PointCloud2.h>

#define PC_SUPPORT

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>

namespace usma_perception
{
  class PCGenPluginBase : public PerceptionPluginBase
  {
    public:
      // Destroys the point cloud generator
      ~PCGenPluginBase() 
      { 
        timer_thread_.join();
      }

      /** \brief Sets all of the plugin IO info and the shortname
        * \param it The in types that are passed back up for wanted subscribers in the order given
        * \param ot The out types that are passed back up for wanted publishers in the order given
        * \param sn The shortname that is passed back for naming purposes
        *
        * Sets all of the info that the PerceptionPluginBase needs to verify the requested ins and outs
        *
        */
      void setPluginIOTypes( std::vector<std::string> &it, std::vector<std::string> &ot, std::string &sn )
      {
        setShortName( sn );

        ot.clear();
        it.clear();

        ot.push_back("pcl::PCLPointCloud2");
      }


      /** \brief Provides a method for the class to determine if it wants to run
        * \return A single bool determining whether or not to process the inputs.
        */
      bool checkInputReady()
      {
        return loop_ready_;
      }
    protected:

      // Creates the point cloud generator
      PCGenPluginBase() { }

      virtual void setShortName( std::string &sn ) = 0;
      virtual void genPC( pcl::PCLPointCloud2::Ptr &out ) = 0;
      virtual void processOptions() = 0;

    private:
      boost::shared_ptr<ros::Publisher> out_cloud_pub_;

      void init()
      {
        rate_ = getOption<double>("rate", 10.0);
        timer_rate_.reset( new ros::Rate( rate_ ) );
        timer_thread_ = boost::thread( &PCGenPluginBase::timingGenerator, this );
        processOptions();
        out_cloud_pub_ = boost::any_cast<boost::shared_ptr<ros::Publisher> >(pubs_[0]);
      }

      void workLoad()
      {
        if( new_options_ready_ )
        {
          options_mtx_.lock();
          processOptions();
          rate_ = getOption<double>("rate", 10.0);
          new_rate_ = true;
          options_mtx_.unlock();
        }

        pcl::PCLPointCloud2::Ptr out_cloud;
        out_cloud.reset( new pcl::PCLPointCloud2 );

        genPC( out_cloud );

        out_cloud_pub_->publish( out_cloud );
      }

      void timingGenerator()
      {
        new_rate_ = false;
        timer_rate_->reset(); // reset the timer to account for possible delay

        while( ros::ok() )
        {
          loop_ready_mutex_.lock();
          loop_ready_ = true;
          loop_ready_mutex_.unlock();

          calc_spike_->notify_one();
          if ( new_rate_ )
          {
            timer_rate_.reset( new ros::Rate( rate_ ) );
            new_rate_ = false;
            timer_rate_->reset();
          }
          timer_rate_->sleep();
        }
      }

      boost::thread timer_thread_;
      bool new_rate_;
      boost::shared_ptr<ros::Rate> timer_rate_;
      double rate_;

      boost::mutex loop_ready_mutex_;
      bool loop_ready_;
  };
}; // end usma_perception namespace

#endif
