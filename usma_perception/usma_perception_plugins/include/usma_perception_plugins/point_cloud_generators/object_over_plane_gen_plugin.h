#ifndef OOP_PCGEN_PLUGIN_H_
#define OOP_PCGEN_PLUGIN_H_

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

// include the plugin base
#include <usma_perception_plugins/point_cloud_generators/point_cloud_gen_plugin_base.h>

namespace usma_perception
{
  class OOPPCGen : public PCGenPluginBase
  {
    public:

      // Creates the point cloud generator
      OOPPCGen() { }

      // Destroys the point cloud generator
      ~OOPPCGen() { }

    private:
      std::string goal_frame_;
      int num_objects_;
      double x_size_;
      double y_size_;
      double resolution_;
      double object_radius_;
      double path_radius_;
      double period_;

      int time_step_num_;


      void setShortName( std::string &sn );
      void genPC( pcl::PCLPointCloud2::Ptr &out );
      void processOptions();

      inline double dist( double x1, double y1, double x2, double y2 )
      {
        return sqrt( (x2-x1)*(x2-x1)+(y2-y1)*(y2-y1) );
      }
  };
}; // end usma_perception namespace

#endif
