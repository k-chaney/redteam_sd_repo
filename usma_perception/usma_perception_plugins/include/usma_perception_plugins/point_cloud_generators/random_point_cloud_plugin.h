#ifndef RANDPCGEN_PLUGIN_H_
#define RANDPCGEN_PLUGIN_H_

// PCL pieces
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>

// include the plugin base
#include <usma_perception_plugins/perception_plugin_base.h>
#include <usma_perception_plugins/point_cloud_generators/point_cloud_gen_plugin_base.h>

namespace usma_perception
{
  class RandPCGen : public PCGenPluginBase
  {
    public:

      // Creates the point cloud generator
      RandPCGen() { }

      // Destroys the point cloud generator
      ~RandPCGen() { }

    private:
      std::string goal_frame_;
      int cloud_size_;


      void setShortName( std::string &sn );
      void genPC( pcl::PCLPointCloud2::Ptr &out );
      void processOptions();
  };
}; // end usma_perception namespace

#endif
