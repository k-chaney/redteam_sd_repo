#ifndef MANAGED_SUBSCRIBER_H_
#define MANAGED_SUBSCRIBER_H_

#include <ros/ros.h>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
#include <boost/thread/locks.hpp>

namespace usma_perception
{
  template <typename T>
  class ManagedSubscriber
  {
    public:      
      /**
        * /brief Creates the ManagedSubscriber with desired settings
        * /param type The type of data that is being subscribed to -- only used for external book keeping
        * /param topic The topic that is being subscribed to
        * /param n The node handle that the subscriber gets created from
        * /param cv The condition variable that is triggered in the callback
        *
        * Sets up the ManagedSubscriber and is immediately ready for a ros::spin to feed data in.
        *
        */
      ManagedSubscriber(std::string type, std::string topic, ros::NodeHandle &n, boost::shared_ptr<boost::condition_variable> &cv): 
        type_(type),
        topic_(topic),
        data_ready_(false)
      {
        data_recieved_ = cv;
        sub_.reset( new ros::Subscriber );
        *(sub_) = (n.subscribe( topic_, 10, &ManagedSubscriber::callback, this ));
      }

      ~ManagedSubscriber()
      {
        sub_->shutdown();
      }

      /**
        * /brief Templated callback that triggers data_recieved_ when called
        * /param m Data provided by the subscriber itself
        *
        * Stores the message that was recieved from the subscriber for another part of the system to grab later. Sets data_ready_ for the outside to see.
        *
        */
      void callback( T m )
      { 
        data_mtx_.lock();
        data_ready_ = true;
        msg_ = m; 
        data_mtx_.unlock();
        data_recieved_->notify_one();
      }

      /**
        * /brief Returns if there is new data ready for the outside
        */
      bool dataReady() { return data_ready_; }

      /**
        * /brief Locks the data to prevent writing.
        */
      void lockData() { data_mtx_.lock(); }

      /**
        * /brief Unlocks the data to allow writing.
        */
      void unlockData() { data_mtx_.unlock(); }

      /**
        * /brief Passes back the most recent piece of data available.
        */
      T getData()
      { 
        lockData();
        data_ready_ = false;
        delivered_msg_ = msg_;
        unlockData();
        return delivered_msg_; 
      }

      /**
        * /brief Passes back the topic that is subscribed to.
        */
      std::string getTopic() { return topic_; }
 
      /**
        * /brief Passes back the type of data that is subscribed to.
        */
      std::string getType() { return type_; }

    private:
      T msg_; ///< Stores the most recent message received from the callback
      T delivered_msg_; ///< Stores the data given to the user


      boost::shared_ptr<ros::Subscriber> sub_; ///< The ROS subscriber instance.
      std::string topic_; ///< The topic that is subscribed to.
      std::string type_; ///< The type of data that is subscribed to.
      boost::mutex data_mtx_; ///< A lock for the data to ensure thread safety
    
      boost::shared_ptr<boost::condition_variable> data_recieved_; ///< signal to worker thread that data is available
      bool data_ready_; ///< Flag that tells when the data is ready
 };
};

#endif
