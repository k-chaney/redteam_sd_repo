#include <usma_perception_manager/usma_perception_mgr.h>
#include <usma_perception_plugins/perception_plugin_base.h>

#include <ros/ros.h>

namespace usma_perception
{
  PerceptionManager::PerceptionManager() : plugin_loader_("usma_perception_plugins", "usma_perception::PerceptionPluginBase")
  {
  }

  void PerceptionManager::initialize()
  {
    nh_.reset( new ros::NodeHandle("usma_perception") );

    branch_request_service_ = nh_->advertiseService( "branch_request", &usma_perception::PerceptionManager::requestPluginBranch, this );
  }

  bool PerceptionManager::requestPluginBranch( usma_perception_manager::BranchRequest::Request& req, usma_perception_manager::BranchRequest::Response& res  )
  {
    return true;
  }
  //{
  //  res.errors = 0; // starts at zero (means no errors)
  //  try
  //  {
  //    // populate the topics knowledge base with the base topic info
  //    for( int i = 0; i < req.base_names.size(); i++ )
  //    {
  //      topics_available_[req.base_names[i]] =  req.base_types[i];
  //    }

  //    // move through each node putting it into place
  //    for( usma_perception_manager::PluginNode node : req.plugin_branch )
  //    {
  //      for( std::string name : node.in_topic_names )
  //      {
  //        bool found_name = false;
  //        if ( topics_available_.find(name) != topics_available_.end() )
  //            found_name = true;
  //        if( found_name == false )
  //        {
  //          res.errors += res.TOPIC_NAME_NOT_KNOWN;
  //          res.error_msg = std::string( "Couldn't find topic " ) + name;
  //          return false;
  //        }
  //      }
  //        
  //      boost::shared_ptr<PluginGraph> plugin_to_load;
  //      plugin_to_load.reset( new PluginGraph );

  //      if ( plugin_loader_.isClassAvailable( node.plugin_type ) )
  //      {
  //        plugin_to_load->plugin = plugin_loader_.createInstance("usma_perception::VoxelPlugin");// node.plugin_type );

  //        std::vector<std::string> in_topic_names;
  //        std::vector<std::string> in_topic_types;
  //        std::vector<std::string> out_topic_names;
  //        std::vector<std::string> options;

  //        for( std::string name : node.in_topic_names )
  //        {
  //          in_topic_names.push_back( name );
  //          in_topic_types.push_back( topics_available_[name] );
  //        }

  //        for( std::string type : plugin_to_load->plugin->getOutTypes() )
  //        {
  //          std::string name;
  //          name = plugin_to_load->plugin->getShortName() + "_" + std::to_string( plugins_graph_.size() ) + "_" + type.substr( type.find_last_of(":") + 1 );
  //          out_topic_names.push_back( name );
  //        }

  //        plugin_to_load->plugin->initialize( in_topic_names, out_topic_names, in_topic_types, options, *nh_ );

  //        plugins_graph_.push_back( plugin_to_load );
  //      }
  //      else
  //      {
  //        ROS_ERROR( "Plugin type not available to load [%s]", node.plugin_type.c_str() );
  //        res.error_msg = std::string( "Plugin type not available to load [ " ) + node.plugin_type.c_str() + std::string( "]" );
  //        res.errors += res.PLUGIN_NOT_FOUND;
  //        return false;
  //      }

  //    }

  //  }
  //  catch(pluginlib::PluginlibException& ex)
  //  {
  //    ROS_ERROR( "[USMA_PERCEPTION_MGR]::Plugin Load Error::%s", ex.what() );
  //    res.error_msg = std::string( "PluginlibException " ) + std::string( ex.what() ) ;
  //    res.errors += res.PLUGIN_LIB_EXCEPTION;
  //  }
  //  return true;
  //}

  void PerceptionManager::start()
  {
//    boost::shared_ptr<usma_perception::PerceptionPluginBase> v_plugin1;
//    boost::shared_ptr<usma_perception::PerceptionPluginBase> v_plugin2;
//
//    try
//    {
//      v_plugin1 = plugin_loader_.createInstance("usma_perception::VoxelPlugin");
//      v_plugin2 = plugin_loader_.createInstance("usma_perception::VoxelPlugin");
//
//      std::vector<std::string> empty_options;
//      std::vector<std::string> in_topic_names;
//      std::vector<std::string> in_msg_types;
//      in_topic_names.push_back("inchatter1");
//      in_msg_types.push_back("std_msgs::String");
//
//      std::vector<std::string> mid_topic_names;
//      std::vector<std::string> mid_msg_types;
//      mid_topic_names.push_back("midchatter1");
//      mid_msg_types.push_back("std_msgs::String");
//
//      std::vector<std::string> out_topic_names;
//      std::vector<std::string> out_msg_types;
//      out_topic_names.push_back("outchatter1");
//      out_msg_types.push_back("std_msgs::String");
//
//
//
//      v_plugin1->initialize( in_topic_names, mid_topic_names, in_msg_types, empty_options, *(this->nh_) );
//
//      v_plugin2->initialize( mid_topic_names, out_topic_names, mid_msg_types, empty_options, *(this->nh_) );
//
//    } 
//    catch(pluginlib::PluginlibException& ex)
//    {
//      ROS_ERROR( "[USMA_PERCEPTION_MGR::Plugin Error]::%s", ex.what() );
//    }

    ros::spin();

  }
}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "usma_perception");
  usma_perception::PerceptionManager pm;

  pm.initialize();
  pm.start();
}
