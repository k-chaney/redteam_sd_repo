#ifndef USMA_PERCEPTION_MGR_H_
#define USMA_PERCEPTION_MGR_H_

// Standard stuff
#include <thread>
#include <string>
#include <vector>

// ROS pieces
#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <usma_perception_manager/BranchRequest.h>
#include <usma_perception_manager/PluginNode.h>

// plugin pieces
#include <pluginlib/class_loader.h>
#include <usma_perception_plugins/perception_plugin_base.h>

namespace usma_perception
{
  struct PluginGraph {
    std::vector<int> bases; // ins from outside the graph
    std::vector<int> ins; // ins from other pieces of the graph
    std::vector<int> outs; // outs to other pieces of the graph
    boost::shared_ptr<usma_perception::PerceptionPluginBase> plugin; // the plugin itself
  };

  struct BaseTopics {
    std::vector<int> outs; // this maps all of the output plugins from the plugins_graph_ vector
    std::string topic; // provides the topic name of the base
    std::string type; // provides the topic type of the base
  };

  class PerceptionManager
  {
    public:
      PerceptionManager();
      ~PerceptionManager(){}
      void initialize();
      void start();
      bool requestPluginBranch( usma_perception_manager::BranchRequest::Request& req, usma_perception_manager::BranchRequest::Response& res );

      boost::shared_ptr<ros::NodeHandle> nh_;

      typedef std::string TopicName;
      typedef std::string TopicType;
      typedef std::pair<TopicName, TopicType> TopicPair;

    private:
      ros::ServiceServer branch_request_service_;

      pluginlib::ClassLoader<usma_perception::PerceptionPluginBase> plugin_loader_;

      std::vector< boost::shared_ptr<PluginGraph> > plugins_graph_; // storage for all of the plugins
      std::vector< boost::shared_ptr<BaseTopics> > bases_; // topics from outside pieces
      std::map< TopicName, TopicType > topics_available_; // topic names used and available within the system

  };
};

#endif
